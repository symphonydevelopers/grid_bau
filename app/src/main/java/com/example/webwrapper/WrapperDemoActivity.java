package com.example.webwrapper;


import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.utils.AESHelper;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.MutableContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.widget.TextView;


public class WrapperDemoActivity extends FragmentActivity
{
	private View          mContent;
	private WebView       mWebView;
	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		//requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		//getWindow().setBackgroundDrawable(null);
		
		// Retains part of the View hierarchy without leaking the previous Context
		mContent = (View)getLastCustomNonConfigurationInstance();
		if(mContent == null){
			LayoutInflater inflater = getLayoutInflater()
				.cloneInContext(new MutableContextWrapper(this));
			ViewGroup parent = (ViewGroup)getWindow().getDecorView()
				.findViewById(Window.ID_ANDROID_CONTENT);
			mContent = inflater.inflate(R.layout.wrapper_app_demo, parent, false);
			setContentView(mContent);
		}
		else{
			MutableContextWrapper context = (MutableContextWrapper)mContent.getContext();
			context.setBaseContext(this);
			setContentView(mContent);
		}
		
		mWebView = (WebView)findViewById(R.id.web_view);
		
		WebSettings webSettings = mWebView.getSettings();
		
		/*
		webSettings.setLoadsImagesAutomatically(true);	
		webSettings.setJavaScriptEnabled(true);		
		webSettings.setDatabaseEnabled(true); 		
		webSettings.setDomStorageEnabled(true);
		webSettings.setLoadWithOverviewMode(true);
		*/
		
		
		
		
		
		
		//webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setLoadWithOverviewMode(true);
	
		webSettings.setUseWideViewPort(true);
		webSettings.setSupportZoom(true);
		webSettings.setDatabaseEnabled(true); 
		
		webSettings.setDomStorageEnabled(true);
		
		webSettings.setJavaScriptEnabled(true);
		
		webSettings.setBuiltInZoomControls(true);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setPluginState(PluginState.ON);
		webSettings.setSupportMultipleWindows(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		
		webSettings.setRenderPriority(RenderPriority.HIGH);
		//webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
	
		//enableHTML5AppCache();
		mWebView.setInitialScale(1);
		
		webSettings.setBuiltInZoomControls(true);
		mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		mWebView.setScrollbarFadingEnabled(true);
		//mWebView.requestFocus(View.FOCUS_DOWN);
		mWebView.setBackgroundColor(0);
		
		
		
		
		
		
		
		
		//webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		//webSettings.setUseWideViewPort(true);
		//webSettings.setSupportZoom(true);
		//mWebView.setInitialScale(1);
			//mWebView.setWebViewClient(mWebViewClient);
		
		
		if(savedInstanceState == null){
			mWebView.loadUrl(getString(R.string.base_url1));
		}
		else{
			setTitle(mWebView.getTitle());
		}
		
		
	}
	
	
	private WebViewClient mWebViewClient = new WebViewClient()
	{
		@Override
		public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
			AESHelper.showSSLErrorDialog(handler, error,WrapperDemoActivity.this);
		}
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{						
			return true;
		}
		
		public void onPageStarted(WebView view, String url, android.graphics.Bitmap favicon)
		{
		}
		
		public void onPageFinished(WebView view, String url)
		{
			setProgressBarIndeterminateVisibility(false);
			setTitle(view.getTitle());
		}
		
	};
			
	@Override
	protected void onStart()
	{
		super.onStart();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		mWebView.onResume();
	}				
	
	@Override
	protected void onPause()
	{
		super.onPause();
		mWebView.onPause();
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();
	}
	
	
	
	
	
}
