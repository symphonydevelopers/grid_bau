package com.ladbrokes.thegrid.utils;

import java.lang.reflect.Method;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;

public class NotificationUtil 
{
    public static Notification createNotification(Context context, PendingIntent pendingIntent, CharSequence text, String title, int iconId) 
    {
        Notification notification;
        if (isNotificationBuilderSupported()) {
            notification = buildNotificationWithBuilder(context, pendingIntent,  text,title, iconId);
        } else {
            notification = buildNotificationPreHoneycomb(context, pendingIntent, text, title,iconId);
        }
        return notification;
    }

    public static boolean isNotificationBuilderSupported() 
    {
        try {
            return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) && Class.forName("android.app.Notification.Builder") != null;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    @SuppressWarnings("deprecation")
    public static Notification buildNotificationPreHoneycomb(Context context, PendingIntent pendingIntent, CharSequence text, String title, int iconId) {
        Notification notification = new Notification(iconId, "", System.currentTimeMillis());
        try {
            // try to call "setLatestEventInfo" if available
            Method m = notification.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class);
            m.invoke(notification, context, text,title,  pendingIntent);
        } catch (Exception e) {
            // do nothing
        }
        return notification;
    }

    @SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressWarnings("deprecation")
    private static Notification buildNotificationWithBuilder(Context context, PendingIntent pendingIntent, CharSequence text, String title, int iconId)
    {
        android.app.Notification.Builder builder = new android.app.Notification.Builder(context)
            .setContentTitle(title)
            .setContentText(text)
            .setContentIntent(pendingIntent)
            .setSmallIcon(iconId);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return builder.build();
        } else {
            return builder.getNotification();
        }
    }
}