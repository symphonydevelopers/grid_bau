package com.ladbrokes.thegrid.cashoutIntroduction.adapter;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Activity.BrowserActivity;
import com.ladbrokes.thegrid.Activity.MainActivity;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.ladbrokes.thegrid.introduction.fragment.HomeActivity;

public class CashoutImageSlideAdapter extends PagerAdapter 
{
	BrowserActivity activity;
	boolean isTablet=false;
	public CashoutImageSlideAdapter(BrowserActivity activity) 
	{
		this.activity = activity;
		
		isTablet=isTablet(activity);
		//isTablet=false;
     	//if(isTablet==true)
     	//	image_count=4;
     	//else
     		image_count=4;
     		
		
   inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

	}
	int image_count=4;
	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	
	
	
	  LayoutInflater inflater=null;
	@Override
	public int getCount() {
		return image_count;
	}
	Button btn;
    //String firstTimeLaunchPref="firstTimeLaunchPref";
    ImageView mImageView =null;
    
  	
	//private AnimatedGifImageView animatedGifImageView;
    
   // View view =null;
	@Override
	public View instantiateItem(ViewGroup container, final int position) 
	{
		  if(inflater==null)
		inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		View view = inflater.inflate(R.layout.cashout_activity_gif_main, container, false);
				
		btn=((Button) view.findViewById(R.id.button1));
		
		btn.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) {
				
				//Log.e("", "CashoutImageSlideAdapter Tab to dismiss  Button Click ");
				
				/*
				SharedPreferences.Editor editor = activity.getSharedPreferences(AppConfig.DB_NAME, 0).edit();
				editor.putBoolean(AppConfig.FIRST_TIME_CASHOUT_LAUNCH, false);
			    editor.commit();
				*/
				
				
			    activity.cashout_slideshow_layout.setVisibility(View.GONE);
			    activity.webview_layout.setVisibility(View.VISIBLE);
			    
			    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			   // activity.callCashOutWebView(activity.betSlipUrl);
			    
			}
		});
		//animatedGifImageView = ((AnimatedGifImageView)view.findViewById(R.id.animatedGifImageView));
		mImageView=null;
		mImageView = (ImageView) view.findViewById(R.id.image_display1);
		if(isTablet==false)
		{
		
				if(position==0)
				{
					
					mImageView.setBackgroundResource(R.drawable.cashout_mobile1);
					btn.setVisibility(View.INVISIBLE);
					
				}	
				else if(position==1)
				{
					mImageView.setBackgroundResource(R.drawable.cashout_mobile2);
					btn.setVisibility(View.INVISIBLE);
				}
				else if(position==2)
				{
					mImageView.setBackgroundResource(R.drawable.cashout_mobile3);
					btn.setVisibility(View.INVISIBLE);
				}
				else if(position==3)
				{
					mImageView.setBackgroundResource(R.drawable.cashout_mobile4);
					btn.setVisibility(View.VISIBLE);
				}
			
			
		}
		else
		{
			if(position==0)
			{				
				mImageView.setBackgroundResource(R.drawable.cashout_tablet1);
				btn.setVisibility(View.INVISIBLE);				
			}	
			else if(position==1)
			{
				mImageView.setBackgroundResource(R.drawable.cashout_tablet2);
				btn.setVisibility(View.INVISIBLE);
			}
			if(position==2)
			{				
				mImageView.setBackgroundResource(R.drawable.cashout_tablet3);
				btn.setVisibility(View.INVISIBLE);				
			}	
			else if(position==3)
			{
				mImageView.setBackgroundResource(R.drawable.cashout_tablet4);
				btn.setVisibility(View.VISIBLE);
			}
			
		}
		
		container.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
}