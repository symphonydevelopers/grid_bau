package com.ladbrokes.thegrid.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class PostLoginCCbEventData {
    int workflowType;
    double accountBalance;
    String accountCurrency,accountId,language,
            sessionToken,ssoToken,
            countryCode,nameIdentifier,
            userToken,timeStamp,screenName,
            email,birthDate,
            userName,partnerSessionId;

    public PostLoginCCbEventData(JSONObject job) {
        try {
            accountCurrency = job.isNull("accountCurrency") ? "" : job.getString("accountCurrency");
            accountId = job.isNull("accountId") ? "" : job.getString("accountId");
            language = job.isNull("language") ? "" : job.getString("language");
            sessionToken = job.isNull("sessionToken") ? "" : job.getString("sessionToken");
            ssoToken = job.isNull("ssoToken") ? "" : job.getString("ssoToken");
            countryCode = job.isNull("countryCode") ? "" : job.getString("countryCode");
            nameIdentifier = job.isNull("nameIdentifier") ? "" : job.getString("nameIdentifier");
            userToken = job.isNull("userToken") ? "" : job.getString("userToken");
            timeStamp = job.isNull("timeStamp") ? "" : job.getString("timeStamp");
            screenName = job.isNull("screenName") ? "" : job.getString("screenName");
            email = job.isNull("email") ? "" : job.getString("email");
            birthDate = job.isNull("birthDate") ? "" : job.getString("birthDate");
            userName = job.isNull("userName") ? "" : job.getString("userName");
            partnerSessionId = job.isNull("partnerSessionId") ? "" : job.getString("partnerSessionId");
            accountBalance = job.isNull("accountBalance") ? 0 : job.getDouble("accountBalance");
            workflowType = job.isNull("workflowType") ? 0 : job.getInt("workflowType");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getWorkflowType() {
        return workflowType;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public String getAccountCurrency() {
        return accountCurrency;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getLanguage() {
        return language;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public String getSsoToken() {
        return ssoToken;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getNameIdentifier() {
        return nameIdentifier;
    }

    public String getUserToken() {
        return userToken;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getUserName() {
        return userName;
    }

    public String getPartnerSessionId() {
        return partnerSessionId;
    }
}
