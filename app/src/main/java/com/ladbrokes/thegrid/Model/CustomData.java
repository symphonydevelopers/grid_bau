package com.ladbrokes.thegrid.Model;

/** This is just a simple class for holding data that is used to render our custom view */
public class CustomData {
	  private int enableImage;
	  private int disbleImage;
	  private int selectImage;
		  
	  
	    private String mText;
	    
	    private boolean toDisplay;
	    private boolean isSelectable;

    public CustomData(int disbleImage,int enableImage,int selectImage, boolean toDisplay,boolean isSelectable) 
    {
    	this.enableImage = enableImage;
    	this.disbleImage = disbleImage;
    	this.selectImage = selectImage;
    	this.toDisplay = toDisplay;
    	this.isSelectable = isSelectable;
    }

    /**
     * @return the backgroundColor
     */
    public int getEnableImage() 
    {
        return enableImage;
    }

    public int getDisableImage() 
    {
        return disbleImage;
    }
    
    
    public int getSelectImage() 
    {
        return selectImage;
    }
    
    
    /**
     * @return the text
     */
    public boolean getBoolean()
    {
    	
        return toDisplay;
    }
    
    
    public boolean getSelectable()
    {
    	
        return isSelectable;
    }
    
    
}
