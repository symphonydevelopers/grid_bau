package com.ladbrokes.thegrid.Adapter;

import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Model.CustomData;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/** An array adapter that knows how to render views when given CustomData classes */
public class CustomArrayAdapter extends ArrayAdapter<CustomData> {
    private LayoutInflater mInflater;

    public CustomArrayAdapter(Context context, CustomData[] values) {
        super(context, R.layout.custom_data_view, values);
        mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    LinearLayout layout;
    LayoutParams params;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) 
        {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.custom_data_view, parent, false);

            
            // Gets linearlayout
             layout = (LinearLayout)convertView.findViewById(R.id.layout_id);
            // Gets the layout params that will allow you to resize the layout
             params = layout.getLayoutParams();
            // Changes the height and width to the specified *pixels*
            
            // Create and save off the holder in the tag so we get quick access to inner fields
            // This must be done for performance reasons
            holder = new Holder();
           // holder.textView = (TextView) convertView.findViewById(R.id.textView);
            convertView.setTag(holder);
        }
        else
        {
            holder = (Holder) convertView.getTag();
        }
        
    /*
    if(position==2)
        params.width = 100;
    else
    	   params.width = 80;	
    layout.setLayoutParams(params);
      */  

        // Populate the text
      //  holder.textView.setText(getItem(position).getText());

        // Set the color
       // convertView.setBackgroundColor(getItem(position).getBackgroundColor());
        if(getItem(position).getBoolean()==true)
        {
                 convertView.setBackgroundResource(getItem(position).getEnableImage());
        }
        else
        convertView.setBackgroundResource(getItem(position).getDisableImage());
        	
        if(getItem(position).getSelectable()==true)
        	   convertView.setBackgroundResource(getItem(position).getSelectImage());
        
       // if(position==0)
       //   convertView.setBackgroundResource(R.drawable.ic_launcher);

        return convertView;
    }

    /** View holder for the views we need access to */
    private static class Holder {
        public TextView textView1;
    }
}
