package com.ladbrokes.thegrid.Config;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WebContainer implements Observable, WebViewEventListener.WebContainerCallback {
    private static final String TAG = "WebContainer";

    private List<WebViewEventListener> webViewEventListeners = new ArrayList<>();
    private Context context;
    private WebView bwinWebView;

    private UrlLoadingHandler mUrlLoadingHandler;
    private HandleFileChooser mHandleFileChooser;
    //Used to get the url clicked on a href
    private Handler hrefUrlHandler;
    public static boolean isLobbyLoaded = false;
    private static  final String LOBBY_SCHEME = "bwinex://?event=lobby&action=loadingcompleted";
    private WebViewErrorListener errorListener;



    public static long applicationLaunch = -1;
    public static long batchInit = -1;
    public static long loadUrlInMillis = -1;
    public static boolean isFirstDynaconCallDone = false;

    public static  final String BATCH_INIT_END = "Batch_Done";
    public static  final String BATCH_Launch_END = "AppInit_To_BchDone";
    public static  final String WEB_SHOWN = "Web_Shown";
    public static  final String APPINIT_TO_BATCHINIT = "AppInit_To_BchInit";
    public static  final String APPINIT_TO_WebShown = "AppInit_To_WebShown";

    public static  final String FABRIC_CUSTOM_DURATION = "TimeConsumed";
    public static  final String PERFORMANCE_TAG = "Performance";
    public static  final String URL_LOADED_CALLBACK = "url_loaded_callback";
    public static  final String SSL_ERROR = "ssl_error";
    public static  final String APPSFLYER_TRACKING = "appsf_conversion";

    public WebContainer(Context context) {
        this.context = context;
        if (context == null) {
            throw new NullPointerException("Context Should not be null");
        }
        bwinWebView = new WebView(context);
        init();
    }

    /**
     * Creating WebContainer with existing {@link WebView}
     *
     * @param context
     * @param bwinWebView
     */
    public WebContainer(Context context, WebView bwinWebView) {
        if (bwinWebView == null) {
            throw new NullPointerException("BWinWebView Should not be null");
        }
        if (context == null) {
            throw new NullPointerException("Context Should not be null");
        }
        this.context = context;
        this.bwinWebView = bwinWebView;
        init();
    }

    @Override
    public void addObserver(WebViewEventListener webViewEventListener, boolean attachContainerCallback) {
        if (webViewEventListener == null) {
            throw new NullPointerException("Null Observer");
        }

        if (!webViewEventListeners.contains(webViewEventListener)) {
            webViewEventListeners.add(webViewEventListener);
        }

        if (attachContainerCallback) {
            webViewEventListener.setWebContainerCallback(this);
        }
    }

    public void removeObserver(WebViewEventListener webViewEventListener) {
        if (webViewEventListener == null) {
            throw new NullPointerException("Null Observer");
        }
        webViewEventListener.setWebContainerCallback(null);
        webViewEventListeners.remove(webViewEventListener);
    }

    public UrlLoadingHandler getUrlLoadingHandler() {
        return mUrlLoadingHandler;
    }

    public void setUrlLoadingHandler(UrlLoadingHandler urlLoadingHandler) {
        this.mUrlLoadingHandler = urlLoadingHandler;
    }

    @Override
    public void messageToWeb(final String json) {
        showMessage("To Web", json);
        bwinWebView.loadUrl("javascript:vanillaApp.native.messageToWeb(" + json + ")");
    }

    @Override
    public void messageToNative(String json) {
        notifyMessageFromWeb(json);
    }

    @Override
    public void loadURL(String url) {
        if (url == null) {
            throw new NullPointerException("URL Should not be null");
        }
        bwinWebView.loadUrl(url);
    }

    /**
     * Returns the current url
     *
     * @return
     */
    @Override
    public String getURL() {
        return bwinWebView.getUrl();
    }


    @Override
    public void notifyObserver(JSONObject message) {
        for (WebViewEventListener webViewEventListener : webViewEventListeners) {
            webViewEventListener.messageFromWeb(message);
        }
    }

    public WebView getWebView() {
        return bwinWebView;
    }

    public void init() {
        clearCookies(context);
        getWebView().addJavascriptInterface(new JavascriptNativeBridge(context), "JsNativeBridge");
        getWebView().setWebChromeClient(new WebChromeClient() {
            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView(view, callback);
                callback.onCustomViewHidden();
            }
            @Override
            public void onHideCustomView() {
                super.onHideCustomView();
            }
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPermissionRequest(PermissionRequest request) {
                request.grant(request.getResources());

            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (getHandleFileChooser() != null) {
                    return getHandleFileChooser().handleFileChooser(filePathCallback, fileChooserParams);
                }
                return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                if (hrefUrlHandler == null) {
                    hrefUrlHandler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            String url = (String) msg.getData().get("url");
                            if (url != null) {
                                try {
                                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                                } catch (ActivityNotFoundException e) {
                                    Log.e(TAG, "handleMessage: ", e);
                                } catch (Exception e) {
                                    Log.e(TAG, "BaseWebViewClient.onInterceptBwinExUriScheme Error open intent:", e);
                                }
                            }
                        }
                    };
                }
                try {
                    Message msg = hrefUrlHandler.obtainMessage();
                    view.requestFocusNodeHref(msg);

                } catch (Exception e) {
                    Log.e("ActivityNotFound", "BaseWebViewClient.onInterceptBwinExUriScheme Error open intent:", e);
                }
                return false;
            }
        });


        getWebView().setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                try {

                    if (errorListener != null && errorListener.onSSLError(view, handler, error)) {
                        super.onReceivedSslError(view, handler, error);
                    }
                    Intent intent = new Intent(SSL_ERROR);
                    intent.putExtra(SSL_ERROR, error.toString());
                    //LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } catch (Exception e) {
                    Log.e(PERFORMANCE_TAG, "exception in SSLError handling::" + e.toString());
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(PERFORMANCE_TAG,"shouldOverrideUrlLoading==="+url);

                if(!isLobbyLoaded && url.contains(LOBBY_SCHEME)){
                    Log.e(PERFORMANCE_TAG,"Dismiss splash on loadingcompleted event");
                    isLobbyLoaded = true;
                    //LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(AppTimeStats.URL_LOADED_CALLBACK));
                }

                if (getUrlLoadingHandler() != null) {
                    return getUrlLoadingHandler().handle(url);
                }

                return super.shouldOverrideUrlLoading(view, url);
            }


            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    String url = request.getUrl().toString();
                    if (!TextUtils.isEmpty(url) && isCCBWrapperJs(url)) {
                        try {
                            return getWrapperJsResource();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                return super.shouldInterceptRequest(view, request);
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    if (!TextUtils.isEmpty(url) && isCCBWrapperJs(url)) {
                        try {
                            return getWrapperJsResource();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                return super.shouldInterceptRequest(view, url);
            }

            private WebResourceResponse getWrapperJsResource() throws Exception {
                return new WebResourceResponse("text/javascript", "UTF-8", context.getAssets().open(String.valueOf("javascript/inject_wrapper_ccb.js")));
            }

            private boolean isCCBWrapperJs(String url) {
                return url.contains("inject_wrapper_ccb.js");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                notifyMessageFromWeb("{\"eventName\": \"ON_PAGE_LOAD\",\"parameters\":{\"URL\":\"" + url + "\"}}");
                try {
                    //checking for QA environment and attaching wrapper js
                    String env = getEnvironment();
                    Log.e(PERFORMANCE_TAG,"getGlobalEnvironment==="+env);
                    if (!TextUtils.isEmpty(env) && env.toLowerCase().startsWith("qa")) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            bwinWebView.evaluateJavascript(getWrapperJsData(), null);
                        } else {
                            bwinWebView.loadUrl(getWrapperJsData());
                        }
                        Log.e(PERFORMANCE_TAG,"onPageFinished=== wrapper JS function added manually..");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e(PERFORMANCE_TAG,"onPageFinished==="+url);

                if(!isLobbyLoaded){
                    Log.e(PERFORMANCE_TAG,"Dismiss splash onPageFinished==="+url);
                    isLobbyLoaded = true;
                }
            }

            public String getEnvironment(){
                return context.getSharedPreferences("env_prefs", Context.MODE_PRIVATE).getString("envName", null);
            }

            @NonNull
            private String getWrapperJsData() {
                return "window.messageToNative = function messageToNative(message) {\n" +
                        "    console.log(message);\n" +
                        "    JsNativeBridge.sendMessageToNative(JSON.stringify(message));\n" +
                        "}";
            }
        });
    }

    private void notifyMessageFromWeb(String message) {
        showMessage("From Web", message);
        try {
            notifyObserver(new JSONObject(message));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Goes back in the history of this WebView.
     *
     * @return true if WebView has history
     */
    public boolean onBackPressed() {
        boolean canGo = bwinWebView.canGoBack();
        if (canGo) {
            bwinWebView.goBack();
        }
        return canGo;
    }

    private void showMessage(String title, String msg) {
        Log.e(TAG, "showMessage() called with: title = [" + title + "], msg = [" + msg + "]");
//        Toast.makeText(context, title + "\n" + msg, Toast.LENGTH_SHORT).show();
    }

    @SuppressWarnings("deprecation")
    private static void clearCookies(Context context) {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                CookieManager.getInstance().removeSessionCookies(null);
            } else {
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.removeSessionCookie();

            }
        } catch (Exception e) {

        }
    }

    /**
     * call to clear webview cache : Mayank Bhatnagar(mbhatnagar@ivycomptech.com)
     */
    private void refreshCache() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                CookieManager.getInstance().removeSessionCookies(null);
                CookieManager.getInstance().removeAllCookies(null);
                CookieManager.getInstance().flush();
            } else {
                CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
                cookieSyncMngr.startSync();
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.removeAllCookie();
                cookieManager.removeSessionCookie();
                cookieSyncMngr.stopSync();
                cookieSyncMngr.sync();
            }

            getWebView().clearCache(true);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

    }

    /**
     * call to clear webview history : Mayank Bhatnagar(mbhatnagar@ivycomptech.com)
     */
    private void clearHistory(){
        getWebView().clearHistory();
    }

    /**
     * refreshes the webview state whenever there is a mismatch in lobby url persisted in app and the one received from Dynacon : Mayank Bhatnagar(mbhatnagar@ivycomptech.com)
     */
    public void refreshWebView(){
        refreshCache();
        clearHistory();
    }

    public HandleFileChooser getHandleFileChooser() {
        return mHandleFileChooser;
    }

    public void setHandleFileChooser(HandleFileChooser mHandleFileChooser) {
        this.mHandleFileChooser = mHandleFileChooser;
    }

    public void setErrorHandler(WebViewErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    public interface UrlLoadingHandler {
        boolean handle(String url);
    }

    public interface HandleFileChooser {
        boolean handleFileChooser(ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams);
    }

    public interface WebViewErrorListener {
        boolean onSSLError(WebView view, SslErrorHandler handler, SslError error);
    }

    private class JavascriptNativeBridge {
        private Handler handler;

        /**
         * Instantiate the interface and set the context
         */
        JavascriptNativeBridge(Context c) {
            handler = new Handler(c.getMainLooper());
        }

        @JavascriptInterface
        public void sendMessageToNative(final String json) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    notifyMessageFromWeb(json);
                }
            });
        }
    }


}