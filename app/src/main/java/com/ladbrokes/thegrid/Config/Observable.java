package com.ladbrokes.thegrid.Config;

import org.json.JSONObject;

public interface Observable {

    void addObserver(WebViewEventListener webViewEventListener, boolean attachContainerCallback);

    void notifyObserver(JSONObject message);
}