package com.ladbrokes.thegrid.Config;

public abstract class WebContainerInterface implements WebViewEventListener {

    private WebContainerCallback webContainerCallback;

    @Override
    public void setWebContainerCallback(WebContainerCallback callback) {
        this.webContainerCallback = callback;
    }

    @Override
    public WebContainerCallback getWebContainerCallback() {
        return webContainerCallback;
    }
}
