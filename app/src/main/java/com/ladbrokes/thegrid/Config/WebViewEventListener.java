package com.ladbrokes.thegrid.Config;

import org.json.JSONObject;

public interface WebViewEventListener {

    void messageFromWeb(JSONObject json);

    void setWebContainerCallback(WebContainerCallback callback);

    WebContainerCallback getWebContainerCallback();

    boolean isValidCCB(String json);


    interface WebContainerCallback {
        void messageToWeb(String json);
        void loadURL(String url);
        void messageToNative(String json);
        String getURL();
    }
}