
/*

Home Page =  https://thegrid-lcm.ladbrokes.com/en?grid=1&clientType=GRD_AND_APP&touchIdRegistered=true&hasCrendential=false&touchIdDeviceSupport=true&setUpTouchIdLater=0&doAutoLogin=0&enableAutoLogin=1

MY_CARD_URL =https://thegrid-lcm.ladbrokes.com/en/mycard?grid=1&clientType=GRD_AND_APP;

LOCATION_URL = https://thegrid-lcm.ladbrokes.com/en/shoplocator?grid=1&clientType=GRD_AND_APP;

BANKING_URL =https://thegrid-lcm.ladbrokes.com/lobby/en/thegrid/banking?header=show&retUrl=https://thegrid-lcm.ladbrokes.com/en/;			

*/

package com.ladbrokes.thegrid.Config;

public class AppConfig {
    public static final String NEW_RELIC_API_KEY = "AA0d6c4c625dfac67ac2e13f74ed3eb50a61b62fed-NRMA";
    public static final String CCB_INITIALIZED_EVENT = "CCB_INITIALIZED";
    public static final String GET_APPLICATION_SETTINGS_EVENT = "GET_APPLICATION_SETTINGS";
    public static final String APP_INITIALIZED_EVENT = "APP_INITIALIZED";
    public static final String LOGIN_SCREEN_ACTIVE_EVENT = "Login_Screen_Active";
    public static final String LOGOUT = "LOGOUT";
    public static final String PRE_LOGIN_EVENT = "PRE_LOGIN";
    public static final String PasswordUpdate_EVENT = "PasswordUpdate";
    public static final String POST_LOGIN_EVENT = "POST_LOGIN";
    public static final String LOGIN_EVENT = "LOGIN";
    public static final String LOGIN_FAIL_EVENT = "LOGIN_FAILED";
    public static final String REGISTRATION_EVENT = "REGISTRATION";
    public static final String SET_APPLICATION_SETTINGS = "SET_APPLICATION_SETTINGS";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String deviceTouchSupported = "deviceTouchSupported";
    public static final String isTouchIDLoginEnabled = "isTouchIDLoginEnabled";
    public static final String keepMeSignedInEnabled = "keepMeSignedInEnabled";
    public static final String IS_TOUCH_ID_ENABLED = "isTouchIDEnabled";
    public static final String CCB_EVENT_NAME = "eventName";
    public static final String CCB_PARAMETERS = "parameters";

    public static int HOME_CLICK = 0;
    public static int CONNECTION_TIMEOUT = 10000;
    public static int READ_TIMEOUT = 30000;
    public static String LANGUAGE = "LANGUAGE_CHANGE";
    public static String USER_NAME = "UserName";
    public static String ONLINE_USER_PASSWORD = "Password";
    public static String HAS_CREDENTIAL = "hasCredential";
    public static String SETUP_LATER = "setUpTouchIdLater";

    public static String IS_GRID_ENABLED = "isGridEnabled";

    public static String AUTO_LOGIN = "auto_login";

    public static String BARCODE_NUMBER_VALUE = "bar_code_number";
    public static String CAME_FROM_SCANNER = "came_from_scanner";
    public static String QR_CODE_SCANNER = "qr_code_scanner";
    public static String BET_SLIP_URL = "bet_slip_url";

    public static String SHOW_FOOTER = "show_footer";

    //public static String OPEN_APP1 ="OpenApp";

    public static String IN_SHOP = "in_shop";
    public static String IN_SHOP_PASSWORD = "Inshop_Password";


    public static String FIRST_TIME_APP_LAUNCH = "firstTimeLaunchValue";

    public static String DB_NAME_ONBOARD_TUTORIAL = "firstTimeOnBoardLaunchPref";


    public static String GA_TRACKING_FIRST_TIME = "gaTrackingFirstTime";
    public static String FIRST_TIME_CASHOUT_LAUNCH = "firstTimeCashOutLaunchValue";

    public static String DB_NAME = "firstTimeLaunchPref";

    public static String CASHOUT_BETSLIP_SCANNED = "Cashout_Betslip_Scanned";

    public static String MARS_STG_URL = "https://stg-thegrid-lcm.ladbrokes.com";
    //  public static String STG_URL ="https://stg-thegrid-lcm.ladbrokes.com";

    //Stage
    public static String STG_URL = "https://stg-thegrid.ladbrokes.com";

    //not working at present
    public static String TST = "https://tst-thegrid.ladbrokes.com";

    //GVC test url
    //public static String QA2 = "https://qa2.myaccount.ladbrokes.com/en/labelhost/login?_nativeApp=sportsw&rurlauth=1&rurl=https://stg-thegrid-lcm.ladbrokes.com/en&cancelUrl=https://stg-thegrid-lcm.ladbrokes.com/en";
    //public static String QA23 = "https://qa2.myaccount.ladbrokes.com/en/labelhost/login?_nativeApp=sportsw&rurlauth=1&rurl=https://stg-thegrid-lcm.ladbrokes.com/en?grid=1&clientType=GRD_AND_APP&touchIdRegistered=false&hasCrendential=false&touchIdDeviceSupport=true&setUpTouchIdLater=0&doAutoLogin=0&enableAutoLogin=1&cancelUrl=https://stg-thegrid-lcm.ladbrokes.com/en?grid=1&clientType=GRD_AND_APP&touchIdRegistered=false&hasCrendential=false&touchIdDeviceSupport=true&setUpTouchIdLater=0&doAutoLogin=0&enableAutoLogin=1";
    //production
    public static String THE_GRID = "https://thegrid.ladbrokes.com";

    public static String MOBILE_TO = "https://mobile2.ladbrokes.com";

    public static String LOCAL = "http://local-mobile.ladbrokes.com";

    //https://thegrid.ladbrokes.com\en?grid=1&clientType=GRD_AND_APP&barcode=
    //DR
    public static String DR_ENVIRONMENT = "https://dr-thegrid-lcm.ladbrokes.com";

    public static String END_EXT = /*_nativeApp=sportsw&*/"grid=1&clientType=GRD_AND_APP";  //mobile2

    /******************   CHANGES START    *******************/

    //public static String MAXOS_URL_IE =https://stg-thegrid-lcm.ladbrokes.com/en/registration?sys_source=ODD&sys_source_id=ODD_MOB&type=gridWrapper";  //FOR MARS

    //public static String FINAL_URL = THE_GRID;
    public static String FINAL_URL = MARS_STG_URL;
    public static boolean ENABLE_GA_TRACKING = true;
    public static boolean SCANNER_THROUGH_JAVASCRIPT = true;

    public static String MAXOS_URL_EN = "http://online.ladbrokes.com/promoRedirect?key=ej0xNjYzMDE5NSZsPTAmcD02NzgwNTg%3D&";       //FOR MC
    public static String MAXOS_URL_IE = "http://online.ladbrokes.com/promoRedirect?key=ej0xNjYzMDE5NSZsPTE3NzU1NjI1JnA9Njg0MTk1";  //FOR MC

    // public static String MAXOS_URL_EN = FINAL_URL+"/"+LANGUAGE+"/registration?sys_source=ODD&sys_source_id=ODD_APP_AND&type=gridWrapper&"+END_EXT;  //FOR MARS
    // public static String MAXOS_URL_IE = FINAL_URL+"/"+LANGUAGE+"/registration?sys_source=ODD&sys_source_id=ODD_APP_AND&type=gridWrapper&"+END_EXT; //FOR MARS

    public static String VERSION_CHECK_URL="http://static.ladbrokes.com/mobile/native/thegrid/version.txt";   //production purpose

    //public static String VERSION_CHECK_URL = "http://static.ladbrokes.com/mobile/native/thegridlite/version.txt";   //testing purpose

    //public static String VERSION_CHECK_URL = "http://static.ladbrokes.com/mobile/native/thegrid/version-test.txt";  //testing purpose


    /******************   CHANGES END    *******************/

    public static String HOME_URL = FINAL_URL + "/" + LANGUAGE + "?" + END_EXT;

    public static String SEND_PREFERENCE_PARAMETER = FINAL_URL + "/api/GDPRMakeAsSeen?mrktPrefStatusSeen=yes";


    public static String MY_CARD_URL = FINAL_URL + "/" + LANGUAGE + "/mycard?" + END_EXT;

    public static String LOCATION_URL = FINAL_URL + "/" + LANGUAGE + "/shoplocator?" + END_EXT;

    public static String BARCODE_SUBMIT_URL = FINAL_URL + "/" + LANGUAGE + "?" + END_EXT + "&barcode=";

    public static String BANKING_URL = FINAL_URL + "/lobby" + "/" + LANGUAGE + "/thegrid/banking?header=show&retUrl=" + FINAL_URL + "/" + LANGUAGE + "/";

    //https://thegrid.ladbrokes.com/lobby/en/thegrid/banking?header=show&retUrl=https://thegrid.ladbrokes.com/en/
}
