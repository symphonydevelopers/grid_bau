package com.ladbrokes.thegrid.Activity;


import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Config.AppConfig;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;


@SuppressLint("NewApi")
public class MyCustomProgressDialog extends ProgressDialog {
    private AnimationDrawable animation;

    public static ProgressDialog ctor(Context context) {
        MyCustomProgressDialog dialog = new MyCustomProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    Context context;

    public MyCustomProgressDialog(Context context) {
        super(context);

        this.context = context;


    }

    public MyCustomProgressDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;


        // setSplashImage();
    }

    public ImageView ani;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_dialog);


        getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT,
                WindowManager.LayoutParams.FILL_PARENT);


        ani = (ImageView) findViewById(R.id.animation);
        // setSplashImage();
        // la.setBackgroundResource(R.drawable.bg);

        ani.setBackgroundResource(R.drawable.custom_progress_dialog_animation);
        animation = (AnimationDrawable) ani.getBackground();


    }

    @Override
    public void create() {
        super.create();
        //animation.start();
        setSplashImage();


        //Log.e("MyCustome","MyCustome  create ");
    }


    @Override
    public void show() {
        super.show();
        animation.start();
        //Log.e("MyCustome","MyCustome  show ");
    }

    @Override
    public void dismiss() {
        super.dismiss();
        animation.stop();

    }


    boolean isTablet = false;


    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public void setSplashImage() {
        isTablet = isTablet(context);
        final int sdk = android.os.Build.VERSION.SDK_INT;
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.scr);
        ImageView img = (ImageView) findViewById(R.id.animation);
        MarginLayoutParams marginParams = new MarginLayoutParams(img.getLayoutParams());
        // marginParams.setMargins(left_margin, top_margin, right_margin, bottom_margin);

        Drawable image;
        if (isTablet == true) {
            if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                //Log.e("","MyCustom Tablet portrait  BG  ---");
                image = (Drawable) context.getResources().getDrawable(R.drawable.bg_tablet_portrait);
                marginParams.setMargins(300, 300, 300, -500);
            } else {
                //Log.e("","MyCustom Tablet landscape  BG  ---");
                image = (Drawable) context.getResources().getDrawable(R.drawable.bg_tablet_landscape);
                marginParams.setMargins(300, 300, 300, -500);
            }
        } else {
            if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                //Log.e("","MyCustom Mobile portrait  BG  ---");
                image = (Drawable) context.getResources().getDrawable(R.drawable.bg_mobile_portrait);
                marginParams.setMargins(300, 300, 300, -450);
            } else {
                //Log.e("","MyCustom Mobile landscape  BG  ---");
                image = (Drawable) context.getResources().getDrawable(R.drawable.bg_mobile_landscape);
                marginParams.setMargins(300, 300, 300, -650);
            }
        }


        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            layout.setBackgroundDrawable(image);
        } else {
            layout.setBackground(image);
        }


        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
        img.setLayoutParams(layoutParams);
    }


}
