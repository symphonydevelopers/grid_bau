package com.ladbrokes.thegrid.Activity;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class MyService extends Service
{

    private static final String TAG = "~!@" + MyService.class.getSimpleName();
    @Override
    public final int onStartCommand(Intent intent, int flags, int startId) 
    {
    	//Log.e(TAG, "---------   MyService   onStartCommand    -------------- ");
        return START_STICKY;
        
    }

 
    @Override
    public void onTaskRemoved(Intent rootIntent) 
    {
       // Toast.makeText(getApplicationContext(), "APP KILLED", Toast.LENGTH_LONG).show(); // here your app is killed by user
    	//Log.e("", "---------   MyService   onTaskRemoved    -------------- ");
        try {
            stopService(new Intent(this, this.getClass()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) 
        {
            super.onTaskRemoved(rootIntent);
        } else{}
    }

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}