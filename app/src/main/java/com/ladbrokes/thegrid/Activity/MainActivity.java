package com.ladbrokes.thegrid.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Activity.BrowserActivity;
import com.ladbrokes.thegrid.Camera.ZBarScannerActivity;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.ladbrokes.thegrid.cashoutIntroduction.adapter.CashoutImageSlideAdapter;
import com.ladbrokes.thegrid.introduction.fragment.HomeActivity;
import com.ladbrokes.thegrid.utils.CheckNetworkConnection;


public class MainActivity extends FragmentActivity implements OnClickListener {

    //private AnimatedGifImageView animatedGifImageView;
    SharedPreferences prefs;
    //String firstTimeLaunchPref="firstTimeLaunchPref";
    String firstTimeLaunchValue = "";
    HomeActivity homeFragment;

    boolean gaTrackingFirstValue = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());

        /*PackageManager pm = getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo("com.mobenga.ladbrokes", PackageManager.GET_ACTIVITIES);

            String test = info.packageName;
            //mWebView.loadUrl("javascript:appSessionExpCheck(\"" + checkLadbrokes + "\")");
        } catch (PackageManager.NameNotFoundException e) {
            String test = e.getMessage();
            String test1=test;
            //mWebView.loadUrl("javascript:onCheckLadbrokesStatus(\"" + checkLadbrokes + "\")");
        }*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.introduction_activity);

        prefs = getSharedPreferences(AppConfig.DB_NAME_ONBOARD_TUTORIAL, MODE_PRIVATE);
        boolean firstTimeLaunchValue = prefs.getBoolean(AppConfig.FIRST_TIME_APP_LAUNCH, true);

        checkAllPermission();

        gaTrackingFirstValue = prefs.getBoolean(AppConfig.GA_TRACKING_FIRST_TIME, true);

        // Log.e("", "MainActivity gaTrackingFirstValue-      "+gaTrackingFirstValue);
        if (gaTrackingFirstValue == true && AppConfig.ENABLE_GA_TRACKING == true)
            appAndFirstLaunch();


        if (firstTimeLaunchValue == true) {

            homeFragment = new HomeActivity();
            switchContent(homeFragment);

        } else {
            if (!CheckNetworkConnection.isConnectionAvailable(MainActivity.this)) {
                String message = getResources().getString(R.string.no_internet_connection);
                showAlertDialog(message, true);
                return;
            } else {
                Intent intent = new Intent(MainActivity.this, BrowserActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    public void showAlertDialog(String message, final boolean finish) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!CheckNetworkConnection.isConnectionAvailable(MainActivity.this)) {
                                    String message = getResources().getString(R.string.no_internet_connection);
                                    showAlertDialog(message, true);
                                    return;
                                } else {
                                    Intent intent = new Intent(MainActivity.this, BrowserActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                //Log.e("", "22  firstTimeCashoutLaunchValue-- 22 ");
                            }
                        });

                        dialog.dismiss();
                        //if (finish)
                        //finish();
                    }
                });
        alertDialog.show();
    }


    public void appAndFirstLaunch() {
        LearningAppApplication application = (LearningAppApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        boolean isTablet = isTablet(this);
        String device_model = "";
        if (isTablet == true)
            device_model = "and_tablet";
        else
            device_model = "and_mobile";

        String set_action = device_model + "_downloads";

        String set_label = device_model + "_downloads";


        String set_label1 = "Grid Android Web";

        mTracker.send(new HitBuilders.EventBuilder().setCategory("android_app_downloads")
                .setAction(set_action)
                .setLabel(set_label)
                .build());
        //Log.e("", "Inside appAndFirstLaunch-      ");
        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
        editor.putBoolean(AppConfig.GA_TRACKING_FIRST_TIME, false);
        editor.commit();

    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private static final int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.USE_FINGERPRINT};


    public void checkAllPermission() {
        try {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        } catch (Exception e) {

        }
    }


    public void switchContent(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragment != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            // Only ProductDetailFragment is added to the back stack.
            transaction.commit();
            //contentFragment = fragment;
        }
    }

    @Override
    public void onClick(View v) {
        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(AppConfig.FIRST_TIME_APP_LAUNCH, false);
        editor.commit();
        Intent intent = new Intent(MainActivity.this, BrowserActivity.class);
        startActivity(intent);
        finish();


    }

    public void onBackPressed() {


    }


}

