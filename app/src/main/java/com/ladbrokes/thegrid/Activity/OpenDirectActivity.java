package com.ladbrokes.thegrid.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;


import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Camera.ZBarScannerActivity;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.salesforce.marketingcloud.notifications.DefaultUrlPresenter;
import com.salesforce.marketingcloud.notifications.NotificationManager;
import com.salesforce.marketingcloud.notifications.NotificationMessage;

import java.util.Locale;

public class OpenDirectActivity extends Activity {

    private static final String TAG = "~#" + OpenDirectActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_open_direct);
       // Log.e(TAG,"OpenDirectActivity 11 ");
        NotificationMessage notificationMessage = NotificationManager.extractMessage(getIntent());
       // Log.e(TAG,"OpenDirectActivity 22 ");
        String url = null;
        if (notificationMessage != null && notificationMessage.type() == NotificationMessage.Type.OPEN_DIRECT) {
            url = notificationMessage.url();
        }

      //  url="https://www.google.com";

       // Log.e(TAG,"OpenDirectActivity 33 "+url);
        Intent intent = new Intent(this, MainActivity.class);
        if (!TextUtils.isEmpty(url))
        {
            switch (url) {
                /*
                case "527":
                    intent = new Intent(this, OriginalDocsActivity.class);
                    break;
                case "123":
                    intent = new Intent(this, MapsActivity.class);
                    break;
                    */
                default:
                    if (URLUtil.isValidUrl(url)) {
                        intent = DefaultUrlPresenter.intentForPresenter(this, notificationMessage);
                    }
            }
        }
       // Log.e(TAG,"OpenDirectActivity 44 ");
        Log.e(TAG, String.format(Locale.ENGLISH, "URL: %s", url));
        startActivity(intent);
       // this.finish();
    }



    @Override
    public void onBackPressed()
    {
     Log.e(TAG,"OpenDirectActivity onBackPressed   --  11  ");
        Intent intent = new Intent(OpenDirectActivity.this,BrowserActivity.class);;
        startActivity(intent);
        this.finish();
    }
}
