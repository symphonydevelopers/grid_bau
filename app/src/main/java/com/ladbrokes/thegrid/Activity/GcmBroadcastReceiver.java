package com.ladbrokes.thegrid.Activity;
import java.lang.reflect.Method;
import java.util.Date;
import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.utils.NotificationUtil;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

public class GcmBroadcastReceiver extends BroadcastReceiver 
{

    private final String TAG = "GcmBroadcastReceiver";
    SharedPreferences prefs ;
    String customerIdPreference1="customerIdPreference1";
    private static final Integer pHashLength = 5;
    String customerIdOld="";
    @SuppressWarnings("deprecation")
	@Override
    public void onReceive(Context context, Intent intent )
    {
    
    	prefs = context.getApplicationContext().getSharedPreferences(customerIdPreference1, 1);  
    	//Log.e("", "11111-------getCustomerId  ------------  tickerText---   ");
       	
       if(intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) 
       {
    	   
       //	Log.e("", "22222-------getCustomerId  ------------  tickerText---   ");

          Context mContext = context;
          CharSequence tickerText = "null";
          Bundle extras = intent.getExtras();
          final PackageManager pm = mContext.getPackageManager();
          ApplicationInfo ai;
          try 
          {
            ai = pm.getApplicationInfo(mContext.getPackageName(), 0);
          } catch (final NameNotFoundException e) {
              ai = null;
          }
          final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
      	//Log.e("", "3333-------getCustomerId  ------------  tickerText---   ");

          @SuppressWarnings("unused")
          String pHash = "";
          try 
          {
              for (String key : extras.keySet())
              {
            	  
            	 // Log.e("", "LLLLLL-------getCustomerId  ------------  tickerText--- pHash-    "+pHash);
                if ( key.compareToIgnoreCase("p") == 0 && extras.getString(key).length() > pHashLength ) 
                {
                    pHash = extras.getString(key);
                	//Log.e("", "LLLLLL-------getCustomerId  ------------  tickerText--- pHash-    "+pHash);
                }
                else if(key.compareToIgnoreCase("payload") == 0 && extras.getString(key).length() > 0 ) 
                {
                    tickerText = extras.getString(key);
                	//Log.e("", "MMMMM-------getCustomerId  ------------  tickerText---  tickerText--   "+tickerText);
                }
                Log.e(TAG, String.format("11  onMessage: %s=%s", key, extras.getString(key)));
                
              //  if(key.equals("URL"))
                	//  Log.e(TAG, String.format("22  onMessage: %s=%s", key, extras.getString(key)));
                	
              }
          }
          catch (NullPointerException npe)
          {
              tickerText = "no key=msg has been provided.";
              //Log.e("", "44444-------getCustomerId  ------------  tickerText---   ");

          }

          /*---- Use notification if your minSDKVersion is below 11 ------*/
          // create status-bar notification
          NotificationManager nm = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
          int icon = R.drawable.icon;
          
         // Notification notification = new Notification(icon, tickerText, System.currentTimeMillis());
        //  notification.flags = Notification.FLAG_AUTO_CANCEL;

        //  notification.defaults |= Notification.DEFAULT_LIGHTS;
        //  notification.defaults |= Notification.DEFAULT_SOUND;
        //  notification.defaults |= Notification.DEFAULT_VIBRATE;

          Intent mainIntent = new Intent(Intent.ACTION_MAIN);
          // The activity to launch when you click on the remote notification
          mainIntent.setClass(mContext, BrowserActivity.class);
          // the unique p-hash from the message is being passed through the extras as "p".
          mainIntent.putExtras(intent.getExtras()); 

          int uniqueID = (int) new Date().getTime();
          // Intent of the activity that is launched once user clicks on notification
          PendingIntent pendingIntent = PendingIntent.getActivity(mContext, uniqueID , mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
     
         // Log.e("", "uniqueID--   "+uniqueID+"   applicationName--  "+applicationName+"   tickerText--   "+tickerText+"   message_no_repeat--  "+message_no_repeat);
       
       //  notification.setLatestEventInfo(mContext, applicationName, tickerText, pendingIntent);
       
          Notification notification= NotificationUtil.createNotification(mContext, pendingIntent,  tickerText,applicationName,icon);
        
         notification.flags = Notification.FLAG_AUTO_CANCEL;

          notification.defaults |= Notification.DEFAULT_LIGHTS;
          notification.defaults |= Notification.DEFAULT_SOUND;
          notification.defaults |= Notification.DEFAULT_VIBRATE; 
         
        customerIdOld =prefs.getString("customerId", "000");
   		
        
   	  	Log.e("", "-------getCustomerId  ------------  tickerText---   "+tickerText+"   applicationName---  "+applicationName);
   	
   		if(!customerIdOld.equalsIgnoreCase((String) tickerText))
   		{
   			SharedPreferences.Editor editor = context.getApplicationContext().getSharedPreferences(customerIdPreference1, 1).edit();
      		editor.putString("customerId", (String) tickerText);
     		editor.commit();
     		nm.notify(uniqueID, notification);
   		}
   
    }
    }
  	
}