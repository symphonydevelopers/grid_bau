package com.ladbrokes.thegrid.Activity;

import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.ladbrokes.thegrid.utils.AESHelper;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class ShowWebView extends Activity {

	//private Button button;
	private WebView mWebView;
	
	Button header;
	
	ProgressDialog progressDialog2;
	boolean activityRunning = false;
	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout
	            & Configuration.SCREENLAYOUT_SIZE_MASK)
	            >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	
	//public static String HOME_URL ="";
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		  requestWindowFeature(Window.FEATURE_NO_TITLE);
		activityRunning = true;
		  boolean isTablet=isTablet(this);
		  if(isTablet==true)
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		  
    	//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
     
		   
		setContentView(R.layout.show_web_view);
	 
	    progressDialog2 = MyCustomProgressDialog.ctor(this);
		//Get webview 
	    mWebView = (WebView) findViewById(R.id.webView1);
        
		WebSettings webSettings = mWebView.getSettings();
		webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setJavaScriptEnabled(true);
		webSettings.setUseWideViewPort(true);
		webSettings.setSupportZoom(true);
		

		//mWebView.setInitialScale(1);
		
		webSettings.setBuiltInZoomControls(true);
		mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		mWebView.setScrollbarFadingEnabled(true);
		
		
		mWebView.setWebViewClient(mWebViewClient);








		header = (Button) findViewById(R.id.back);


		header.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				
					//  Intent intent = new Intent(ZBarScannerActivity.this, MainActivity.class);
			          //  startActivity(intent);
			            finish();
			
			}
		});
        
        Bundle extras = getIntent().getExtras();
        
        if(extras == null) 
	    {	
        	urlConection= AppConfig.HOME_URL;
	    
	    }
        else
        {
        	urlConection= extras.getString("urlConection");
        	if(urlConection.contains("help")){ //Condition to check url contains "HELP" text
				header.setVisibility(View.GONE);//Hiding the header if url contains help
			}
        }
        
        
        eighteen_available=isWordAvailable(urlConection,"images");
        if(eighteen_available==false)
		mWebView.setBackgroundColor(0);
        
        Log.e("", "urlConection--    "+urlConection+"   eighteen_available--   "+eighteen_available);
		
		//startWebView(urlConection);
		
		mWebView.loadUrl(urlConection);
		
	}

	@Override
	protected void onStart() {
		activityRunning = true;
		super.onStart();
	}

	@Override
	protected void onResume() {
		activityRunning = true;
		super.onResume();
	}


	String urlConection="";
	boolean loadingFirst=true;
	boolean eighteen_available=false;
	
	
	   public boolean isWordAvailable(String s,String word)
	    {   
	  	  boolean d=false;
	  	 // Log.e("","s--   "+s+"   word--  "+word);
	  	  try
	  		  {
	  			     d=s.contains(word);
	  		  }
	  		catch(Exception e)
	  		  {
	  			d=false;
	  		  }
	  	    return d;    
	  	}
	
	
	private WebViewClient mWebViewClient = new WebViewClient()
	{
		@Override
		public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
			AESHelper.showSSLErrorDialog(handler, error,ShowWebView.this);
		}
		 public void onPageStarted(WebView view, String url, Bitmap favicon)
	        {
	            // TODO Auto-generated method stub
	            super.onPageStarted(view, url, favicon);

	            String url_new = view.getUrl();             

	            Log.e("","onPageStarted------- Webview Function URL: "+url_new);

	           // addressbar.setText(url_new);
	            
	            if(loadingFirst==true)
	        	{
	        		
	        		//loadingFirst=false;
	        		 if (progressDialog2 != null && activityRunning)
		          	  {
	        			 	//loadingFirst=false;
			                // in standard case YourActivity.this
		          	       //  progressDialog2 = MyCustomProgressDialog.ctor(getApplicationContext());
			              //  progressDialog2.setMessage("");
			               // progressDialog2.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
			                progressDialog2.show();
			            
			            }
	        		 	
	        	}
	            
	            
	            
	            
	        }
		
		
		
		
     public boolean shouldOverrideUrlLoading(WebView view, String url) {              
         view.loadUrl(url);
         return true;
     }

     //Show loader on url load
     public void onLoadResource (WebView view, String url) 
     {
     	
     	
        
     }
     public void onPageFinished(WebView view, String url) 
     {
     	
     	
         try{
         	
         	
         	
         	
         	  if (progressDialog2!=null) 
		            {
	     		    	progressDialog2.dismiss();
	     		    	progressDialog2.cancel();
		               // progressDialog = null;
		            }	
         
         /*	
         if (progressDialog2.isShowing()) 
         
         {
             progressDialog2.dismiss();
             //progressDialog2 = null;
         }*/
         }catch(Exception exception){
             exception.printStackTrace();
         }
     }
     
		
	};
	
	
	
	
	private void startWebView1(String url) 
	{
	    
		//Create new webview Client to show progress dialog
		//When opening a url or click on link
		
		mWebView.setWebViewClient(new WebViewClient() {      
	       // ProgressDialog progressDialog;
	     
	        //If you will not use this method url links are opeen in new brower not in webview
			
			
			
			
			
			 public void onPageStarted(WebView view, String url, Bitmap favicon)
		        {
		            // TODO Auto-generated method stub
		            super.onPageStarted(view, url, favicon);

		            String url_new = view.getUrl();             

		            Log.e("","onPageStarted------- Webview Function URL: "+url_new);

		           // addressbar.setText(url_new);
		            
		            if(loadingFirst==true)
		        	{
		        		
		        		//loadingFirst=false;
		        		 if (progressDialog2 != null) 
			          	  {
		        			 	//loadingFirst=false;
				                // in standard case YourActivity.this
			          	       //  progressDialog2 = MyCustomProgressDialog.ctor(getApplicationContext());
				              //  progressDialog2.setMessage("");
				               // progressDialog2.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
				                progressDialog2.show();
				            
				            }
		        		 	
		        	}
		            
		            
		            
		            
		        }
			
			
			
			
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {              
	            view.loadUrl(url);
	            return true;
	        }
	   
	        //Show loader on url load
	        public void onLoadResource (WebView view, String url) 
	        {
	        	
	        	
	           
	        }
	        public void onPageFinished(WebView view, String url) 
	        {
	        	
	        	
	            try{
	            	
	            	
	            	
	            	
	            	  if (progressDialog2!=null) 
			            {
		     		    	progressDialog2.dismiss();
		     		    	progressDialog2.cancel();
			               // progressDialog = null;
			            }	
	            
	            /*	
	            if (progressDialog2.isShowing()) 
	            
	            {
	                progressDialog2.dismiss();
	                //progressDialog2 = null;
	            }*/
	            }catch(Exception exception){
	                exception.printStackTrace();
	            }
	        }
	        
	    }); 
	     
	  
	    
	    //Load url in webview
		mWebView.loadUrl(url);
	     
	     
	}
	
	// Open previous opened link from history on webview when back button pressed
	
	@Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        if(mWebView.canGoBack()) {
        	mWebView.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }

	@Override
	protected void onPause() {
		super.onPause();
		activityRunning = false;
	}

	@Override
	protected void onStop() {
		super.onStop();
		activityRunning = false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityRunning = false;
	}
}