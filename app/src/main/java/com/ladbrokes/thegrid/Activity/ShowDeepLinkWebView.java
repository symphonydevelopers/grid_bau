package com.ladbrokes.thegrid.Activity;

import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.ladbrokes.thegrid.utils.AESHelper;
import com.otherlevels.android.sdk.OlAndroidLibrary;
import com.salesforce.marketingcloud.notifications.NotificationManager;
import com.salesforce.marketingcloud.notifications.NotificationMessage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class ShowDeepLinkWebView extends FragmentActivity {

	//private Button button;
	private WebView mWebView;
	
	//Button btnBack;
	private static final String TAG = "~!@#" + ShowDeepLinkWebView.class.getSimpleName();

	ProgressDialog progressDialog2;
	public static boolean isTablet(Context context)
	{
	    return (context.getResources().getConfiguration().screenLayout
	            & Configuration.SCREENLAYOUT_SIZE_MASK)
	            >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	private View mContent;
	//public static String HOME_URL ="";
	public void onCreate(Bundle savedInstanceState) {
		
		
		super.onCreate(savedInstanceState);
		
		  requestWindowFeature(Window.FEATURE_NO_TITLE);
		 // boolean isTablet=isTablet(this);
		 // if(isTablet==true)
			//	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		  
    	//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
     
		   
		//setContentView(R.layout.show_deeplink_web_view);


		mContent = (View)getLastCustomNonConfigurationInstance();
		if(mContent == null)
		{
			LayoutInflater inflater = getLayoutInflater().cloneInContext(new MutableContextWrapper(this));
			ViewGroup parent = (ViewGroup)getWindow().getDecorView().findViewById(Window.ID_ANDROID_CONTENT);
			mContent = inflater.inflate(R.layout.show_deeplink_web_view, parent, false);
			setContentView(mContent);
		}
		else
		{
			MutableContextWrapper context = (MutableContextWrapper)mContent.getContext();
			context.setBaseContext(this);
			setContentView(mContent);
		}



		progressDialog2 = MyCustomProgressDialog.ctor(this);
		//Get webview 
	    mWebView = (WebView) findViewById(R.id.webView1);






		WebSettings webSettings = mWebView.getSettings();
		//webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setLoadWithOverviewMode(true);

		webSettings.setUseWideViewPort(true);
		webSettings.setSupportZoom(true);
		webSettings.setDatabaseEnabled(true);

		webSettings.setDomStorageEnabled(true);

		webSettings.setJavaScriptEnabled(true);

		webSettings.setBuiltInZoomControls(true);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setPluginState(WebSettings.PluginState.ON);
		//mWebView.addJavascriptInterface(new BrowserActivity.WebAppInterface(this), "Android");

		webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
		//webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

		//enableHTML5AppCache();
		mWebView.setInitialScale(1);

		webSettings.setBuiltInZoomControls(true);
		mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		mWebView.setScrollbarFadingEnabled(true);
		//mWebView.requestFocus(View.FOCUS_DOWN);
		mWebView.setBackgroundColor(0);


		webSettings.setSupportMultipleWindows(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);


		mWebView.setWebViewClient(mWebViewClient);



		setSplashImage();



		
	}
	NotificationMessage notificationMessage=null;

	public void callNotification()
	{
		Log.e(TAG,"showDeepLinkWebView   callNotification  111111 ");


		notificationMessage = NotificationManager.extractMessage(getIntent());
		//Log.e(TAG,"showDeepLinkWebView   callNotification 11  notificationId   "+notificationMessage.notificationId()+"       URL --  "+notificationMessage.url());
		String url = null;

		if (notificationMessage != null && notificationMessage.type() == NotificationMessage.Type.OPEN_DIRECT)
		{
			//Log.e(TAG,"showDeepLinkWebView   callNotification Inside 11 url --  "+url);
			url = notificationMessage.url();
			//Log.e(TAG,"showDeepLinkWebView   callNotification Inside 22 url --  "+url+"  Title - "+notificationMessage.title()+"  SubTitle - "+notificationMessage.subTitle());
		}
		//Log.e(TAG,"showDeepLinkWebView   callNotification 22 url --  "+url);

		//String  url="https://dr-thegrid-lcm.ladbrokes.com/en/couponBuddy?clientType=APP_GRD_AND";
		mWebView.loadUrl(url);
	}


	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);


		setSplashImage();


	}


	@Override
	public Object onRetainCustomNonConfigurationInstance()
	{
		// We detach our content and return it to be retained
		((ViewGroup)findViewById(Window.ID_ANDROID_CONTENT)).removeView(mContent);
		return mContent;
	}


	@Override
	protected void onPause()
	{
		try {
			//Log.e(TAG,"---   onPause   ---11");
			super.onPause();

			//Log.e(TAG,"---   onPause   ---22");
			//mWebView.reload();
			this.finish();
			//Log.e(TAG,"---   onPause   ---33");
			//callNotification();
		}
		catch(Exception e)
		{


		}

	}


	@Override
	protected void onResume()
	{

		Log.e(TAG,"---   onResume   ---");
		super.onResume();
		//mWebView.onResume();
		//startService(new Intent(this, MyService.class));
		callNotification();

	}


	//String urlConection="";
	boolean loadingFirst=true;
	boolean eighteen_available=false;
	
	/*
	   public boolean isWordAvailable(String s,String word)
	    {   
	  	  boolean d=false;
	  	 // Log.e("","s--   "+s+"   word--  "+word);
	  	  try
	  		  {
	  			     d=s.contains(word);
	  		  }
	  		catch(Exception e)
	  		  {
	  			d=false;
	  		  }
	  	    return d;    
	  	}
	*/
	
	private WebViewClient mWebViewClient = new WebViewClient()
	{
		@Override
		public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
			AESHelper.showSSLErrorDialog(handler, error,ShowDeepLinkWebView.this);
		}

		 public void onPageStarted(WebView view, String url, Bitmap favicon)
	        {
	            // TODO Auto-generated method stub
	            super.onPageStarted(view, url, favicon);

	            String url_new = view.getUrl();             

	           // Log.e(TAG,"onPageStarted------- Webview Function URL: "+url_new);

	           // addressbar.setText(url_new);
	            
	            if(loadingFirst==true)
	        	{
	        		
	        		//loadingFirst=false;
	        		 if (progressDialog2 != null) 
		          	  {
	        			 	//loadingFirst=false;
			                // in standard case YourActivity.this
		          	       //  progressDialog2 = MyCustomProgressDialog.ctor(getApplicationContext());
			              //  progressDialog2.setMessage("");
			               // progressDialog2.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
			                progressDialog2.show();
			            
			            }
	        		 	
	        	}
	            
	            
	            
	            
	        }
		
		
		
		
     public boolean shouldOverrideUrlLoading(WebView view, String url) {              
         view.loadUrl(url);
         return true;
     }

     //Show loader on url load
     public void onLoadResource (WebView view, String url) 
     {
     	
     	
        
     }
     public void onPageFinished(WebView view, String url) 
     {
     	
     	
         try{
         	
         	
         	
         	
         	  if (progressDialog2!=null) 
		            {
	     		    	progressDialog2.dismiss();
	     		    	progressDialog2.cancel();
		               // progressDialog = null;
		            }	
         
         /*	
         if (progressDialog2.isShowing()) 
         
         {
             progressDialog2.dismiss();
             //progressDialog2 = null;
         }*/
         }catch(Exception exception){
             exception.printStackTrace();
         }
     }
     
		
	};
	
	
	
	
	private void startWebView1(String url) 
	{
	    
		//Create new webview Client to show progress dialog
		//When opening a url or click on link
		
		mWebView.setWebViewClient(new WebViewClient() {      
	       // ProgressDialog progressDialog;
	     
	        //If you will not use this method url links are opeen in new brower not in webview


			@Override
			public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
				AESHelper.showSSLErrorDialog(handler, error,ShowDeepLinkWebView.this);
			}
			
			
			 public void onPageStarted(WebView view, String url, Bitmap favicon)
		        {
		            // TODO Auto-generated method stub
		            super.onPageStarted(view, url, favicon);

		            String url_new = view.getUrl();             

		           // Log.e(TAG,"onPageStarted------- Webview Function URL: "+url_new);

		           // addressbar.setText(url_new);
		            
		            if(loadingFirst==true)
		        	{
		        		
		        		//loadingFirst=false;
		        		 if (progressDialog2 != null) 
			          	  {
		        			 	//loadingFirst=false;
				                // in standard case YourActivity.this
			          	       //  progressDialog2 = MyCustomProgressDialog.ctor(getApplicationContext());
				              //  progressDialog2.setMessage("");
				               // progressDialog2.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
				                progressDialog2.show();
				            
				            }
		        		 	
		        	}
		            
		            
		            
		            
		        }
			
			
			
			
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {              
	            view.loadUrl(url);
	            return true;
	        }
	   
	        //Show loader on url load
	        public void onLoadResource (WebView view, String url) 
	        {
	        	
	        	
	           
	        }
	        public void onPageFinished(WebView view, String url) 
	        {
	        	
	        	
	            try{
	            	
	            	
	            	
	            	
	            	  if (progressDialog2!=null) 
			            {
		     		    	progressDialog2.dismiss();
		     		    	progressDialog2.cancel();
			               // progressDialog = null;
			            }	
	            
	            /*	
	            if (progressDialog2.isShowing()) 
	            
	            {
	                progressDialog2.dismiss();
	                //progressDialog2 = null;
	            }*/
	            }catch(Exception exception){
	                exception.printStackTrace();
	            }
	        }
	        
	    }); 
	     
	  
	    
	    //Load url in webview
		mWebView.loadUrl(url);
	     
	     
	}



	@SuppressLint("NewApi")
	public void setSplashImage()
	{
		// Log.e("", "Brow setSplashImage isTablet  "+isTablet);
		Drawable image;

		boolean isTablet=isTablet(this);
		if(isTablet==true)
		{
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			{
				//Log.e("","Tablet portrait  BG  ---");
				image=(Drawable)getResources().getDrawable(R.drawable.bg_tablet_portrait);
				//progressDialog2 = new MyCustomProgressDialog(this);
				progressDialog2.create();

			}
			else
			{
				//Log.e("","Brows Tablet landscape  BG  ---");
				image=(Drawable)getResources().getDrawable(R.drawable.bg_tablet_landscape);
				//progressDialog2 = new MyCustomProgressDialog(this);
				progressDialog2.create();
			}
		}
		else
		{
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			{
				//Log.e("","Brow Mobile portrait  BG  ---");
				image=(Drawable)getResources().getDrawable(R.drawable.bg_mobile_portrait);
				//progressDialog2 = new MyCustomProgressDialog(this);
				progressDialog2.create();
			}
			else
			{
				//Log.e("","Brow Mobile landscape  BG  ---");
				image=(Drawable)getResources().getDrawable(R.drawable.bg_mobile_landscape);
				// progressDialog2 = new MyCustomProgressDialog(this);
				progressDialog2.create();
			}
		}

		mWebView.setBackground(image);

	}
	
	// Open previous opened link from history on webview when back button pressed
	
	@Override
    // Detect when the back button is pressed
    public void onBackPressed() 
	{
		Log.e(TAG,"onBackPressed------- : ");
		Intent intent = new Intent(ShowDeepLinkWebView.this, BrowserActivity.class);
		startActivity(intent);

		/*
        if(mWebView.canGoBack())
        {
        	//mWebView.goBack();
        	

			  Intent intent = new Intent(ShowDeepLinkWebView.this, BrowserActivity.class);
	          startActivity(intent);
        	
        } else
        {
            // Let the system handle the back button
            super.onBackPressed();
        }
        */
    }

}