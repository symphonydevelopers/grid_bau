package com.ladbrokes.thegrid.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.Settings.Secure;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.gson.Gson;
import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Adapter.CustomArrayAdapter;
import com.ladbrokes.thegrid.Camera.ZBarConstants;
import com.ladbrokes.thegrid.Camera.ZBarScannerActivity;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.ladbrokes.thegrid.Model.CustomData;
import com.ladbrokes.thegrid.Model.PostLoginCCbEventData;
import com.ladbrokes.thegrid.cashoutIntroduction.adapter.CashoutImageSlideAdapter;
import com.ladbrokes.thegrid.fingerprintdialog.FingerprintAuthenticationDialogFragment;
import com.ladbrokes.thegrid.fingerprintdialog.FingerprintModule;
import com.ladbrokes.thegrid.utils.AESHelper;
import com.ladbrokes.thegrid.utils.CheckNetworkConnection;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.mobileapptracker.MobileAppTracker;
import com.otherlevels.android.sdk.OlAndroidLibrary;
import com.salesforce.marketingcloud.MarketingCloudSdk;
import com.salesforce.marketingcloud.notifications.NotificationManager;
import com.salesforce.marketingcloud.notifications.NotificationMessage;
import com.salesforce.marketingcloud.registration.RegistrationManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@SuppressWarnings("ALL")
@SuppressLint({"AddJavascriptInterface", "NewApi"})
public class BrowserActivity extends FragmentActivity implements OnTouchListener, TextToSpeech.OnInitListener/*, WebViewEventListener.WebContainerCallback*/ {
    private View mContent;
    public WebView mWebView;

    private ViewPager mViewPager;

    private View mCustomView;
    CookieSyncManager cookieSyncManager;
    CookieManager cookieManager;
    String android_id = "";
    //String deviceId1 ="";
    public String betSlipUrl = "22";
    MobileAppTracker mobileAppTracker;
    ProgressDialog progressDialog2;
    String mAppKey = "Not Key";
    String Date = "01-07-18";
    private WebChromeClient.CustomViewCallback customViewCallback;
    private myWebChromeClient mWebChromeClient;

    boolean qr_code_correct = false;
    private FrameLayout customViewContainer;

    private ValueCallback<Uri[]> mUploadMessages;
    private Uri mCapturedImageURI = null;

    public static final int FILECHOOSER_RESULTCODE = 7777;
    private CustomData[] mCustomData = new CustomData[]
            {
                    new CustomData(R.drawable.home_disabled, R.drawable.home_enabled, R.drawable.home_select, true, true),
                    new CustomData(R.drawable.banking_disabled, R.drawable.banking__enabled, R.drawable.banking_select, false, false),
                    new CustomData(R.drawable.grid_disabled, R.drawable.grid_enabled, R.drawable.grid_select, false, false),
                    new CustomData(R.drawable.scanner_disabled, R.drawable.scanner_enabled, R.drawable.scanner_select, true, false),
                    new CustomData(R.drawable.locator_disabled, R.drawable.locator_enabled, R.drawable.locator_select, true, false)
            };

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

//boolean home_click=false;

    String trackingId = "";

    private static final String TAG1 = "~*" + BrowserActivity.class.getSimpleName();
    private static final String TAG = "~!@" + BrowserActivity.class.getSimpleName();

    private static final String JAVASCRIPT_TAG = "~!#" + BrowserActivity.class.getSimpleName();
    private static final String DIALOG_FRAGMENT_TAG = "myFragment";
    private static final String SECRET_MESSAGE = "Very secret message";
    /**
     * Alias for our key in the Android Key Store
     */
    private static final String KEY_NAME = "my_key";
    private static final int FINGERPRINT_PERMISSION_REQUEST_CODE = 0;
    //@Inject
    KeyguardManager mKeyguardManager;

    FingerprintManager fingerprintManager;// = getSystemService(FingerprintManager.class);
    //@Inject
    FingerprintAuthenticationDialogFragment mFragment;
    //@Inject
    java.security.KeyStore mKeyStore;
    //@Inject
    KeyGenerator mKeyGenerator;
    //@Inject
    Cipher mCipher;
    //@Inject
    SharedPreferences mSharedPreferences;
    //Button purchaseButton;
    boolean showLinkTouchID = false, apploginCalled = false;

    boolean touchIdRegistered1 = false;
    boolean touchIdDeviceSupport1 = false;
    boolean hasCredential1 = false;
    String setUpTouchIdLater1 = "0";

    ImageView bg_img;//,ladbrokes_logo;
    RelativeLayout bottom_relative_layout, bottom_parent_layout;
    public RelativeLayout cashout_slideshow_layout;

    public FrameLayout webview_layout;

    public LinearLayout webview_layout1;

    Context activity_context;
    boolean isTablet;

    String test = "";
    boolean /*postLoginReceived = false,*/ loginLoading = false, isAutoLogin = false, loginDataReceived = false, doLogin = false;/*  loginLoading = false, postLoginReceived = false, refreshCalled = false*/
    boolean newUserMayLogin = true, registration = false;
    boolean differentUser = false;
    //WebContainer webContainer;
    boolean settingsCalled = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
//		Crashlytics.start(this);
        // Fabric.with(this, new Crashlytics());
        isTablet = isTablet(this);
        FingerprintModule fingerprintModule = new FingerprintModule(BrowserActivity.this);
        mKeyguardManager = fingerprintModule.providesKeyguardManager();
        mKeyStore = fingerprintModule.providesKeystore();
        mKeyGenerator = fingerprintModule.providesKeyGenerator();
        mCipher = fingerprintModule.providesCipher(mKeyStore);
        mSharedPreferences = fingerprintModule.providesSharedPreferences();
        mFragment = new FingerprintAuthenticationDialogFragment();
        //Log.e("","OnCreate 111 ");

        // try{scanUfoHandler.post(scanUfoRunnable); }catch(Exception e){}

        activity_context = this;
        textToSpeech = new TextToSpeech(this, this);
        //isTablet=false;
        //if(isTablet==true)
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

//     	  startService(new Intent(this, MyService.class));

        try {
            mAppKey = OlAndroidLibrary.getInstance(this).getAppKey();
        } catch (Exception e) {
        }

        android_id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
        // deviceId1 = ((TelephonyManager) getSystemService(this.TELEPHONY_SERVICE)).getDeviceId();


        progressDialog2 = MyCustomProgressDialog.ctor(this);

        mContent = (View) getLastCustomNonConfigurationInstance();
        if (mContent == null) {
            LayoutInflater inflater = getLayoutInflater().cloneInContext(new MutableContextWrapper(this));
            ViewGroup parent = (ViewGroup) getWindow().getDecorView().findViewById(Window.ID_ANDROID_CONTENT);
            mContent = inflater.inflate(R.layout.browser_activity_main, parent, false);
            setContentView(mContent);
        } else {
            MutableContextWrapper context = (MutableContextWrapper) mContent.getContext();
            context.setBaseContext(this);
            setContentView(mContent);
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            LearningAppApplication learningAppApplication = (LearningAppApplication) getApplicationContext();
            //learningAppApplication.inject(this);

            requestPermissions(new String[]{Manifest.permission.USE_FINGERPRINT}, FINGERPRINT_PERMISSION_REQUEST_CODE);
            // touchIdDeviceSupport1=true;

            fingerprintManager = getSystemService(FingerprintManager.class);
        }
		/*
        else
        {
         	  touchIdDeviceSupport1=false;

        }*/
        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        mWebView = (WebView) findViewById(R.id.web_view);
        //webContainer = new WebContainer(this, mWebView);
        bg_img = (ImageView) findViewById(R.id.bg_img);
        bg_img.setVisibility(View.INVISIBLE);
        //ladbrokes_logo = (ImageView)findViewById(R.id.ladbrokes_logo);

        cashout_slideshow_layout = (RelativeLayout) findViewById(R.id.cashout_slideshow_layout);
        webview_layout = (FrameLayout) findViewById(R.id.webview_layout);

        webview_layout1 = (LinearLayout) findViewById(R.id.webview_layout1);


        mViewPager = (ViewPager) findViewById(R.id.view_pager);

        // mViewPager = (ViewPager) view.findViewById(R.id.view_pager);

        setSplashImage();

        //bg_img.setVisibility(View.GONE);





        /*  	Bottom Menu Hide Start */

        bottom_relative_layout = (RelativeLayout) findViewById(R.id.button_layout);

        bottom_parent_layout = (RelativeLayout) findViewById(R.id.bottom_parent_layout);
        display = getWindowManager().getDefaultDisplay();
        bottom_parent_layout.setVisibility(View.INVISIBLE);
        //  r_parms = new RelativeLayout.LayoutParams(width,height);

        //LinearLayout.LayoutParams parms2 = new LinearLayout.LayoutParams(width,height);
        //bg_img.setLayoutParams(parms);
        //customViewContainer.setLayoutParams(parms2);

        /*  Bottom Menu Hide End */


        WebSettings webSettings = mWebView.getSettings();
        //webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setUseWideViewPort(true);
        webSettings.setSupportZoom(true);
        webSettings.setDatabaseEnabled(true);

        webSettings.setDomStorageEnabled(true);

        webSettings.setJavaScriptEnabled(true);

        webSettings.setBuiltInZoomControls(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setPluginState(PluginState.ON);
        mWebView.addJavascriptInterface(new WebAppInterface(this), "Android");
        //mWebView.addJavascriptInterface(new JavascriptNativeBridge(this), "androidBridge");
        mWebView.addJavascriptInterface(new JavascriptNativeBridge(this), "JsNativeBridge");

        webSettings.setRenderPriority(RenderPriority.HIGH);
        //webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        //enableHTML5AppCache();
        mWebView.setInitialScale(1);

        webSettings.setBuiltInZoomControls(true);
        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.setScrollbarFadingEnabled(true);
        //mWebView.requestFocus(View.FOCUS_DOWN);
        mWebView.setBackgroundColor(0);

        mWebView.setWebContentsDebuggingEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE))

            WebView.setWebContentsDebuggingEnabled(true);
        }

        webSettings.setSupportMultipleWindows(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);


        mWebView.setWebViewClient(mWebViewClient);
        //mWebView.setWebChromeClient(mwebChromeClient);

        cookieSyncManager = CookieSyncManager.createInstance(mWebView.getContext());
        cookieManager = CookieManager.getInstance();


        mobileAppTracker = MobileAppTracker.init(getApplicationContext(),
                "13306",   //your_advertiser_ID
                "5a920829272e0b0db47101ff2cca302c"     //your_conversion_key
        );


        mobileAppTracker = MobileAppTracker.getInstance();

        //mobileAppTracker.setDeviceId(deviceId1);


        String finalUrl1 = handleCustomIntent(getIntent());

        mWebChromeClient = new myWebChromeClient();

        mWebView.getSettings().setGeolocationEnabled(true);
        mWebView.setWebChromeClient(mWebChromeClient);

        // 	String finalUrl1= finalUrl();


        Bundle extras = getIntent().getExtras();


        if (extras == null) {
            if (savedInstanceState == null) {
                if (cookieManager != null) {
                    try {
                        cookieManager.removeSessionCookie();
                        /*cookieManager.removeAllCookie();
                        cookieManager.removeExpiredCookie();*/
                        cookieManager.flush();
                        // cookieManager=null;
                        // cookieSyncManager=null;
                        saveGridEnabled(false);
                    } catch (Exception e) {
                    }
                }

                mWebView.setVerticalScrollBarEnabled(true);
                //mWebView.loadUrl(finalUrl1);
                loadWebViewUrl(activity_context, mWebView, finalUrl1);

                checkVersion();


            } else {
                setTitle(mWebView.getTitle());
                //home_click=false;
            }
        } else {

            betSlipUrl = extras.getString("betslip_url");
            qr_code_correct = extras.getBoolean("qr_code_correct");
            // betSlipUrl="https://www.google.com";
            try {
                if (betSlipUrl.equalsIgnoreCase("") || betSlipUrl == null || betSlipUrl == "") {
                    //mWebView.loadUrl(finalUrl1);
                    loadWebViewUrl(activity_context, mWebView, finalUrl1);
                } else {
                    hideFooterMenu();
                    callCashOutWebView1(betSlipUrl);
                    //Log.e("", "22  betSlipUrl--  "+betSlipUrl);
                }
            } catch (Exception e) {
                try {
                    String pushUrl = extras.getString("URL");
                    if (pushUrl.equalsIgnoreCase("") || pushUrl == null || pushUrl == "" || pushUrl.equalsIgnoreCase(null)) {
                        // Log.e("","Inside Local Build  pushUrl-  111 -");
                        if (cookieManager != null) {
                            try {
                                cookieManager.removeSessionCookie();
                                /*cookieManager.removeAllCookie();
                                cookieManager.removeExpiredCookie();*/
                                cookieManager.flush();
                                saveGridEnabled(false);
                                // cookieManager=null;
                                // cookieSyncManager=null;
                            } catch (Exception e1) {
                            }
                        }
                    }
                } catch (Exception e1) {
                    //Log.e("","Inside Local Build  Exception pushUrl-  111 -");
                    if (cookieManager != null) {
                        try {
                            cookieManager.removeSessionCookie();
                            /*cookieManager.removeAllCookie();
                            cookieManager.removeExpiredCookie();*/
                            cookieManager.flush();
                            saveGridEnabled(false);
                            // cookieManager=null;
                            // cookieSyncManager=null;
                        } catch (
                                Exception e2) {

                        }
                    }
                }

                mWebView.setVerticalScrollBarEnabled(true);
                hideFooterMenu();
                //mWebView.loadUrl(finalUrl1);
                loadWebViewUrl(activity_context, mWebView, finalUrl1);
                //home_click=true;
            }
        }

        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);

        // gaTrackingFirstValue=prefs.getBoolean(AppConfig.GA_TRACKING_FIRST_TIME, true);
        //MarketingCloudSdk.requestSdk(this);
    }

    //String adDeviceId="";
    public void getAdId() {

        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                AdvertisingIdClient.Info idInfo = null;
                try {
                    idInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String advertId = null;
                try {
                    advertId = idInfo.getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return advertId;
            }

            @Override
            protected void onPostExecute(String advertId) {
                //Toast.makeText(getApplicationContext(), advertId, Toast.LENGTH_SHORT).show();
                //adDeviceId=advertId;
                setGoogleAdId(advertId);
            }
        };
        task.execute();


    }


    //boolean gaTrackingFirstValue=true;
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }


    private void setMargins(View view, int left, int top, int right, int bottom) {

        //Log.e("","11  setMargins  --   top--  "+top);
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            final float scale = this.getResources().getDisplayMetrics().density;
            // convert the DP into pixel
            int pixel = (int) (top * scale + 0.5f);
            //Log.e("","setMargins  --   top--  "+top+"  pixel--  "+pixel);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, pixel, right, bottom);
            view.requestLayout();
        }
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private static final int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.USE_FINGERPRINT};

    public boolean checkAllPermission() {
        boolean activePermission = false;
        try {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
                activePermission = false;
            } else
                activePermission = true;
        } catch (Exception e) {
            activePermission = false;
        }
        return activePermission;

    }


    public void setSplashImage() {
        // Log.e("", "Brow setSplashImage isTablet  "+isTablet);
        Drawable image;

        isTablet = isTablet(this);
        if (isTablet == true) {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                //Log.e("","Tablet portrait  BG  ---");
                image = (Drawable) getResources().getDrawable(R.drawable.bg_tablet_portrait);
                //progressDialog2 = new MyCustomProgressDialog(this);
                progressDialog2.show();

            } else {
                //Log.e("","Brows Tablet landscape  BG  ---");
                image = (Drawable) getResources().getDrawable(R.drawable.bg_tablet_landscape);
                //progressDialog2 = new MyCustomProgressDialog(this);
                progressDialog2.show();
            }
        } else {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                //Log.e("","Brow Mobile portrait  BG  ---");
                image = (Drawable) getResources().getDrawable(R.drawable.bg_mobile_portrait);
                //progressDialog2 = new MyCustomProgressDialog(this);
                progressDialog2.show();
            } else {
                //Log.e("","Brow Mobile landscape  BG  ---");
                image = (Drawable) getResources().getDrawable(R.drawable.bg_mobile_landscape);
                // progressDialog2 = new MyCustomProgressDialog(this);
                progressDialog2.show();
            }
        }

        //bg_img.setBackgroundDrawable(image);

    }


    int setBottomMargin = 83;
    LinearLayout.LayoutParams l_parms;
    Display display;
    int width, height;

    float h1;

    public void hideFooterMenu() {
        try {

            prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
            showFooter3 = prefs.getString(AppConfig.SHOW_FOOTER, "0");


            width = display.getWidth();

            // Logger("hideFooterMenu   showFooter    "+showFooter3);

            h1 = convertDpToPixel(setBottomMargin, this);
            if (showFooter3.equalsIgnoreCase("1")) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                height = (int) (display.getHeight() - h1);
                l_parms = new LinearLayout.LayoutParams(width, height);
                bottom_parent_layout.setVisibility(View.VISIBLE);
            } else {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                bottom_parent_layout.setVisibility(View.GONE);
            }
            mWebView.setLayoutParams(l_parms);
            //bg_img.setVisibility(View.GONE);
        } catch (Exception e) {

        }
    }


    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }


    SharedPreferences prefs;

    public void callCashOutWebView1(String betSlipUrl) {


        //betSlipUrl="http://local-mobile.ladbrokes.com/en?grid=1&clientType=GRD_AND_APP&barcode=7727174597797571";

        // betSlipUrl="https://mobile2.ladbrokes.com/en?grid=1&clientType=GRD_AND_APP&betslipkey=38076724857714";

        Log.e("", "callCashOutWebView1  --   " + betSlipUrl);

        loadingFirst = false;


        //mWebView.loadUrl(betSlipUrl);

        loadWebViewUrl(activity_context, mWebView, betSlipUrl);
        if (qr_code_correct == false)
            Toast.makeText(this, "Invalid QR Code", Toast.LENGTH_LONG).show();
        //Log.e("","BrowserActovity   callCashOutWebView--  betSlipUrl--   "+betSlipUrl);


    }


    public void loadWebViewUrl(Context context, WebView web_view, String web_url) {
        web_view.loadUrl(web_url);
    }


    public class myWebChromeClient extends WebChromeClient {
        //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onPermissionRequest(final PermissionRequest request) {
			/*if (ActivityCompat.checkSelfPermission(activity_context, "android.permission.CAMERA") != 0) {
				ActivityCompat.requestPermissions(BrowserActivity.this, new String[] { "android.permission.READ_EXTERNAL_STORAGE" }, 4);
			}
			BrowserActivity.this.runOnUiThread(new Runnable()
			{
				public void run()
				{
					PermissionRequest localPermissionRequest = request;
					localPermissionRequest.grant(localPermissionRequest.getResources());
				}
			});
			checkAllPermission();*/
//			AESHelper.checkAndRequestPermissions(BrowserActivity.this);
            request.grant(request.getResources());
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            mUploadMessages = filePathCallback;
            if (openImageChooser()) {
                return true;
            }
            return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
        }


        @Override
        public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
            WebView newWebView = new WebView(view.getContext());

            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }
            newWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.equalsIgnoreCase("https://sports-stg2.ladbrokes.com/lotto") ||
                            url.equalsIgnoreCase("https://lottos.ladbrokes.com/en")) {
                        //url = "https://sports-hl.ladbrokes.com/lotto";
                        url = "https://sports.ladbrokes.com/lotto";
                    }

                    /*if(!url.startsWith(“http:”) || !url.startsWith(“https:”) ){

                    }*/
                    test = url;
                    //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData(Uri.parse(url));
                    startActivity(browserIntent);
                    return true;
                }

                @Override
                public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                    AESHelper.showSSLErrorDialog(handler, error, activity_context);
                }
            });
            String testingUrl = test;
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newWebView);
            resultMsg.sendToTarget();
            return true;
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            // Always grant permission since the app itself requires location
            // permission and the user has therefore already granted it
            callback.invoke(origin, true, false);
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }

            mCustomView = view;

            bg_img.setBackgroundColor(Color.rgb(0, 0, 0));
            mWebView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            bg_img.setBackgroundColor(Color.rgb(0, 0, 0));

            mWebView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
            // customViewContainer.refreshDrawableState();
        }
    }

    public void Logger(String log) {
        Log.e(TAG, log);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] state) {

        try {
            if (requestCode == FINGERPRINT_PERMISSION_REQUEST_CODE)// && state[0] == PackageManager.PERMISSION_GRANTED)
            {

	   				/*
	    		   if(!fingerprintManager.isHardwareDetected())
		            {
		                return;
		            }*/

                if (!createKey()) {
                    return;
                }
                if (!fingerprintManager.hasEnrolledFingerprints()) {

                    return;
                }

            }


        } catch (Exception e) {

        }

    }


    public boolean isHardwareDetected1() {
        try {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (!fingerprintManager.isHardwareDetected()) {
                    return false;
                } else
                    return true;
            } else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean hasEnrolledFingerprints() {
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    return false;
                } else
                    return true;
            } else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void callSetUpTouchId() {
        try {
            Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS);
            startActivity(callGPSSettingIntent);
            setup_touch_id_button_called = true;
        } catch (Exception e) {

        }
    }

    boolean setup_touch_id_button_called = false;

    public void callFingerPrint() {
        try {
            findViewById(R.id.confirmation_message).setVisibility(View.GONE);
            findViewById(R.id.encrypted_message).setVisibility(View.GONE);

            // Set up the crypto object for later. The object will be authenticated by use
            // of the fingerprint.
            if (initCipher()) {
                // Show the fingerprint dialog. The user has the option to use the fingerprint with
                // crypto, or you can fall back to using a server-side verified password.
                mFragment.setCryptoObject(new android.hardware.fingerprint.FingerprintManager.CryptoObject(mCipher));
                boolean useFingerprintPreference = mSharedPreferences.getBoolean(getString(R.string.use_fingerprint_to_authenticate_key1), true);
                if (useFingerprintPreference) {
                    mFragment.setStage(FingerprintAuthenticationDialogFragment.Stage.FINGERPRINT);
                } else {
                    mFragment.setStage(FingerprintAuthenticationDialogFragment.Stage.PASSWORD);
                }
            } else {
                // This happens if the lock screen has been disabled or or a fingerprint got
                // enrolled. Thus show the dialog to authenticate with their password first
                // and ask the user if they want to authenticate with fingerprints in the
                // future
                mFragment.setCryptoObject(new android.hardware.fingerprint.FingerprintManager.CryptoObject(mCipher));
                mFragment.setStage(FingerprintAuthenticationDialogFragment.Stage.NEW_FINGERPRINT_ENROLLED);
            }
            Fragment prev = getFragmentManager().findFragmentByTag(DIALOG_FRAGMENT_TAG);
            mFragment.setCancelable(false);
            if (prev == null) {
                mFragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
            }

        } catch (Exception e) {

        }

    }

    /**
     * Initialize the {@link Cipher} instance with the created key in the {@link #createKey()}
     * method.
     *
     * @return {@code true} if initialization is successful, {@code false} if the lock screen has
     * been disabled or reset after the key was generated, or if a fingerprint got enrolled after
     * the key was generated.
     */
    private boolean initCipher() {
        try {

            try {
                mKeyStore.load(null);
                SecretKey key = (SecretKey) mKeyStore.getKey(KEY_NAME, null);

                mCipher.init(Cipher.ENCRYPT_MODE, key);

                Enumeration<String> aliases = mKeyStore.aliases();

                return true;
            } catch (Exception e) {
                return false;
            }

        } catch (Exception e) {
            return false;
        }

    }


    // Show confirmation, if fingerprint was used show crypto information.
    private void showConfirmation(byte[] encrypted) {
        findViewById(R.id.confirmation_message).setVisibility(View.VISIBLE);
        if (encrypted != null) {
            TextView v = (TextView) findViewById(R.id.encrypted_message);
            v.setVisibility(View.VISIBLE);
            v.setText(Base64.encodeToString(encrypted, 0));
        }
    }

    /**
     * Tries to encrypt some data with the generated key in {@link #createKey} which is
     * only works if the user has just authenticated via fingerprint.
     */
    private void tryEncrypt() {
        try {
            byte[] encrypted = mCipher.doFinal(SECRET_MESSAGE.getBytes());
            showConfirmation(encrypted);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            Toast.makeText(this, "Failed to encrypt the data with the generated key. "
                    + "Retry the purchase", Toast.LENGTH_LONG).show();

            Logger("Failed to encrypt the data with the generated key." + e.getMessage());
        }
    }

	    /*
	    public boolean createKey(boolean invalidatedByBiometricEnrollment) {
	        // The enrolling flow for fingerprint. This is where you ask the user to set up fingerprint
	        // for your flow. Use of keys is necessary if you need to know if the set of
	        // enrolled fingerprints has changed.
	        try {
	            mKeyStore.load(null);
	            // Set the alias of the entry in Android KeyStore where the key will appear
	            // and the constrains (purposes) in the constructor of the Builder

	            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(KEY_NAME,
	                    KeyProperties.PURPOSE_ENCRYPT |
	                            KeyProperties.PURPOSE_DECRYPT)
	                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
	                    // Require the user to authenticate with a fingerprint to authorize every use
	                    // of the key
	                    .setUserAuthenticationRequired(true)
	                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7);

	            // This is a workaround to avoid crashes on devices whose API level is < 24
	            // because KeyGenParameterSpec.Builder#setInvalidatedByBiometricEnrollment is only
	            // visible on API level +24.
	            // Ideally there should be a compat library for KeyGenParameterSpec.Builder but
	            // which isn't available yet.
	            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
	            {
	                builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);
	            }
	            mKeyGenerator.init(builder.build());
	            mKeyGenerator.generateKey();
	            return true;
	        }
	        catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException
	                | CertificateException | IOException e)
	        {

	           // throw new RuntimeException(e);
	        	return false;
	        }



	    }
	    */

    public boolean createKey() {
        // The enrolling flow for fingerprint. This is where you ask the user to set up fingerprint
        // for your flow. Use of keys is necessary if you need to know if the set of
        // enrolled fingerprints has changed.

        try {

            mKeyStore.load(null);

            // Set the alias of the entry in Android KeyStore where the key will appear
            // and the constrains (purposes) in the constructor of the Builder
            mKeyGenerator.init(new android.security.keystore.KeyGenParameterSpec.Builder(KEY_NAME,
                    android.security.keystore.KeyProperties.PURPOSE_ENCRYPT |
                            android.security.keystore.KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(android.security.keystore.KeyProperties.BLOCK_MODE_CBC)
                    // Require the user to authenticate with a fingerprint to authorize every use
                    // of the key
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(android.security.keystore.KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            mKeyGenerator.generateKey();

            return true;
        } catch (IllegalStateException e) {
            // This happens when no fingerprints are registered.

            // Toast.makeText(this,"Go to 'Settings 2-> Security -> Fingerprint' and register at least one fingerprint", Toast.LENGTH_LONG).show();
            return false;
        }
	        /*
	        catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | CertificateException | IOException e)
	        {
	           // throw new RuntimeException(e);
	             Toast.makeText(this,"11  Go to 'Settings 2-> Security -> Fingerprint' and register at least one fingerprint", Toast.LENGTH_LONG).show();
	            return false;
	        }*/ catch (Exception e) {
            // throw new RuntimeException(e);

            //if(touchIdDeviceSupport1==true)
            //Toast.makeText(this," Go to 'Settings -> Security -> Fingerprint' and register at least one fingerprint", Toast.LENGTH_LONG).show();
            return false;
        }


    }

    //String userName3="";
//String passWord3="";
    String saveRealCredential3 = "";

    //String autoLogin3="";
    public void saveUserPreference2(String userName2, String passWord2, String saveRealCredential, String autoLogin, String inShop) {

        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);

        hasCredential1 = prefs.getBoolean(AppConfig.HAS_CREDENTIAL, false);
        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();

        editor.putString(AppConfig.USER_NAME, userName2);

        if (inShop.equalsIgnoreCase("1")) {
            //passWord2="1991-01-01";
            editor.putString(AppConfig.IN_SHOP_PASSWORD, passWord2);
        } else
            editor.putString(AppConfig.ONLINE_USER_PASSWORD, passWord2);


        editor.putString(AppConfig.IN_SHOP, inShop);
        editor.putString(AppConfig.AUTO_LOGIN, autoLogin);
        editor.commit();


        credentialSavedSuccessPopup(saveRealCredential);

    }


    public void saveSetUpLaterPreference(String setuptouchLater) {


        Logger("saveSetUpLaterPreference  setuptouchLater --    " + setuptouchLater);

        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
        editor.putString(AppConfig.SETUP_LATER, setuptouchLater);
        editor.commit();

    }


    public void saveGridEnabled(boolean saveGridEnabled) {


        //Logger("saveGridEnabled  saveGridEnabled --    "+saveGridEnabled);

        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(AppConfig.IS_GRID_ENABLED, saveGridEnabled);
        editor.commit();

    }


    public void saveHasCredentialPreference1(boolean hasCredential) {

        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();

        //editor.putString(AppConfig.HAS_CREDENTIAL, hasCredential);

        editor.putBoolean(AppConfig.HAS_CREDENTIAL, hasCredential);

        editor.commit();

    }


    public void getAppCashoutTutorialValue2(final BrowserActivity mContext) {
        Log.e("", "Call  getAppCashoutTutorialValue 11  ");
        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
        editor.putBoolean(AppConfig.CASHOUT_BETSLIP_SCANNED, true);
        editor.commit();


        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);

        boolean firstTimeCashoutLaunchValue = prefs.getBoolean(AppConfig.FIRST_TIME_CASHOUT_LAUNCH, true);
        //boolean cashout_betslip_scanned =prefs.getBoolean(AppConfig.CASHOUT_BETSLIP_SCANNED, false);

        //editor.putBoolean(AppConfig.CASHOUT_BETSLIP_SCANNED, cashout_betslip_scanned);
        //Log.e("", "00  firstTimeCashoutLaunchValue--   "+firstTimeCashoutLaunchValue);//+"    cashout_betslip_scanned--  "+cashout_betslip_scanned);

        //firstTimeCashoutLaunchValue=true;

        if (firstTimeCashoutLaunchValue == true)// && cashout_betslip_scanned==true)
        {
            Log.e("", "Inside   firstTimeCashoutLaunchValue--   " + firstTimeCashoutLaunchValue);//+"    cashout_betslip_scanned--  "+cashout_betslip_scanned);

            SharedPreferences.Editor editor1 = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
            editor1.putBoolean(AppConfig.FIRST_TIME_CASHOUT_LAUNCH, false);
            editor1.commit();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    webview_layout.setVisibility(View.INVISIBLE);
                    cashout_slideshow_layout.setVisibility(View.VISIBLE);
                    mViewPager.setAdapter(new CashoutImageSlideAdapter(mContext));
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    hideImageLoading();
                    //Log.e("", "22  firstTimeCashoutLaunchValue-- 22 ");
                }
            });

        }

    }


//boolean bankingIconEnableFromJavaScript=false;
//boolean myCardIconEnableFromJavaScript=false;

    boolean tmpisBankingEnable = false, tmpisGridCardEnable = false;


    public void convertBarcodeIntoBetslipNumber(String barcode_number) {
        Log.e(TAG1, " 111  convertBarcodeIntoBetslipNumber   " + barcode_number);


        //mWebView.loadUrl("javascript:convertBarcodeIntoBetslipNumber(\""+barcode_number+"\")");


        //mWebView.loadUrl("javascript:if(typeof convertBarcodeIntoBetslipNumber !== \"function\"){document.getElementById('bet-scanner-iframe').src = document.getElementById('bet-scanner-iframe').src+'&barcode='+\"" + barcode_number + "\";}else {convertBarcodeIntoBetslipNumber(\"" + barcode_number + "\")}");
        String tempUrl = "javascript:if(typeof convertBarcodeIntoBetslipNumber !== \"function\"){document.getElementById('bet-scanner-iframe').src = document.getElementById('bet-scanner-iframe').src+'&barcode='+\"" + barcode_number + "\";}else {convertBarcodeIntoBetslipNumber(\"" + barcode_number + "\")}";
        Log.e("BetResult", "  barcode_number--  " + tempUrl);

        mWebView.loadUrl(tempUrl);
        //mWebView.loadUrl("javascript:convertBarcodeIntoBetslipNumber(\""+barcode_number+"\")");


        Log.e(TAG1, " 222  convertBarcodeIntoBetslipNumber   " + barcode_number);

        //mWebView.loadUrl("javascript:document.getElementById('bet-scanner-iframe').src = document.getElementById('bet-scanner-iframe').src+'&barcode='+\"B4087303913803\";");


        //mWebView.loadUrl("javascript:document.getElementById('bet-scanner-iframe').src = document.getElementById('bet-scanner-iframe').src+'&barcode='+\"B3216282502588\"");


        //if(typeof convertBarcodeIntoBetslipNumber !== "function"){document.getElementById('bet-scanner-iframe').contentWindow.postMessage(""+barcode_number+"", window.location.protocol+window.location.hostname);}


        //mWebView.loadUrl("javascript:top.frames['bet-scanner-iframe'].contentDocument.convertBarcodeIntoBetslipNumber(\""+barcode_number+"\")");


        //top.frames['bet-scanner-iframe'].contentDocument.convertBarcodeIntoBetslipNumber();


    }


    public void changeNewPassword(String tmp_passWord) {
        Log.e(TAG, " changeNewPassword  tmp_passWord-- " + tmp_passWord);


        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
        String doAutoLoginValue = prefs.getString(AppConfig.AUTO_LOGIN, "");

        String inShop = prefs.getString(AppConfig.IN_SHOP, "");

        String userName2 = prefs.getString(AppConfig.USER_NAME, "");

        String password2 = prefs.getString(AppConfig.ONLINE_USER_PASSWORD, "");

        // Logger("11 changePasswordSetIt  tmp_passWord-- "+tmp_passWord+"  userName2--"+userName2+"   tmp_passWord--  "+tmp_passWord);

        if (tmp_passWord == null || tmp_passWord == "" || tmp_passWord.equalsIgnoreCase("") || tmp_passWord.equalsIgnoreCase(null))
            tmp_passWord = password2;

        String saveRealCredential = "0";

        Logger("22 changePasswordSetIt  AA--   userName2--" + userName2 + "   tmp_passWord--  " + tmp_passWord);

        saveUserPreference2(userName2, tmp_passWord, saveRealCredential, doAutoLoginValue, inShop);  //mitesh


    }

    boolean isUniversalLink = false;
    String autoLogin1 = "";

    public String finalUrl() {

        String final_Url = "";
        LanguageName = checkLanguage1(AppConfig.HOME_URL);

        String url = replace(AppConfig.HOME_URL, AppConfig.LANGUAGE, LanguageName);

        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
        hasCredential1 = prefs.getBoolean(AppConfig.HAS_CREDENTIAL, false);

        setUpTouchIdLater1 = prefs.getString(AppConfig.SETUP_LATER, "0");
        autoLogin1 = prefs.getString(AppConfig.AUTO_LOGIN, "0");

        // Logger("---  finalUrl autoLogin1------  "+autoLogin1);

        if (showNotificationUrl == true)
            hasCredential1 = false;

        try {
            Uri data1 = getIntent().getData();
            String scheme = data1.getScheme(); // "http"
            String host = data1.getHost(); // "twitter.com"
            String path = data1.getPath();

            final_Url = scheme + "://" + host + path;

            boolean mobile2 = isWordAvailable(host, "mobile2");

            if (mobile2 == true)
                final_Url = scheme + "://" + host + path + "?grid=1&" + AppConfig.END_EXT;
            else
                final_Url = scheme + "://" + host + path + "?" + AppConfig.END_EXT;

            boolean return_en1 = isWordAvailable(final_Url, "/en");
            boolean return_ie1 = isWordAvailable(final_Url, "/ie");

            if (return_en1 == false && return_ie1 == false)
                final_Url = scheme + "://" + host + path + LanguageName + "?" + AppConfig.END_EXT;

            isUniversalLink = true;
        } catch (Exception e) {
            final_Url = url;
            isUniversalLink = false;
        }

        boolean nfc_keyword = isWordAvailable(final_Url, "nfc");

        if (nfc_keyword == true)
            final_Url = url;

        boolean createKeyInsideFinalUrl = false;
        try {
            createKeyInsideFinalUrl = hasEnrolledFingerprints();

        } catch (Exception e) {
            createKeyInsideFinalUrl = false;
        }

        try {
            touchIdDeviceSupport1 = isHardwareDetected1();
        } catch (Exception e) {
            touchIdDeviceSupport1 = false;
        }


        // touchIdDeviceSupport1=true;
        //Logger("FinalUrl  createKeyInsideFinalUrl  --   "+createKeyInsideFinalUrl+"  touchIdDeviceSupport1--  "+touchIdDeviceSupport1);

        if (createKeyInsideFinalUrl == false)
            touchIdRegistered1 = false;
        else
            touchIdRegistered1 = true;

// rm == autoLogin1 == 1
        //touchIdRegistered1 = true;
        //hasCredential1 = false;
        /*if(){

        }*/
        String cookieGlob = cookieManager.getCookie("https://ladbrokes.com");
        //Log.i("remembermecheck", cookieGlob);
        if (cookieGlob != null) {
            boolean rm_h = cookieGlob.contains("rm-h");
            boolean rm_i = cookieGlob.contains("rm-i");
            if (rm_h || rm_i) {
                autoLogin1 = "1";
            } else {
                autoLogin1 = "0";
            }
        }

        if (setUpTouchIdLater1.equalsIgnoreCase("0"))
            final_Url = final_Url + "&touchIdRegistered=" + touchIdRegistered1 + "&hasCrendential=" + hasCredential1 + "&touchIdDeviceSupport=" + touchIdDeviceSupport1 + "&setUpTouchIdLater=" + setUpTouchIdLater1 + "&doAutoLogin=" + autoLogin1 + "&versionHandleFromWeb=1";
        else {
            setUpTouchIdLater1 = "1";
            final_Url = final_Url + "&touchIdRegistered=" + touchIdRegistered1 + "&hasCrendential=" + hasCredential1 + "&touchIdDeviceSupport=" + touchIdDeviceSupport1 + "&setUpTouchIdLater=" + setUpTouchIdLater1 + "&doAutoLogin=" + autoLogin1 + "&versionHandleFromWeb=1";
        }
        final_Url = final_Url + "&enableAutoLogin=1";
        Logger(" finalUrl1()------  " + final_Url);
        // writeTextFile2(final_Url,"Final_Url");
        return final_Url;
    }

    android.view.ViewGroup.LayoutParams params;
    private HorizontalListView mHlvCustomListWithDividerAndFadingEdge;

    private void setupCustomLists() {
        // Make an array adapter using the built in android layout to render a list of strings

        mHlvCustomListWithDividerAndFadingEdge = (HorizontalListView) findViewById(R.id.hlvCustomListWithDividerAndFadingEdge);

        CustomArrayAdapter adapter = new CustomArrayAdapter(this, mCustomData);

        mHlvCustomListWithDividerAndFadingEdge.setAdapter(adapter);

        mHlvCustomListWithDividerAndFadingEdge.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                callBottomButtons(position);
            }
        });
    }

    int version_msg = 0;

    public void displayDialog() {
        Force_Action = "1";
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (Force_Action.equals("0"))
            //  version_msg="A new version of The Grid app is available. Please click OK to upgrade or Cancel to stay in the same version.";
            version_msg = R.string.version_change_optional_message;

        else
            //version_msg="A new version of The Grid app is available. Please click OK to upgrade or Cancel to close the app.";
            version_msg = R.string.version_change_mandatory_message;

        builder.setTitle("Alert").setMessage(version_msg)
                .setCancelable(false)
                .setIcon(R.drawable.alert)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String url = DownloadUrl;
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                startActivity(browserIntent);
                                finish();

                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Force_Action.equals("0")) {
                            Log.e("", "onClick call_finger_print_popup--   " + call_finger_print_popup);

                            SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                            editor.putBoolean("ForceCancel", true);
                            editor.commit();
                            dialog.cancel();

                            if (call_finger_print_popup == true) {
                                //callFingerPrint();
                            }
                        } else if (Force_Action.equals("1"))
                            finish();
                        else {
                            dialog.cancel();
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    boolean call_finger_print_popup = false;

    /*
	  @Override
		public void onNewIntent(Intent intent)
		{
		   // Log.e("", "onNewIntent  11  ");
		        super.onNewIntent(intent);
		    setIntent(intent);
		    handleCustomIntent1(intent);

		   // onNewIntentForNFC(intent);
		}
		*/
    boolean showNotificationUrl = false;

    public String handleCustomIntent(Intent intent) {

        String final_url = "";


        if (intent.getExtras() == null) {
            final_url = finalUrl();
        } else {

            //OlAndroidLibrary.getInstance(this).registerIntent(intent);
            // Log.e("", "handleCustomIntent  11");
            //  String url = intent.getStringExtra("URL");

            String url = "";
            NotificationMessage notificationMessage = NotificationManager.extractMessage(getIntent());
            Logger("handleCustomIntent1 handleCustomIntent1 else  url  " + url);

            if (notificationMessage != null && notificationMessage.type() == NotificationMessage.Type.OPEN_DIRECT) {
                url = notificationMessage.url();
            }


            try {

                if (url.startsWith("http:") || url.startsWith("https:")) {
                    // Log.e("", "handleCustomIntent  33 - "+url);
                    Intent intent1 = new Intent(BrowserActivity.this, ShowDeepLinkWebView.class);
                    intent1.putExtra("urlConection", url);
                    startActivity(intent1);
                    final_url = url;
                    showNotificationUrl = true;

                } else {

                    final_url = finalUrl();
                    // Log.e("", "handleCustomIntent  44 final_url - "+final_url);
                }
            } catch (Exception e) {
                final_url = finalUrl();
                // Log.e("", "handleCustomIntent Exception  55 final_url - "+final_url);
            }


        }
        Logger("  handleCustomIntent  last  final_url--   " + final_url);
        return final_url;
    }


    public void callShare() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "The Grid");
        i.putExtra(Intent.EXTRA_TEXT, "Bet In-Shop & Track your bets on the go. Download the Grid App Now! - https://thegrid.ladbrokes.com/");
        try {
            startActivity(Intent.createChooser(i, "Share"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no clients installed.",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;

    }


    protected void checkVersion() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        Current_VersionNumber = pInfo.versionName;
        int version = pInfo.versionCode;

        try {
            String url = (AppConfig.VERSION_CHECK_URL);
            //displayDialog();
            //new DownloadVersionCheck().execute(url);
            /*Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    versionCheck();
                }
            });*/
            //t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String Update_Action = "";
    String Force_Action = "";
    String Latest_VersionNumber = "";
    String DownloadUrl = "";
    String showFooter3 = "0";


    String Current_VersionNumber = "1.0.0";
    boolean newVersionAvailable = false;

    String currentSeperatedValue[];
    String latestSeperatedValue[];
    int currentCount = 0;
    int latestCount = 0;

    public boolean checkNewVersionAvailable(String currentValue, String latestValue) {
        boolean newVersion = false;
        try {
            if (currentValue.equalsIgnoreCase(latestValue))
                newVersion = false;
            else {

                currentValue = currentValue.replace('.', ':');
                latestValue = latestValue.replace('.', ':');
                currentSeperatedValue = new String[10];
                latestSeperatedValue = new String[10];

                for (String s : latestValue.split(":")) {
                    latestSeperatedValue[latestCount] = s;
                    latestCount++;
                }

                for (String s : currentValue.split(":")) {
                    currentSeperatedValue[currentCount] = s;
                    currentCount++;
                }
                int tmpCount = 0;
                if (currentCount < latestCount)
                    tmpCount = latestCount;
                else
                    tmpCount = currentCount;

                for (int i = 0; i < tmpCount; i++) {

                    if (currentSeperatedValue[i] == null)
                        currentSeperatedValue[i] = "0";

                    if (latestSeperatedValue[i] == null)
                        latestSeperatedValue[i] = "0";

                    if (Integer.parseInt(latestSeperatedValue[i]) > Integer.parseInt(currentSeperatedValue[i])) {
                        newVersion = true;
                        break;
                    } else if (Integer.parseInt(latestSeperatedValue[i]) < Integer.parseInt(currentSeperatedValue[i])) {
                        newVersion = false;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            newVersion = false;
        }
        return newVersion;
    }

    boolean ForceCancel = false;

    class DownloadVersionCheck extends AsyncTask<String, Void, Boolean> {

        //ProgressDialog pDialog;

        protected void onPreExecute() {
				/*
				if(pDialog==null)
				pDialog = new ProgressDialog(BrowserActivity.this);
				pDialog.setMessage("Checking Version. Please wait. ");
				pDialog.setCancelable(false);
				pDialog.show();
				*/

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        protected Boolean doInBackground(String... params) {
            InputStream inputStream = null;

            boolean flag = false;

            String result = "";
            while (!flag) {
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(params[0]);

                    httpGet.setHeader("Accept", "application/json");
                    httpGet.setHeader("Content-type", "application/json; charset=utf-8");

                    HttpResponse httpResponse = httpclient.execute(httpGet);

                    inputStream = httpResponse.getEntity().getContent();
                    if (inputStream != null)
                        result = convertInputStreamToString(inputStream);
                    else
                        result = "Did not work!";

                    // result= textReader();
                    //   Log.e("", "result--   "+result);


                    try {

                        JSONObject jsonresponse = new JSONObject(result);
                        JSONObject android = jsonresponse.getJSONObject("android");
                        JSONArray jsonContents = (JSONArray) android.get("versions");

                        for (int i = 0; i < jsonContents.length(); i++) {
                            JSONObject jsonObj = jsonContents.getJSONObject(i);
                            Latest_VersionNumber = jsonObj.getString("v");
                            Update_Action = jsonObj.getString("update");
                            Force_Action = jsonObj.getString("force");
                            DownloadUrl = jsonObj.getString("location");
                            try {

                                //Log.e("","11  Wrapper doInBackground  showFooter3   --  "+showFooter3);

                                showFooter3 = jsonObj.getString("showFooter");
                                //showFooter3="1";
                                //Log.e("","22  Wrapper doInBackground  showFooter3   --  "+showFooter3);

                                SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                                editor.putString(AppConfig.SHOW_FOOTER, showFooter3);
                                editor.commit();


                            } catch (Exception e) {
                            }

                            //showFooter="0";
                            //Log.e("","33  Wrapper doInBackground  showFooter   --  "+showFooter3);

                        }
                    } catch (Exception npe) {
                        Latest_VersionNumber = "";
                        /*if (pDialog != null && pDialog.isShowing()) {
                            pDialog.dismiss();
                        }*/
                        npe.printStackTrace();
                    }
                    flag = true;
                    inputStream.close();

                } catch (Exception e) {

                    Latest_VersionNumber = "";
                    /*if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }*/
                }
            }
            return flag;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            hideFooterMenu();
            try {
                /*if (pDialog != null & pDialog.isShowing())
                    pDialog.dismiss();*/
            } catch (Exception e) {
            }

            if (result) {
                newVersionAvailable = checkNewVersionAvailable(Current_VersionNumber, Latest_VersionNumber);
                if (newVersionAvailable == true) {
                    ForceCancel = prefs.getBoolean("ForceCancel", false);
                    if (Update_Action.equalsIgnoreCase("1") && Force_Action.equalsIgnoreCase("1")) {
                        displayDialog();
                    } else if (Update_Action.equalsIgnoreCase("1") && Force_Action.equalsIgnoreCase("0") && ForceCancel == false) {
                        displayDialog();
                    } else {
                        onNewIntent(getIntent());
                        setupCustomLists();
                    }
                } else {
                    SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                    editor.putBoolean("ForceCancel", false);
                    editor.commit();
                    onNewIntent(getIntent());
                    setupCustomLists();
                }
            } else {


                onNewIntent(getIntent());
                setupCustomLists();
            }
        }
    }

    public void versionCheck() {
        OkHttpClient client = new OkHttpClient();
        client.newBuilder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build();
        Request request = new Request.Builder().url(AppConfig.VERSION_CHECK_URL).addHeader("content-type", "application/json; charset=utf-8")
                .addHeader("Accept", "application/json").build();

        Log.d("ServerUrl", AppConfig.VERSION_CHECK_URL);
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                String test = e.getMessage();
                String result = test;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        onNewIntent(getIntent());
                        setupCustomLists();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code() == 200 || response.code() == 201) {
                    if (response != null) {
                        String result = response.body().string();
                        String test = result;
                        try {

                            JSONObject jsonresponse = new JSONObject(result);
                            JSONObject android = jsonresponse.getJSONObject("android");
                            JSONArray jsonContents = (JSONArray) android.get("versions");

                            for (int i = 0; i < jsonContents.length(); i++) {
                                JSONObject jsonObj = jsonContents.getJSONObject(i);
                                Latest_VersionNumber = jsonObj.getString("v");
                                Update_Action = jsonObj.getString("update");
                                Force_Action = jsonObj.getString("force");
                                DownloadUrl = jsonObj.getString("location");
                                try {

                                    //Log.e("","11  Wrapper doInBackground  showFooter3   --  "+showFooter3);

                                    showFooter3 = jsonObj.getString("showFooter");
                                    //showFooter3="1";
                                    //Log.e("","22  Wrapper doInBackground  showFooter3   --  "+showFooter3);

                                    SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                                    editor.putString(AppConfig.SHOW_FOOTER, showFooter3);
                                    editor.commit();


                                } catch (Exception e) {
                                }

                                //showFooter="0";
                                //Log.e("","33  Wrapper doInBackground  showFooter   --  "+showFooter3);

                            }

                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    newVersionAvailable = checkNewVersionAvailable(Current_VersionNumber, Latest_VersionNumber);
                                    if (newVersionAvailable == true) {
                                        ForceCancel = prefs.getBoolean("ForceCancel", false);
                                        if (Update_Action.equalsIgnoreCase("1") && Force_Action.equalsIgnoreCase("1")) {
                                            displayDialog();
                                        } else if (Update_Action.equalsIgnoreCase("1") && Force_Action.equalsIgnoreCase("0") && ForceCancel == false) {
                                            displayDialog();
                                        } else {
                                            onNewIntent(getIntent());
                                            setupCustomLists();
                                        }
                                    } else {
                                        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                                        editor.putBoolean("ForceCancel", false);
                                        editor.commit();
                                        onNewIntent(getIntent());
                                        setupCustomLists();
                                    }
                                }
                            });

                        } catch (Exception npe) {
                            Latest_VersionNumber = "";
                            /*if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }*/
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    onNewIntent(getIntent());
                                    setupCustomLists();
                                }
                            });
                            npe.printStackTrace();
                        }
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                onNewIntent(getIntent());
                                setupCustomLists();
                            }
                        });
                    }
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            onNewIntent(getIntent());
                            setupCustomLists();
                        }
                    });
                }
            }
        });

        hideFooterMenu();
    }

    private static String makePostRequest(String url) {
        String service_response = "";

        try {

            Log.e(TAG, "makePostRequest url--    " + url);

            InputStream in = null;
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(url);

            //Log.e("", "11  Inside LIL_Login coolie_value--   "+coolie_value+"  LIL_Login--  "+LIL_Login);


            httpPost.setHeader("myPreferences[mrktPrefStatusSeen]", "yes");


            //Log.e("", "222 coolie_value--   "+coolie_value);


            try {

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();

                in = httpEntity.getContent();
            } catch (Exception e) {
            }

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");

                }
                in.close();
                service_response = sb.toString();

            } catch (Exception e) {
                service_response = "";
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
        } catch (Exception e) {
            service_response = "";
        }
        Log.e(TAG, "service_response--  " + service_response);
        return service_response;

    }


    class callPrefrenceParameter extends AsyncTask<String, Void, Boolean> {


        protected void onPreExecute() {

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        protected Boolean doInBackground(String... params) {
            InputStream inputStream = null;

            boolean flag = false;

            String result = "";
            while (!flag) {
                try {


                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(params[0]);

                    httpGet.setHeader("Accept", "application/json");
                    httpGet.setHeader("Content-type", "application/json; charset=utf-8");

                    HttpResponse httpResponse = httpclient.execute(httpGet);

                    inputStream = httpResponse.getEntity().getContent();
                    if (inputStream != null)
                        result = convertInputStreamToString(inputStream);
                    else
                        result = "Did not work!";

                    // result= textReader();

                    //makePostRequest(params[0]);
                    Logger("doInBackground SRINI result--   " + result);


                    flag = true;
                    //inputStream.close();

                } catch (Exception e) {

                }
            }
            return flag;
        }


        @Override
        protected void onPostExecute(Boolean result) {

        }
    }


    public String textReader() throws IOException {
        try {
            String str = "";

            File sdcard = Environment.getExternalStorageDirectory();

            //Get the text file
            File file = new File(sdcard, "grid.txt");

            //Read text from file
            StringBuilder text = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
            } catch (IOException e) {
                //You'll need to add proper error handling here
            }
            return text.toString();
        } catch (Exception e) {
            return null;
        }


    }// PlayWithSDFiles


		/*
		public String textReader1() throws IOException
		{
			try{
			String str="";
			StringBuffer buf = new StringBuffer();
			InputStream is = getResources().openRawResource(R.drawable.my_base_data);


			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			if (is!=null) {
				while ((str = reader.readLine()) != null) {
					buf.append(str + "\n" );
				}
			}
			is.close();
			//Toast.makeText(getBaseContext(),
					//buf.toString(), Toast.LENGTH_LONG).show();

			return buf.toString();
			}

			catch(Exception e)
			{
				return null;
			}


		}// PlayWithSDFiles
		*/

    private void enableHTML5AppCache() {

        mWebView.getSettings().setDomStorageEnabled(true);

        // Set cache size to 8 mb by default. should be more than enough
        mWebView.getSettings().setAppCacheMaxSize(1024 * 1024 * 8);

        // This next one is crazy. It's the DEFAULT location for your app's cache
        // But it didn't work for me without this line
        mWebView.getSettings().setAppCachePath("/data/data/" + getPackageName() + "/cache");
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setAppCacheEnabled(true);

        mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
    }


    private static final int ZBAR_SCANNER_REQUEST = 0;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;

    @SuppressLint("NewApi")
    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public void launchScanner() {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);

            // intent.putExtra(AppConfig.OPEN_APP,AppConfig.LOCAL_BUILD);

            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, "Rear Facing Camera Unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		/*if (resultCode == RESULT_OK)
		{
			// Toast.makeText(this, "Scan Result = " + data.getStringExtra(ZBarConstants.SCAN_RESULT), Toast.LENGTH_SHORT).show();
			// Toast.makeText(this, "Scan Result Mode= " + data.getStringExtra(ZBarConstants.SCAN_MODES), Toast.LENGTH_SHORT).show();
		} else if(resultCode == RESULT_CANCELED && data != null) {
			String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
			if(!TextUtils.isEmpty(error)) {
				Toast.makeText(this, error, Toast.LENGTH_SHORT).show();com.salesforce.marketingcloud
			}
		}*/
        if (resultCode != RESULT_CANCELED && requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessages) {
                return;
            }
            if (mUploadMessages != null) {
                handleUploadMessages(requestCode, resultCode, data);
            }
        } else if (resultCode == RESULT_OK) {
            // Toast.makeText(this, "Scan Result = " + data.getStringExtra(ZBarConstants.SCAN_RESULT), Toast.LENGTH_SHORT).show();
            // Toast.makeText(this, "Scan Result Mode= " + data.getStringExtra(ZBarConstants.SCAN_MODES), Toast.LENGTH_SHORT).show();
        } else if (resultCode == RESULT_CANCELED && data != null) {
            String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
            if (!TextUtils.isEmpty(error)) {
                Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == RESULT_CANCELED && requestCode == FILECHOOSER_RESULTCODE) {
            //Toast.makeText(this, "User canceled", Toast.LENGTH_SHORT).show();
            mUploadMessages.onReceiveValue(null);
            //mWebView.reload();
        }
//		super.onActivityResult(requestCode, resultCode, data);
    }

    boolean home_click = false;

    public void callBottomButtons(int position) {
        switch (position) {
            case 0:

                loadingFirst = true;

                //callWebView(AppConfig.HOME_URL);
                home_click = true;

                //mWebView.loadUrl(AppConfig.HOME_URL);
                //saveSetUpLaterPreference("1");

                String ff = finalUrl();
                mWebView.loadUrl(ff);

                //String success_touch_id="success";

                isSelected1[0] = true;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = false;

                url_counter = 0;
                callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);

                break;

            case 1:

                if (isBankingEnable == true) {
                    //callWebView(AppConfig.BANKING_URL);

                    //mWebView.loadUrl(AppConfig.BANKING_URL);

                    LanguageName = checkLanguage1(AppConfig.HOME_URL);
                    String url = replace(AppConfig.BANKING_URL, AppConfig.LANGUAGE, LanguageName);
                    mWebView.loadUrl(url);

                    isSelected1[0] = false;
                    isSelected1[1] = true;
                    isSelected1[2] = false;
                    isSelected1[3] = false;
                    isSelected1[4] = false;

                    url_counter = 0;
                    callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);

                }

                break;
            case 3:  //Scannerb
                launchScanner();
                url_counter = 0;
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = true;
                isSelected1[4] = false;

                callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);

                break;

            case 2:
                if (isGridCardEnable == true) {

                    LanguageName = checkLanguage1(AppConfig.HOME_URL);
                    String url = replace(AppConfig.MY_CARD_URL, AppConfig.LANGUAGE, LanguageName);
                    mWebView.loadUrl(url);


                    url_counter = 0;
                    isSelected1[0] = false;
                    isSelected1[1] = false;
                    isSelected1[2] = true;
                    isSelected1[3] = false;
                    isSelected1[4] = false;

                    callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);

                }

                break;
            case 4:


                LanguageName = checkLanguage1(AppConfig.HOME_URL);
                String url = replace(AppConfig.LOCATION_URL, AppConfig.LANGUAGE, LanguageName);

                mWebView.loadUrl(url);


                url_counter = 0;
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = true;

                callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);


                // shareResultAsImage(mWebView);


                break;


        }
        saveGridEnabled(false);

    }


    private void shareResultAsImage(WebView webView) {
        Bitmap bitmap = getBitmapOfWebView(webView);
        String pathofBmp = Images.Media.insertImage(getContentResolver(), bitmap, "data", null);
        Uri bmpUri = Uri.parse(pathofBmp);
        final Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent1.setType("image/png");
        emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);

        startActivity(emailIntent1);
    }

    private Bitmap getBitmapOfWebView(final WebView webView) {
        Picture picture = webView.capturePicture();
        Bitmap bitmap = Bitmap.createBitmap(picture.getWidth(), picture.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        picture.draw(canvas);
        return bitmap;
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        // We detach our content and return it to be retained
        ((ViewGroup) findViewById(Window.ID_ANDROID_CONTENT)).removeView(mContent);
        return mContent;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

//        setSplashImage();

        hideFooterMenu();
    }

    public void showAlertDialog(String message, final WebView web, final String url, final boolean finish) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //dialog.dismiss();
                        //if (finish)
                        //finish();
                        web.getSettings().setJavaScriptEnabled(true);
                        web.loadUrl("javascript:window.location.reload( true )");
                        //loadWebViewUrl(activity_context,web,url);

                    }
                });
        alertDialog.show();
    }

    boolean banking, shopLocatorAvailable, mycardAvailable;

    boolean mypreferences = false;

    //public String url="lobby://resetPassword/gamestopol2/lbr123456/http://stg-thegrid-lcm.ladbrokes.com/en";
    int latestpasswordArrayCount = 0;
    String latestpasswordArrayValue[];

    //boolean loadImage1=true;
    String print_url = "";
    String LanguageName = "";
    private WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            AESHelper.showSSLErrorDialog(handler, error, activity_context);
        }

        public void onPageStarted(WebView view, String url) {

            LanguageName = checkLanguage1(AppConfig.HOME_URL);

            if (LanguageName != null && LanguageName != "" && !LanguageName.equalsIgnoreCase("") && !LanguageName.equalsIgnoreCase(null)) {
                url = replace(url, AppConfig.LANGUAGE, LanguageName);
                url = replace(url, LanguageName + "/" + LanguageName, LanguageName);
                url = replace(url, LanguageName + "//", LanguageName + "/");
            } else
                url = replace(url, AppConfig.LANGUAGE, "");

            boolean is_registration_word_available = isWordAvailable(url, "registration");
            boolean return_en = isWordAvailable(url, "/en");
            boolean return_ie = isWordAvailable(url, "/ie");
            boolean banking1 = isWordAvailable(url, "banking");
            mypreferences = isWordAvailable(url, "mypreferences");
            Logger("onPageStarted    mypreferences--   " + mypreferences);

            /*if (is_registration_word_available == true && mexos_url_should_not_repeat == true) {
                int index = url.indexOf("?");
                String tmp_url = "";
                if (index != -1)
                    tmp_url = url.substring(index + 1, url.length());
                mexos_url_should_not_repeat = false;

                if (return_ie == true)
                    url = AppConfig.MAXOS_URL_IE + tmp_url;
                else
                    url = AppConfig.MAXOS_URL_EN + tmp_url;

                Logger("In maxos shouldOverrideUrlLoading Last     url--    " + url);
            }*/
        }

        public boolean shouldOverrideUrlLoading(WebView webView1, String url) {
            try {
                System.out.println("link testing        " + url);
                // String url="";
                //Logger("11 - - - - - - In First  shouldOverrideUrlLoading ");
                if (!CheckNetworkConnection.isConnectionAvailable(activity_context)) {
                    String message = getResources().getString(R.string.no_internet_connection);
                    showAlertDialog(message, webView1, url, true);
                    return true;
                }

                LanguageName = checkLanguage1(url);
                getSalesForceContactKey(url);


                //Logger("22 - - - - - - In First  shouldOverrideUrlLoading Last     url--    "+url+"  LanguageName--   "+LanguageName);

                if (LanguageName != null && LanguageName != "" && !LanguageName.equalsIgnoreCase("") && !LanguageName.equalsIgnoreCase(null)) {
                    url = replace(url, AppConfig.LANGUAGE, LanguageName);
                    url = replace(url, LanguageName + "/" + LanguageName, LanguageName);
                    url = replace(url, LanguageName + "//", LanguageName + "/");

                    //Logger("22  - - - - - - After  Language updtae   shouldOverrideUrlLoading Last     url--    "+url+"  LanguageName--   "+LanguageName);

                } else {
                    url = replace(url, AppConfig.LANGUAGE, "");
                    //	Logger("33  - - - - - - In Second  shouldOverrideUrlLoading Last     url--    "+url+"  LanguageName--   "+LanguageName);

                }

                //Logger("44  - - - - - - In Second  shouldOverrideUrlLoading Last     url--    "+url+"  LanguageName--   "+LanguageName);

                //print_url="\n\r      "+print_url +"-----\n\r\n\r"+url;

                boolean return_en = isWordAvailable(url, "/en");
                boolean return_ie = isWordAvailable(url, "/ie");


                boolean banking1 = isWordAvailable(url, "banking");
                boolean transfer = isWordAvailable(url, "transfer");
                boolean DomainSync = isWordAvailable(url, "DomainSync");

                boolean repeatDeposit = isWordAvailable(url, "repeatDeposit");
                boolean withdraw = isWordAvailable(url, "withdraw");
                boolean cardLimit = isWordAvailable(url, "cardLimit");
                boolean transactionHistory = isWordAvailable(url, "transactionHistory");
                boolean blog = isWordAvailable(url, "blog");
                boolean balanceViewer = isWordAvailable(url, "balanceViewer");
                boolean google1 = isWordAvailable(url, "google");

                boolean responsiblegambling1 = isWordAvailable(url, "responsiblegambling");
                boolean cardLimit1 = isWordAvailable(url, "cardLimit");
                boolean cardLimit2 = isWordAvailable(url, "cardlimit");

                //Log.e("","cardLimit Cap--   "+cardLimit+"    cardlimit small--  "+cardLimit2);

                if (banking1 == true || transfer == true || repeatDeposit == true || withdraw == true || cardLimit == true || transactionHistory == true || google1 == true || responsiblegambling1 == true || cardLimit1 == true || cardLimit2 == true)
                    bg_img.setVisibility(View.INVISIBLE);
                else
                    bg_img.setVisibility(View.VISIBLE);

                boolean is_previous_login_word_available = isWordAvailable(url, "login");
                boolean is_helpcentre_word_available = isWordAvailable(url, "help");
                boolean is_eighteen_word_available = isWordAvailable(url, "siteImages");
                boolean is_hm_word_available = isWordAvailable(url, "20120704133407");
                boolean is_gamblingcommission_word_available = isWordAvailable(url, "gamblingcommission");
                boolean sports = isWordAvailable(url, "m.ladbrokes.com");
                boolean lobby = isWordAvailable(url, "gaming");
                boolean mhub = isWordAvailable(url, "mhub");

                boolean is_registration_word_available = isWordAvailable(url, "registration");

                boolean is_logout = isWordAvailable(url, "logout");

                mypreferences = isWordAvailable(url, "mypreferences");

                Logger("shouldOverrideUrlLoading    mypreferences--   " + mypreferences);


                boolean hasCrendential11 = isWordAvailable(url, "hasCrendential");
                boolean end_ext = isWordAvailable(url, AppConfig.END_EXT);


                if (is_logout == true) {
                    mexos_url_should_not_repeat = true;
                    //bankingIconEnableFromJavaScript=false;
                    //myCardIconEnableFromJavaScript=false;
                    //saveUserPreference("","",false);
                }


                hasCredential1 = prefs.getBoolean(AppConfig.HAS_CREDENTIAL, false);

                if (is_registration_word_available == true && mexos_url_should_not_repeat == true) {
                    int index = url.indexOf("?");
                    String tmp_url = "";
                    if (index != -1)

                        tmp_url = url.substring(index + 1, url.length());

                    mexos_url_should_not_repeat = false;
                    /*if (return_ie == true)
                        url = AppConfig.MAXOS_URL_IE + tmp_url;
                    else
                        url = AppConfig.MAXOS_URL_EN + tmp_url;*/
                    //Logger("In maxos shouldOverrideUrlLoading Last     url--    "+url);

                }

                Logger("In Last shouldOverrideUrlLoading Last     url--    " + url);

                if (return_en == true || return_ie == true || url.endsWith(AppConfig.END_EXT)) {
                    loadingFirst = true;
                }
                if (is_helpcentre_word_available == true || is_eighteen_word_available == true || is_hm_word_available == true || is_gamblingcommission_word_available == true) {
                    Intent intent = new Intent(BrowserActivity.this, ShowWebView.class);
                    intent.putExtra("urlConection", url);
                    startActivity(intent);
                    url = AppConfig.HOME_URL;
                    return true;
                } else if (url.startsWith("lobby")) {

                    //Logger("2222");
                    latestpasswordArrayValue = new String[10];
                    for (String s : url.split("/")) {
                        //Logger("33333");
                        latestpasswordArrayValue[latestpasswordArrayCount] = s;
                        //Logger("OUT "+latestpasswordArrayCount+"  ----  "+latestpasswordArrayValue[latestpasswordArrayCount]);

                        if (latestpasswordArrayCount == 4) {

                            //Logger("OUT 111 "+latestpasswordArrayCount+"  ----  "+latestpasswordArrayValue[latestpasswordArrayCount]);

                            changeNewPassword(latestpasswordArrayValue[latestpasswordArrayCount]);
                        }
                        if (latestpasswordArrayCount == 5) {
                            int u = url.indexOf("http");
                            String tmp = url.substring(u, url.length());
                            Logger("OUT " + latestpasswordArrayCount + "  ----  " + tmp);
                            break;
                        }
                        latestpasswordArrayCount++;
                    }

                    return true;
                } else if (url.startsWith(SCANNER_URL)) {

                    boolean activePermission = checkAllPermission();
                    //Log.e(TAG,"activePermission--   "+activePermission);
                    try {
                        if (activePermission == true) {
                            launchScanner();
                            url = AppConfig.HOME_URL;
                            return true;
                        } else {

                            //Toast.makeText(BrowserActivity.this,"Set all permissions..", Toast.LENGTH_LONG).show();
                            checkAllPermission();
                            return true;
                        }


                        //callBarcodeResultScreen();
                        //return true;

                    } catch (Exception e) {
                        Logger("Exception in Opening camera--  " + e);
                    }

                } else if (url.startsWith(SHARE_URL)) {
                    //launchScanner();
                    url = AppConfig.HOME_URL;
                    callShare();
                    return true;
                } else if (url.startsWith(SHARE_URL)) {
                    //launchScanner();
                    url = AppConfig.HOME_URL;
                    callShare();
                    return true;
                } else if (url.startsWith(AUTO_LOGIN)) {

                    appLoginSubmit("AUTO_LOGIN");

                    //mitesh
                    return true;
                } else if (url.startsWith(SHOW_TOUCH_ID_URL)) {
                    System.out.println("link testing");
                    call_finger_print_popup = true;
                    url = AppConfig.HOME_URL;
                    boolean newVersionAvailable1 = checkNewVersionAvailable(Current_VersionNumber, Latest_VersionNumber);
                    boolean ForceCancel = prefs.getBoolean("ForceCancel", false);

                    System.out.println(newVersionAvailable1 + "   link testing   " + ForceCancel);
                    // Log.e("","ForceCancel--   "+ForceCancel+"    newVersionAvailable1--  "+newVersionAvailable1);
                    /*if (newVersionAvailable1 == false || ForceCancel == true)
                        callFingerPrint();*/
                    return true;
                } else if (url.startsWith(SET_UP_TOUCH_ID_URL)) {
                    url = AppConfig.HOME_URL;
                    callSetUpTouchId();
                    return true;
                } else if (url.startsWith(CLEAR_CREDENTIAL_URL)) {

                    //Logger("CLEAR_CREDENTIAL_URL URL ");

                    url = AppConfig.HOME_URL;

                    SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
                    editor.putString(AppConfig.USER_NAME, "");
                    editor.putString(AppConfig.ONLINE_USER_PASSWORD, "");
                    editor.putString(AppConfig.AUTO_LOGIN, "0");
                    editor.putString(AppConfig.IN_SHOP_PASSWORD, "");

                    editor.commit();

                    saveHasCredentialPreference1(false);
                    saveSetUpLaterPreference("1");

                    Toast.makeText(BrowserActivity.this, "Clear credential done..", Toast.LENGTH_LONG).show();

                    return true;
                } else if (url.startsWith(MYCARD_URL)) {
                    url = AppConfig.MY_CARD_URL;
                } else if (url.startsWith(SHOPLOCATOR_URL)) {
                    url = AppConfig.LOCATION_URL;
                } else if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                    return true;
                } else if (url.startsWith("mailto:")) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
                    startActivity(intent);
                    return true;
                } else if (url.startsWith("bwinex") || url.contains("event=deposit")) {
                    Log.i("paymenturlcheck", url);
                    /*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);*/
                    return true;
                }


                if (LanguageName != null && LanguageName != "" && !LanguageName.equalsIgnoreCase("") && !LanguageName.equalsIgnoreCase(null)) {
                    url = replace(url, AppConfig.LANGUAGE, LanguageName);
                    url = replace(url, LanguageName + "/" + LanguageName, LanguageName);
                } else
                    url = replace(url, AppConfig.LANGUAGE, "");


                boolean loginInternal = isWordAvailable(url, "login");

                Logger("shouldOverrideUrlLoading Last     url--    " + url + "  LanguageName--   " + LanguageName);

                webView1.scrollTo(0, 0);
                // webView1.loadUrl(url);
                String urlTest = url;
                if (url.equalsIgnoreCase("https://sports-stg2.ladbrokes.com/")) {
                    url = "https://sports.ladbrokes.com/";
                }
                /*Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(url));
                startActivity(browserIntent);*/

                loadWebViewUrl(activity_context, webView1, url);

                return true;
            } catch (Exception e) {
                //System.gc();
                return false;
            }
        }

        //Show loader on url load
        public void onLoadResource(WebView webView, String url) {
            try {
                //Logger("onLoadResource   url_counter--   ");;
	        		/*
	        		int tmp_count=0;
	        		if(home_click==false)
	        			tmp_count=15;
	        		else
	        			tmp_count=10;


	           if(url_counter<tmp_count)
	           {
	        	   showImageLoading1(url);

	        	   url_counter++;
	        	  // loadImage1=false;
	           }
	           else
	           {
	        	     hideImageLoading();
	        	     home_click=false;
	           }
	         	*/
                //showImageLoading1(url);
            } catch (Exception e) {

            }
        }

        public void onPageFinished(WebView webView, String url) {

            //Logger(" 11  onPageFinished   url_counter--   ");;
            try {

                //notifyMessageFromWeb("{\"eventName\": \"ON_PAGE_LOAD\",\"parameters\":{\"URL\":\"" + url + "\"}}");
                System.out.println("this url check on finish" + url);
                //if(url.contains("qa2")){
                //messageToWeb(getFormattedCCBJson("IS_LOGGED_IN", null));
                //}
                cookieManager.setAcceptCookie(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    cookieManager.flush();
                } else {
                    cookieSyncManager.sync();
                }
                isGridCardEnable = checkLoyaltiLoginSuccess(AppConfig.HOME_URL);
                isBankingEnable = checkLoginSuccess1(AppConfig.HOME_URL);
                isLocatorEnable = checkLocatorEnabled(AppConfig.HOME_URL);

                boolean isGridEnabled2 = prefs.getBoolean(AppConfig.IS_GRID_ENABLED, false);

                boolean is_logout1 = isWordAvailable(url, "logout");
                mypreferences = isWordAvailable(url, "mypreferences");

                Logger("onPageFinished    mypreferences--   " + mypreferences);

                if (is_logout1 == true)
                    saveGridEnabled(false);


                if (isGridCardEnable == true || isGridEnabled2 == true)
                    isGridCardEnable = true;

                // Logger("22 ---   onPageFinished   isGridCardEnable   ---"+isGridCardEnable+"  isGridEnabled2--  "+isGridEnabled2+"   is_logout1--  "+is_logout1);

                //isBanking_InShop_Enable=checkInShopLoginSuccess(AppConfig.HOME_URL);

             	    /*
             	   	if(isBankingEnable==true)
             	   	{
             	   		saveUserPreference(userName3,passWord3,true);

             	   	}
            */

                mycardAvailable = isWordAvailable(url, "mycard");

                banking = isWordAvailable(url, "banking");
                bottomItemSelect1(url);
                setOtherLevelTrackingId(url);

                getCustomerId(url);

                exit_url = url;


                callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);

                boolean test = loadingFirst;
                //Logger(" 11  onPageFinished   url_counter-- loadingFirst  "+loadingFirst);
                if (loadingFirst == false || url.contains("login")) {
                    //if (!loginLoading) {
                    hideImageLoading();
                    //}
                }
            } catch (Exception exception) {
                System.out.println("test");
                //exception.printStackTrace();
            }

            //progressDialog2.dismiss();
            // Logger("onPageFinished Last     url--    "+url+"  LanguageName--   "+LanguageName);

            //getAndDeviceDetails();


            // appAndFirstLaunch();


            // throw new RuntimeException("Check Crashlytics Unhandled exceptions are working");


        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (!TextUtils.isEmpty(url) && isCCBWrapperJs(url)) {

                    try {
                        return getWrapperJsResource();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                if (!TextUtils.isEmpty(url) && isCCBWrapperJs(url)) {
                    try {
                        return getWrapperJsResource();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            return super.shouldInterceptRequest(view, url);
        }

        private WebResourceResponse getWrapperJsResource() throws Exception {
            return new WebResourceResponse("text/javascript", "UTF-8", getAssets().open(String.valueOf("javascript/inject_wrapper_ccb.js")));
        }

        private boolean isCCBWrapperJs(String url) {
            return url.contains("inject_wrapper_ccb.js");
        }

        @NonNull
        private String getWrapperJsData() {
            return "window.messageToNative = function messageToNative(message) {\n" +
                    "    console.log(message);\n" +
                    "    JsNativeBridge.sendMessageToNative(JSON.stringify(message));\n" +
                    "}";
        }

    };
    int tmp_ga_tracking_count = 0;
    String return_url = "";
    String exit_url = "";


    public static String replace(String _text, String _searchStr, String _replacementStr) {
        // String buffer to store str
        StringBuffer sb = new StringBuffer();

        // Search for search
        int searchStringPos = _text.indexOf(_searchStr);
        int startPos = 0;
        int searchStringLength = _searchStr.length();

        // Iterate to add string
        while (searchStringPos != -1) {
            sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
            startPos = searchStringPos + searchStringLength;
            searchStringPos = _text.indexOf(_searchStr, startPos);
        }

        // Create string
        sb.append(_text.substring(startPos, _text.length()));

        return sb.toString();
    }

	  /*
	  public void appAndFirstLaunch()
	  {

	       //	gaTrackingFirstValue=prefs.getBoolean(AppConfig.GA_TRACKING_FIRST_TIME, true);
		  //Log.e("", "11111  Wrapper gaTrackingFirstValue Value tmp_ga_tracking_count--  "+tmp_ga_tracking_count+"    gaTrackingFirstValue   ---    "+gaTrackingFirstValue);


			if(gaTrackingFirstValue==true)
			{
				  tmp_ga_tracking_count++;

				if(tmp_ga_tracking_count>2)
				{

					//webview_layout1.setVisibility(View.VISIBLE);
					//bg_img.setVisibility(View.GONE);

				  Log.e("", "  Wrapper App gaTrackingFirstValue Value tmp_ga_tracking_count--  "+tmp_ga_tracking_count+"    gaTrackingFirstValue   ---    "+gaTrackingFirstValue);

					String ga_tracking_value="wrapperApp";
				   	mWebView.loadUrl("javascript:appAndFirstLaunch(\""+ga_tracking_value+"\")");

				   	SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
					editor.putBoolean(AppConfig.GA_TRACKING_FIRST_TIME, false);
					editor.commit();

					gaTrackingFirstValue=false;
				}


			}
	}

	*/


    public void getUserSession() {
        //Log.e("","getUserSession       Function Called ");
        mWebView.loadUrl("javascript:getUserSession(\"\")");
    }


    private TextToSpeech textToSpeech;


    private void convertTextToSpeech(String text) {
        //String text = "";
			/*
			if (null == text || "".equals(text))
			{
				text = "Please give some input.";
			}
			*/
        if (!text.equalsIgnoreCase("") && text != null)
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    @Override
    public void onInit(int status) {
        /*
         *
         * As the convertTextToSpeech is already commented,
         * there is no use of this condition,
         * as this condition is leading to a crash, commented the total condition
         *
         */
        /*if (status == TextToSpeech.SUCCESS) {
            int result = textToSpeech.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("error", "This Language is not supported");
            } else {
                //convertTextToSpeech();
            }
        } else {
            Log.e("error", "Initilization Failed!");
        }*/
    }


    @JavascriptInterface
    @SuppressLint("AddJavascriptInterface")
    public void setGoogleAdId(String adDeviceId1) {

        Log.e(TAG, "setGoogleAdId       Function Called  11");
        //String adDeviceId1=adDeviceId;
        Log.e(TAG, "setGoogleAdId       Function Called  22  adDeviceId1--  " + adDeviceId1);
        //Logger("AndCallSessionExpireUrl homeurl--  "+homeurl);
        mWebView.loadUrl("javascript:setGoogleAdId(\"" + adDeviceId1 + "\")");
        Log.e(TAG, "setGoogleAdId       Function Called  33  adDeviceId1--  " + adDeviceId1);

    }


    public void appLoginSubmit(String title) {
        Log.e("", "appLoginSubmit     Inside --- " + title);


        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
        String user_name = prefs.getString(AppConfig.USER_NAME, "");
        String user_password = prefs.getString(AppConfig.ONLINE_USER_PASSWORD, "");
        String user_in_shop_password = prefs.getString(AppConfig.IN_SHOP_PASSWORD, "");
        String user_in_shop = prefs.getString(AppConfig.IN_SHOP, "");
        String do_auto_login = prefs.getString(AppConfig.AUTO_LOGIN, "");

        if (user_in_shop.equalsIgnoreCase("1")) {
            //user_in_shop_password="3424";
            //user_name="fgdfgd";
            //Logger("appInshopLoginSubmit  "+title+"  USER_NAME--   "+user_name+"  user_in_shop_password--  "+user_in_shop_password);

            mWebView.loadUrl("javascript:appInshopLoginSubmit(\"" + user_name + "\",\"" + user_in_shop_password + "\")");

        } else {
            //Logger("appLoginSubmit  "+title+"  USER_NAME--   "+user_name+"  user_password--  "+user_password);
            /*if (progressDialog2 != null && !progressDialog2.isShowing()) {
                progressDialog2.show();
            }*/
            mWebView.loadUrl("javascript:appLoginSubmit(\"" + user_name + "\",\"" + user_password + "\")");
            // convertTextToSpeech();
        }
    }

    public void touchIdSetUpSuccess(String touchIdRegistered3, String hasCredential3, String touchIdDeviceSupport3) {
        //Logger("touchIdSetUpSuccess--   touchIdRegistered3-  "+touchIdRegistered3+"   hasCredential3--  "+hasCredential3+"   touchIdDeviceSupport3--  "+touchIdDeviceSupport3);
        mWebView.loadUrl("javascript:touchIdSetUpSuccess(\"" + touchIdRegistered3 + "\",\"" + hasCredential3 + "\",\"" + touchIdDeviceSupport3 + "\")");
    }

    public void populateLoginCredential(String title, String user_in_shop, String user_name, String user_password) {

        //Logger(title+" --   user_in_shop-- "+user_in_shop+"   user_name11-  "+user_name+"   user_password--  "+user_password);

        if (user_in_shop.equalsIgnoreCase("1"))
            mWebView.loadUrl("javascript:populateLoginFormForInShop(\"" + user_name + "\",\"" + user_password + "\")");
        else
            mWebView.loadUrl("javascript:populateLoginForm(\"" + user_name + "\",\"" + user_password + "\")");

    }


    // govindtesttouchid4
    //1234

    public void credentialSavedSuccessPopup(String saveRealCredential) {

        //Logger("credentialSavedSuccessPopup success status-  "+saveRealCredential);
        if (saveRealCredential.equalsIgnoreCase("1"))
            mWebView.loadUrl("javascript:credentialSavedSuccessPopup(\"success\")");
    }


    boolean isGridCardEnable = false;
    boolean isBankingEnable = false;
    //boolean isBanking_InShop_Enable=false;
    boolean isLocatorEnable = false;

    public void bottomItemSelect1(String url) {

        try {
            boolean banking1 = isWordAvailable(url, "banking");
            boolean banking_shop_locator = isWordAvailable(url, "frmsrc=banking");


            mycardAvailable = isWordAvailable(url, "mycard");


            // Log.e("", "bottomItemSelect1   mycardAvailable--  "+mycardAvailable );

            boolean transfer = isWordAvailable(url, "transfer");
            boolean deposit = isWordAvailable(url, "deposit");
            boolean repeatDeposit = isWordAvailable(url, "repeatDeposit");
            boolean withdraw = isWordAvailable(url, "withdraw");
            boolean cardLimit = isWordAvailable(url, "cardLimit");
            boolean transactionHistory = isWordAvailable(url, "transactionHistory");
            shopLocatorAvailable = isWordAvailable(url, "shoplocator");

            boolean loginInternal = isWordAvailable(url, "login");
            boolean blog = isWordAvailable(url, "blog");
            boolean balanceViewer = isWordAvailable(url, "balanceViewer");
            boolean accountHistory = isWordAvailable(url, "accountHistory");
            boolean ModifyPersonalDetails = isWordAvailable(url, "ModifyPersonalDetails");
            boolean responsiblegambling = isWordAvailable(url, "responsiblegambling");


            if (shopLocatorAvailable == true && banking_shop_locator == true) {
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = true;
                isExit = false;
            } else if (banking1 == true || transfer == true || repeatDeposit == true || withdraw == true || cardLimit == true || transactionHistory == true || deposit == true || balanceViewer == true || accountHistory == true || ModifyPersonalDetails == true || responsiblegambling == true) {
                isSelected1[0] = false;
                isSelected1[1] = true;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = false;
                isExit = false;

            } else if (shopLocatorAvailable == true)// && loginInternal==true)
            {
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = true;
                isExit = false;
            } else if (mycardAvailable == true) {
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = true;
                isSelected1[3] = false;
                isSelected1[4] = false;

                isExit = false;
            } else if (shopLocatorAvailable == true) {
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = true;
                isExit = false;

            } else {
                isSelected1[0] = true;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = false;
                isExit = true;
            }

        } catch (Exception e) {
            isSelected1[0] = true;
            isSelected1[1] = false;
            isSelected1[2] = false;
            isSelected1[3] = false;
            isSelected1[4] = false;
            isExit = true;

        }
    }


    public void showImageLoading1(String url) {
        try {


            boolean shopLocatorAvailable = isWordAvailable(url, "shoplocator");

            boolean mycardAvailable = isWordAvailable(url, "mycard");
            boolean banking = isWordAvailable(url, "banking");
            boolean end_ext = isWordAvailable(url, AppConfig.END_EXT);


            if (url != null && loadingFirst == true) {
                //Log.e("","end_ext-- "+end_ext);
                //if(end_ext==true)
                //	if((url.endsWith("/en") || url.endsWith("/ie")  || url.endsWith(AppConfig.END_EXT)) && shopLocatorAvailable==false && banking==false && mycardAvailable==false)


                if (end_ext == true && shopLocatorAvailable == false && banking == false && mycardAvailable == false) {
                    try {
                        if (progressDialog2 != null) {
                            //loadingFirst = false;
                            progressDialog2.show();
                        }
                    } catch (Exception e) {
                        String testing = e.getMessage();
                    }

                }
                //else
                //	hideImageLoading();

            }
        } catch (Exception e) {
            String testing = e.getMessage();
        }


    }


    public void hideImageLoading() {

        try {
            if (progressDialog2.isShowing()) {
                Log.i("CBB_Events", "hideImageLoading 3428");
                progressDialog2.dismiss();
                // progressDialog2 = null;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    String customerIdOld = "";

    public String getCustomerId(String url) {
        try {

            String customerId = "";

            cookieManager.setAcceptCookie(true);

            //cookieManager.setCookie(AppConfig.HOME_URL,"mid=444444"+" ; Domain=.xxx.com");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");
            for (String ar1 : temp) {

                if (ar1.contains("customerId")) {
                    String[] temp1 = ar1.split("=");


                    customerIdOld = prefs.getString("customerId", "000");

                    customerId = temp1[1];

                    if (!customerIdOld.equalsIgnoreCase(customerId)) {
                        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                        editor.putString("customerId", customerId);
                        editor.commit();

                        mobileAppTracker.setUserId(customerId);
                        //  mobileAppTracker.measureEvent("First App Login");
                        mobileAppTracker.measureEvent("registration");
                    }
                    return customerId;
                }

            }
            return customerId;
        } catch (Exception e) {
            return "";
        }

    }


    boolean isExit = false;

    public boolean checkLoyaltiLoginSuccess(String url) {
        try {
            String CookieValue = "";
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");
            for (String ar1 : temp) {

                //Logger("checkLoyaltiLoginSuccess  ar1--  "+ar1);
                if (ar1.contains("InShopLogin") || ar1.contains("Loyalty_Login"))
                // if(ar1.contains("Loyalty_Login"))
                {
                    String[] temp1 = ar1.split("=");
                    CookieValue = temp1[1];
                    // Logger("----------Loyalty_Login Value   ------>"+CookieValue);
                    return true;
                }


            }
            return false;
        } catch (Exception e) {
            return false;
        }

    }


    public String getSalesForceContactKey(String url) {
        //Log.e("getSalesForceContactKey","getSalesForceContactKey  -- 11 ");

        try {
            String CookieValue = "";
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");
            //Log.e("getSalesForceContactKey","getSalesForceContactKey  -- 22 ");
            for (String ar1 : temp) {

                if (ar1.contains("loginUid")) {
                    String[] temp1 = ar1.split("=");
                    CookieValue = temp1[1];

                    //Log.e("getSalesForceContactKey","getSalesForceContactKey  CookieValue--  "+CookieValue);
                    //String CookieValue1 = "Ladbrokes_" + CookieValue;
                    String CookieValue1 = "ld_" + CookieValue;
                    setSalesForceContactKey(CookieValue1);
                    //if(CookieValue.equals("1"))
                    return CookieValue;

                }

            }
            return CookieValue;
        } catch (Exception e) {
            return "";
        }

    }


    public boolean checkLocatorEnabled(String url) {
        try {
            String CookieValue = "";
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");
            for (String ar1 : temp) {
                if (ar1.contains("locator")) {
                    String[] temp1 = ar1.split("=");
                    CookieValue = temp1[1];

                    if (CookieValue.equals("1"))
                        return true;

                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }


    public String checkLanguage11(String url) {

        try {
            String CookieValue = "";
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");

            for (String ar1 : temp) {
                // Logger("checkLanguage  ar1--  "+ar1);
                if (ar1.contains("grid_locale")) {
                    String[] temp1 = ar1.split("=");

                    CookieValue = temp1[1];
                    // Logger("------------ checkLoginSuccess    CookieValue--  "+CookieValue);
                    return CookieValue;
                }


            }
            //CookieValue="en";  //mitesh
            return CookieValue;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }


    public String checkLanguage1(String url) {

        url = AppConfig.HOME_URL;
        String Language = "";
        try {

            String tmpLanguage1 = checkLanguage11(url);
            String tmpLanguage2 = checkLanguage2(url);

            if (tmpLanguage1.equalsIgnoreCase("") || tmpLanguage1 == null || tmpLanguage1 == "" || tmpLanguage1.equalsIgnoreCase("null"))
                Language = tmpLanguage2;
            else
                Language = tmpLanguage1;
            // Logger("checkLanguage    tmpLanguage1--  "+tmpLanguage1+"    tmpLanguage2--   "+tmpLanguage2+"    Language--  "+Language);
            return Language;


            //String language=Locale.getDefault().getLanguage();      // ---> en

            //Log.e("", "checkLanguage  language--   "+language);
            // return language;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }


    public String checkLanguage2(String url) {
        // Logger("checkLanguage 2   url--  ");
        try {
            String CookieValue2 = "";
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");

            for (String ar1 : temp) {
                // Logger("checkLanguage  ar1--  "+ar1);
                if (ar1.contains("FLAGS")) {
                    String[] temp1 = ar1.split("=");

                    String CookieValue1 = temp1[1];

                    String[] temp2 = CookieValue1.split("|");

                    CookieValue2 = "" + temp2[1] + temp2[2];

                    // Logger("------------ checkLoginSuccess    CookieValue2--  "+CookieValue2+"  temp2[0]--  "+temp2[0]+"    temp2[1]--  "+temp2[1]+"    temp2[2]--  "+temp2[2]);
                    return CookieValue2;
                }


            }
            //CookieValue="en";  //mitesh
            return CookieValue2;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }


    //testjul25   inshop
    //1357

    //test_aditi_auto5
    // Lbr12345

    public boolean checkLoginSuccess1(String url) {
        //Logger("checkLoginSuccess1  --  "+url);
        try {
            String CookieValue = "";
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");


            for (String ar1 : temp) {
                // Logger("checkLoginSuccess1  ar1--  "+ar1);
                if (ar1.contains("LIL_Login") || ar1.contains("Inshop_Auto_Login")) {
                    String[] temp1 = ar1.split("=");
                    showNotificationUrl = false;
                    CookieValue = temp1[1];

                    //  Logger("------------ checkLoginSuccess    CookieValue--  "+CookieValue);
                    return true;
                }

            }
            return false;
        } catch (Exception e) {
            return false;
        }

    }


    public void setSalesForceContactKey(final String contactKey) {
        //Log.e("setSalesForceContactKey","setSalesForceContactKey  11 ");
        //contactKey="Ladbrokes_123456";
        MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
            @Override
            public void ready(MarketingCloudSdk marketingCloudSdk) {
                RegistrationManager registrationManager = marketingCloudSdk.getRegistrationManager();
                //Log.e(TAG,"setSalesForceContactKey  22 ");
                //Set contact key
                registrationManager.edit().setContactKey(contactKey).commit();
                //Log.e(TAG,"setSalesForceContactKey  33 ");
                //Get contact key
                String contactKey = registrationManager.getContactKey();

                Log.e(TAG, "setSalesForceContactKey  44 get   --  " + contactKey);
            }
        });
    }


    boolean sendOlTrackId = true;

    public boolean setOtherLevelTrackingId(String url) {
        try {
            String CookieValue = "";
            cookieManager.setAcceptCookie(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                cookieManager.flush();
            } else {
                cookieSyncManager.sync();
            }

            String cookies = cookieManager.getCookie(url);

            String[] temp = cookies.split(";");
            for (String ar1 : temp) {
                //Logger("setOtherLevelTrackingId  ar1--  "+ar1);
                if (ar1.contains("lbruser")) {
                    String[] temp1 = ar1.split("=");
                    CookieValue = temp1[1];


                    boolean is_logout1 = isWordAvailable(url, "logout");
                    if (is_logout1 == true)
                        saveGridEnabled(false);
                    //Log.e("", "setOtherLevelTrackingId sendOlTrackId - -  "+sendOlTrackId);

                    if (sendOlTrackId == true) {
                        sendOlTrackId = false;
                        OlAndroidLibrary.getInstance(this).setTrackingID(CookieValue);
                        trackingId = OlAndroidLibrary.getInstance(this).getTrackingId();
                    }


                    if (is_logout1 == true) {
                        is_logout1 = false;
                        sendOlTrackId = true;
                        //  OlAndroidLibrary.getInstance(this).setTrackingID(deviceId);
                        trackingId = OlAndroidLibrary.getInstance(this).getTrackingId();
                    }

                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void callmCustomData1(boolean isGridCardEnable1, boolean isBankingEnable, boolean isLocatorEnable) {
        //Log.e("", "1 callmCustomData   "+isGridCardEnable+"   isBankingEnable--  "+isBankingEnable+"   isLocatorEnable--  "+isLocatorEnable);
        mCustomData = new CustomData[]
                {
                        new CustomData(R.drawable.home_disabled, R.drawable.home_enabled, R.drawable.home_select, true, isSelected1[0]),
                        new CustomData(R.drawable.banking_disabled, R.drawable.banking__enabled, R.drawable.banking_select, isBankingEnable, isSelected1[1]),

                        new CustomData(R.drawable.grid_disabled, R.drawable.grid_enabled, R.drawable.grid_select, isGridCardEnable1, isSelected1[2]),
                        new CustomData(R.drawable.scanner_disabled, R.drawable.scanner_enabled, R.drawable.scanner_select, true, isSelected1[3]),

                        new CustomData(R.drawable.locator_disabled, R.drawable.locator_enabled, R.drawable.locator_select, true, isSelected1[4])

                };
        setupCustomLists();

    }

    boolean[] isSelected1 = new boolean[5];

    boolean isSelected = false;


    public boolean isWordAvailable(String s, String word) {
        boolean d = false;
        try {
            d = s.contains(word);
        } catch (Exception e) {
            d = false;
        }
        return d;
    }


    String customerId = "";
    String SCANNER_URL = "thegridapp://scannner";


    String SHARE_URL = "thegridapp://share";
    String MYCARD_URL = "thegridapp://mycard";
    String SHOPLOCATOR_URL = "thegridapp://shoplocator";

    String SHOW_TOUCH_ID_URL = "thegridapp://showtouchId";

    String SET_UP_TOUCH_ID_URL = "thegridapp://setUpTouchId";

    String CLEAR_CREDENTIAL_URL = "thegridapp://clearCredentials";
    String AUTO_LOGIN = "thegridapp://autologin";

    //String AUTO_POPULATE="thegridapp://autoPapulate";

    boolean loadingFirst = true;
    boolean mexos_url_should_not_repeat = true;
    int url_counter = 0;

    public void onStart() {
        super.onStart();

        //Logger("---   onStart   ---");
        //callBottomButtons(AppConfig.HOME_CLICK);

        //Logger("11 ---   onStart   isGridCardEnable   ---"+isGridCardEnable+"  tmpisGridCardEnable--  "+tmpisGridCardEnable);

        if (isGridCardEnable == false)
            isGridCardEnable = checkLoyaltiLoginSuccess(AppConfig.HOME_URL);


        try {
            prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
        } catch (Exception e) {
        }

        boolean isGridEnabled1 = prefs.getBoolean(AppConfig.IS_GRID_ENABLED, false);

        // Logger("22 ---   onStart   isGridCardEnable   ---"+isGridCardEnable+"  isGridEnabled1--  "+isGridEnabled1);

        if (isGridCardEnable == true || isGridEnabled1 == true)
            isGridCardEnable = true;


        //if(isGridCardEnable==false)
        //	isGridCardEnable=checkLoyaltiLoginSuccess(AppConfig.HOME_URL);


        if (isBankingEnable == false)
            isBankingEnable = checkLoginSuccess1(AppConfig.HOME_URL);

        //if(isBanking_InShop_Enable==false)
        //isBanking_InShop_Enable=checkInShopLoginSuccess(AppConfig.HOME_URL);

        if (isLocatorEnable == false)
            isLocatorEnable = checkLocatorEnabled(AppConfig.HOME_URL);

        callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);

    }

    public void updateFingerPrint() {

        try {
            touchIdDeviceSupport1 = isHardwareDetected1();
        } catch (Exception e) {
            touchIdDeviceSupport1 = false;
        }

        if (touchIdDeviceSupport1 == true) {

            try {
                prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
                boolean createKeyInsideFinalUrl = hasEnrolledFingerprints();


                if (createKeyInsideFinalUrl == false) {
                    touchIdRegistered1 = false;
                    touchIdRegistered2 = "false";
                } else {
                    touchIdRegistered1 = true;
                    touchIdRegistered2 = "true";
                }

                touchIdRegisteredOld = prefs.getString("touchIdRegistered", "000");

                String hasCredential3 = "false";
                String touchIdRegistered2 = "false";
                String touchIdDeviceSupport2 = "false";

                //if(!touchIdRegisteredOld.equalsIgnoreCase(touchIdRegistered2))  //commented bcoz if user comes inbetween from settings screen , then "touchIdRegistered2" was not changing
                {

                    SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();

                    if (touchIdRegistered1 == false) {
                        editor.putString(AppConfig.USER_NAME, "");
                        editor.putString(AppConfig.ONLINE_USER_PASSWORD, "");


                        editor.putString(AppConfig.IN_SHOP_PASSWORD, "");

                        saveHasCredentialPreference1(false);
                    }
                    editor.putString("touchIdRegistered", touchIdRegistered2);

                    editor.commit();


                    boolean hasCredential2 = prefs.getBoolean(AppConfig.HAS_CREDENTIAL, false);

                    if (hasCredential2 == true)
                        hasCredential3 = "true";
                    else
                        hasCredential3 = "false";

                    if (touchIdRegistered1 == true)
                        touchIdRegistered2 = "true";
                    else
                        touchIdRegistered2 = "false";

                    if (touchIdDeviceSupport1 == true)
                        touchIdDeviceSupport2 = "true";
                    else
                        touchIdDeviceSupport2 = "false";

                    if (setup_touch_id_button_called == true && hasCredential2 == false) {
                        setup_touch_id_button_called = false;  //when will send success
                        touchIdSetUpSuccess(touchIdRegistered2, hasCredential3, touchIdDeviceSupport2);
                    }

                }

            } catch (Exception e) {

            }
        }

    }


    String touchIdRegisteredOld = "";
    String touchIdRegistered2 = "";


    public void callBarcodeResultScreen() {
        //Log.e("BetResult","11 callBarcodeResultScreen  SCANNER_THROUGH_JAVASCRIPT--      "+AppConfig.SCANNER_THROUGH_JAVASCRIPT);

        boolean qr_code_correct = prefs.getBoolean(AppConfig.QR_CODE_SCANNER, false);


        if (AppConfig.SCANNER_THROUGH_JAVASCRIPT == true && qr_code_correct == false) {
            boolean came_from_scanner = prefs.getBoolean(AppConfig.CAME_FROM_SCANNER, false);

            String barcode_number = prefs.getString(AppConfig.BARCODE_NUMBER_VALUE, "0");

            String bet_slip_url = prefs.getString(AppConfig.BET_SLIP_URL, "0");

            Log.e("BetResult", "  barcode_number--  " + barcode_number + "   came_from_scanner--  " + came_from_scanner + "  qr_code_scanner--  " + qr_code_correct + "  bet_slip_url--  " + bet_slip_url);

            // barcode_number="B3216282502588";
            // came_from_scanner=true;

            if (came_from_scanner == true) {
                //hideFooterMenu();

                if ((barcode_number != null) || (barcode_number != "0") || (barcode_number != "")) {
                    convertBarcodeIntoBetslipNumber(barcode_number);
                    SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
                    editor.putString(AppConfig.BARCODE_NUMBER_VALUE, "0");
                    editor.putBoolean(AppConfig.CAME_FROM_SCANNER, false);
                    editor.commit();

                }
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (settingsCalled) {
            if (fingerprintManager.hasEnrolledFingerprints()) {
                linkTouchIDDialog();
            } else {
                setUpTouchIDDialog();
            }
            mWebView.onResume();
        } else {
            Logger("---   onResume   ---");
            prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);

            autoLogin1 = prefs.getString(AppConfig.AUTO_LOGIN, "0");


            callBarcodeResultScreen();

            if (autoLogin1.equalsIgnoreCase("0"))
                updateFingerPrint();




	 	/*
		if(isGridCardEnable==false)
 			isGridCardEnable=checkLoyaltiLoginSuccess(AppConfig.HOME_URL);

 		if(isBankingEnable==false)
 			isBankingEnable=checkLoginSuccess1(AppConfig.HOME_URL);

 		//if(isBanking_InShop_Enable==false)
 			//isBanking_InShop_Enable=checkInShopLoginSuccess(AppConfig.HOME_URL);

 		if(isLocatorEnable==false)
 			isLocatorEnable=checkLocatorEnabled(AppConfig.HOME_URL);
 			*/

            if (mobileAppTracker != null) {
                mobileAppTracker.setReferralSources(this);
                mobileAppTracker.measureSession();
            }
            url_counter = 0;
            if (mycardAvailable == true) {
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = true;
                isSelected1[3] = false;
                isSelected1[4] = false;

                //home_click=false;

            } else if (banking == true) {
                isSelected1[0] = false;
                isSelected1[1] = true;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = false;

                //home_click=false;
            } else if (shopLocatorAvailable == true) {
                isSelected1[0] = false;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = true;

                //home_click=false;
            } else {
                isSelected1[0] = true;
                isSelected1[1] = false;
                isSelected1[2] = false;
                isSelected1[3] = false;
                isSelected1[4] = false;

                //home_click=true;
            }


            callmCustomData1(isGridCardEnable, isBankingEnable, isLocatorEnable);

            getUserSession();


            mWebView.onResume();

            OlAndroidLibrary.getInstance(this).registerResume(this);

            //enableForegroundMode();
        }


    }

    @Override
    protected void onPause() {

        //Logger("---   onPause   ---");
        super.onPause();
        if (addJobDialog != null && (addJobDialog.isShowing() || !pickedTouchIDOption)) {
            addJobDialog.dismiss();
            getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                    .edit()
                    .putString("Logged_userName", "")
                    .putString("Logged_password", "")
                    .putString("Logged_touchIdEnabled", "1")
                    .commit();
            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    mWebView.loadUrl("javascript:setupLaterTouch()");
                    if (settingsCalled) {
                        settingsCalled = false;
                    }
                }
            });
            pickedTouchIDOption = true;
        }

        if (setUpDialog != null && (setUpDialog.isShowing() || !pickedTouchIDOption)) {
            setUpDialog.dismiss();
            getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                    .edit()
                    .putString("Logged_userName", "")
                    .putString("Logged_password", "")
                    .putString("Logged_touchIdEnabled", "1")
                    .commit();
            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    mWebView.loadUrl("javascript:setupLaterTouch()");
                    if (settingsCalled) {
                        settingsCalled = false;
                    }
                }
            });
            pickedTouchIDOption = true;
        }
        mWebView.onPause();
        OlAndroidLibrary.getInstance(this).registerPause(this);
    }

    @Override
    protected void onStop() {

        //Logger("---   onStop   ---");
        super.onStop();

        if (inCustomView()) {
            hideCustomView();
        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if (cookieManager != null) {
            try {
                //rememberMeChecks();
                cookieManager.removeSessionCookie();
                /*cookieManager.removeAllCookie();
                cookieManager.removeExpiredCookie();*/
                //cookieManager.setCookie(".ladbrokes.com","vauth=''");
                /*String cookieGlob = cookieManager.getCookie("https://ladbrokes.com");
                Log.i("remembermecheck", cookieGlob);
                boolean rm_h = cookieGlob.contains("rm-h");
                boolean rm_i = cookieGlob.contains("rm-i");
                if(rm_h == false || rm_i == false){
                    Log.i("remembermecheck", "rm");
                    cookieManager.removeSessionCookie();
                    cookieManager.removeAllCookie();
                    cookieManager.removeExpiredCookie();
                }
                Log.i("remembermecheck", cookieManager.getCookie("https://ladbrokes.com"));*/
                cookieManager.flush();
                cookieManager = null;
                cookieSyncManager = null;
            } catch (Exception e) {
            }
        }

    }

    public void rememberMeChecks() {
        String cookieGlob = cookieManager.getCookie("https://ladbrokes.com");
        if (!cookieGlob.contains("rm-h") || !cookieGlob.contains("rm-i")) {
            Log.i("remembermecheck", "rm");
            cookieManager.removeSessionCookie();
            /*cookieManager.removeAllCookie();
            cookieManager.removeExpiredCookie();*/
        }
    }

    // boolean onDestroy=false;
    public static int getResourceIdByName(Context context, String packageName, String defType, String name) {
        Resources resources = context.getResources();
        return resources.getIdentifier(name, defType, packageName);
    }


    @Override
    public void onBackPressed() {
        Logger("---   onBackPressed   mypreferences  ---" + mypreferences);


		  /*
    	String hu_1="https://thegrid.ladbrokes.com/"+LanguageName;
    	String hu_2="https://thegrid.ladbrokes.com/"+LanguageName+"?"+AppConfig.END_EXT;




    	if(exit_url.equalsIgnoreCase(hu_1)|| exit_url.equalsIgnoreCase(hu_2) ||exit_url.equalsIgnoreCase(hu_2) ||exit_url.equalsIgnoreCase(hu_4))
       // if(isUniversalLink==true)
    	//if(isExit==true)
    	{

    		finish();
    	}
    	else
    	{


			if(mWebView.canGoBack())
			{
				//if(exit_url.contentEquals(hu_1)|| exit_url.contentEquals(hu_2) ||exit_url.contentEquals(hu_2) ||exit_url.contentEquals(hu_4))
				//mWebView.goBack();
			}
			else
			{
				super.onBackPressed();
			}


    	}
    	*/
    }

    int touch_count = 0;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (inCustomView()) {
                hideCustomView();
                return true;
            }


            if (mypreferences == true) {

                Logger("11---   onTouch   mypreferences  ---" + mypreferences);
                mypreferences = false;
                //SEND_PREFERENCE_PARAMETER
                try {
                    String url = (AppConfig.SEND_PREFERENCE_PARAMETER);
                    new callPrefrenceParameter().execute(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                //super.onBackPressed();
                Logger("22---   onTouch   mypreferences  ---" + mypreferences);
            }


            if ((mCustomView == null) && mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            }

        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            if (touch_count > 150) {
                touch_count = 0;
                Toast.makeText(this, "Wrapper Date- " + Date + "  URL- " + AppConfig.HOME_URL + "  -C- " + Current_VersionNumber + "  -L-  " + Latest_VersionNumber + "  " + mAppKey + "  SF - " + getString(R.string.access_token), Toast.LENGTH_LONG).show();
            } else
                touch_count++;
            // return true;

        }

        return super.onKeyDown(keyCode, event);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void handleUploadMessages(int requestCode, int resultCode, Intent intent) {
        Uri[] results = null;
        try {
            if (resultCode != RESULT_OK) {
                results = null;

            } else {
                if (intent != null && (intent.getDataString() != null || intent.getClipData() != null)) {
                    String dataString = intent.getDataString();
                    ClipData clipData = intent.getClipData();
                    if (clipData != null) {
                        results = new Uri[clipData.getItemCount()];
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            ClipData.Item item = clipData.getItemAt(i);
                            results[i] = item.getUri();
                        }
                    }
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                } else {
                    results = new Uri[]{mCapturedImageURI};
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mUploadMessages.onReceiveValue(results);
        mUploadMessages = null;
        return;
    }

    private boolean openImageChooser() {
        try {
            File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "FolderName");
            if (!imageStorageDir.exists()) {
                imageStorageDir.mkdirs();
            }
            File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
            mCapturedImageURI = Uri.fromFile(file);

            final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);

            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("*/*");

            Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
//			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{captureIntent});

            startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    //***************************************WebAppInterface START ************************************************//
    int checkLadbrokes = 0;

    @SuppressLint("AddJavascriptInterface")
    public class WebAppInterface {
        BrowserActivity mContext;
        String tmpPassword_to_Upgrade_inshop_Customer2 = "";

        String tmp_userName = "";
        String tmp_passWord = "";

        /**
         * Instantiate the interface and set the context
         */
        WebAppInterface(BrowserActivity c) {

            //Logger("WebAppInterface Constructor");
            mContext = c;
            ;
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void checkLadbrokes() {
            String test = "test";
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PackageManager pm = getPackageManager();
                    try {
                        PackageInfo info = pm.getPackageInfo("com.mobenga.ladbrokes", PackageManager.GET_ACTIVITIES);
                        checkLadbrokes = 1;
                        mWebView.loadUrl("javascript:onCheckLadbrokesStatus(\"" + checkLadbrokes + "\")");
                    } catch (PackageManager.NameNotFoundException e) {
                        String test = e.getMessage();
                        String test1 = test;
                        mWebView.loadUrl("javascript:onCheckLadbrokesStatus(\"" + checkLadbrokes + "\")");
                    }
                }
            });
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void gotoLadbrokes() {
            String test = "test";
            Log.e("jayakrishnatest", "gotoLadbrokes testing");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PackageManager pm = getPackageManager();
                    try {
                        PackageInfo info = pm.getPackageInfo("com.mobenga.ladbrokes", PackageManager.GET_ACTIVITIES);
                        checkLadbrokes = 1;
                        //mWebView.loadUrl("javascript:onCheckLadbrokesStatus(\"" + checkLadbrokes + "\")");
                    } catch (PackageManager.NameNotFoundException e) {
                        //String test = e.getMessage();
                        // String test1 = test;
                        //mWebView.loadUrl("javascript:onCheckLadbrokesStatus(\"" + checkLadbrokes + "\")");
                    }

                    if (checkLadbrokes == 1) {
                        Intent intent = pm.getLaunchIntentForPackage("com.mobenga.ladbrokes");
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        if (intent == null) {
                            Intent playStoreIntent = null;
                            try {
                                playStoreIntent = new Intent(Intent.ACTION_VIEW);
                                playStoreIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                playStoreIntent.setData(Uri.parse("market://details?id=com.mobenga.ladbrokes"));
                                startActivity(playStoreIntent);
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.mobenga.ladbrokes")));
                            }
                        } else {
                            startActivity(intent);
                        }
                    } else {
                        Intent playStoreIntent = null;
                        try {
                            playStoreIntent = new Intent(Intent.ACTION_VIEW);
                            playStoreIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            playStoreIntent.setData(Uri.parse("market://details?id=com.mobenga.ladbrokes"));
                            startActivity(playStoreIntent);
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.mobenga.ladbrokes")));
                        }
                    }
                }
            });
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void saveTouchIdStatus(final String enabled) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                            .edit()
                            .putString("Logged_touchIdEnabled", enabled)
                            .commit();
                }
            });
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void portalLoginActive() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    doLogin = true;
                    //loginLoading = true;
                    //progressDialog2.show();
                }
            });
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void saveSetUpLaterStatus(String setuptouchLater) {
            getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                    .edit()
                    .putString("Logged_touchIdEnabled", setuptouchLater)
                    .commit();
            saveSetUpLaterPreference(setuptouchLater);
        }


//0 we don't need to send  "success" string
//1 we need to send  "success" string


        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void clearCredential() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
                    editor.putString(AppConfig.USER_NAME, "");
                    editor.putString(AppConfig.ONLINE_USER_PASSWORD, "");
                    editor.putString(AppConfig.AUTO_LOGIN, "0");

                    editor.commit();

                    saveHasCredentialPreference1(false);


                }
            });


        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void sendCredential() {
            prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
            String user_name11 = prefs.getString(AppConfig.USER_NAME, "");
            String user_password = prefs.getString(AppConfig.ONLINE_USER_PASSWORD, "");
            String doAutoLoginValue = prefs.getString(AppConfig.AUTO_LOGIN, "");
            String user_in_shop_password = prefs.getString(AppConfig.IN_SHOP_PASSWORD, "");
            String user_in_shop = prefs.getString(AppConfig.IN_SHOP, "");


            if (doAutoLoginValue.equalsIgnoreCase("1")) {
                if (user_in_shop.equalsIgnoreCase("1")) {
                    populateLoginCredential("populateLoginFormForInShop", user_in_shop, user_name11, user_in_shop_password);
                } else {
                    populateLoginCredential("populateLoginForm", user_in_shop, user_name11, user_password);
                }
            } else {

            }
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void enableBottomIcon(String bankingIcon, String myCardIcon) {
            //Logger("JavaScript enableBottomIcon Function");

            if (bankingIcon.equalsIgnoreCase("1"))
                isBankingEnable = true;
            else
                isBankingEnable = false;


            if (myCardIcon.equalsIgnoreCase("1"))
                isGridCardEnable = true;
            else
                isGridCardEnable = false;


            tmpisBankingEnable = isBankingEnable;
            tmpisGridCardEnable = isGridCardEnable;

            saveGridEnabled(isGridCardEnable);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    callmCustomData1(tmpisGridCardEnable, tmpisBankingEnable, isLocatorEnable);
                    setOtherLevelTrackingId(AppConfig.HOME_URL);
                }
            });

            //Logger("  enableBottomIcon  bankingIcon--  "+tmpisBankingEnable+"   myCardIcon--  "+tmpisGridCardEnable);

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void AndroidRemoveSplashScreen() {
            loadingFirst = false;
            hideImageLoading();
            getAdId();
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void appLogin(String userName, String passWord, String saveRealCredential, String autoLogin) {
            getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                    .edit()
                    .putString("Logged_touchIdEnabled", autoLogin)
                    .commit();
            //Logger("4  appLogin  userName-- "+userName+"  passWord--"+passWord+"  saveRealCredential--  "+saveRealCredential+"   autoLogin----  "+autoLogin);
            String inShop = "0";
            saveUserPreference2(userName, passWord, saveRealCredential, autoLogin, inShop);

            saveHasCredentialPreference1(true);


        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void AndCallSessionExpireUrl() {

            // Log.e("","AndCallSessionExpireUrl       Function Called ");
            String homeurl = finalUrl();

            //Logger("AndCallSessionExpireUrl homeurl--  "+homeurl);
            mWebView.loadUrl("javascript:appSessionExpCheck(\"" + homeurl + "\")");

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void appLogin(String userName, String passWord, String saveRealCredential) {

            String autoLogin = "0";
            String inShop = "0";

            saveUserPreference2(userName, passWord, saveRealCredential, autoLogin, inShop);
            saveHasCredentialPreference1(true);

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void appLogin(final String userName, final String passWord, final String saveRealCredential, final String inShop, final String autoLogin) {
            apploginCalled = true;
            getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                    .edit()
                    .putString("Logged_touchIdEnabled", autoLogin)
                    .commit();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (showLinkTouchID && apploginCalled) {
                        showLinkTouchID = false;
                        apploginCalled = false;
                        if (fingerprintManager.hasEnrolledFingerprints()) {
                            linkTouchIDDialog();
                        } else {
                            setUpTouchIDDialog();
                        }
                    }
                    saveUserPreference2(userName, passWord, saveRealCredential, autoLogin, inShop);
                    saveHasCredentialPreference1(true);
                    // Logger("appLogin  2222 appLogin   ");
                }
            });

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void urlRedirect(String url) {

            //Logger(" urlRedirect   url-- "+url);
            if (url.equalsIgnoreCase("https://sports-stg2.ladbrokes.com/lotto") ||
                    url.equalsIgnoreCase("https://lottos.ladbrokes.com/en")) {
                //url = "https://sports-hl.ladbrokes.com/lotto";
                url = "https://sports.ladbrokes.com/lotto";
            }
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
            //return true;

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void betslipResultStatus(String text) {

            //Logger("22  betslipResultStatus  text-- "+text);

            String txt1 = "";

            if (text.equalsIgnoreCase("lost"))
                txt1 = "You have lost the bet \u00a31.00";
            else if (text.equalsIgnoreCase("Won"))
                txt1 = "You have won the bet \u00a31.00";
            else if (text.equalsIgnoreCase("Pending"))
                txt1 = "You bet result is pending";
            else if (text.equalsIgnoreCase("Invalid"))
                txt1 = "You betslip is invalid";
            else
                txt1 = "";

            convertTextToSpeech(txt1);

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void betslipResultStatus(String text, String stack, String result_amount) {

            Log.e("", "11  betslipResultStatus  text-- " + text + "  stack-- " + stack + "   result_amount-- " + result_amount);

            String txt1 = "";

            if (text.equalsIgnoreCase("lost"))
                txt1 = "You have lost the bet , you had placed stack of " + stack;

            else if (text.equalsIgnoreCase("Won"))
                txt1 = "you had placed stack of " + stack + "and your total winning is " + result_amount;

            else if (text.equalsIgnoreCase("Pending"))
                txt1 = "You bet result is pending";
            else if (text.equalsIgnoreCase("Invalid"))
                txt1 = "You betslip is invalid";
            else
                txt1 = "";
            convertTextToSpeech(txt1);
        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void getAppCashoutTutorialValue() {

            Log.e("", "Call  getAppCashoutTutorialValue   ");
            SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
            editor.putBoolean(AppConfig.CASHOUT_BETSLIP_SCANNED, true);
            editor.commit();


            prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);

            boolean firstTimeCashoutLaunchValue = prefs.getBoolean(AppConfig.FIRST_TIME_CASHOUT_LAUNCH, true);
            //boolean cashout_betslip_scanned =prefs.getBoolean(AppConfig.CASHOUT_BETSLIP_SCANNED, false);

            //editor.putBoolean(AppConfig.CASHOUT_BETSLIP_SCANNED, cashout_betslip_scanned);
            //Log.e("", "00  firstTimeCashoutLaunchValue--   "+firstTimeCashoutLaunchValue);//+"    cashout_betslip_scanned--  "+cashout_betslip_scanned);

            //firstTimeCashoutLaunchValue=true;

            if (firstTimeCashoutLaunchValue == true)// && cashout_betslip_scanned==true)
            {
                Log.e("", "Inside   firstTimeCashoutLaunchValue--   " + firstTimeCashoutLaunchValue);//+"    cashout_betslip_scanned--  "+cashout_betslip_scanned);

                SharedPreferences.Editor editor1 = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
                editor1.putBoolean(AppConfig.FIRST_TIME_CASHOUT_LAUNCH, false);
                editor1.commit();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        webview_layout.setVisibility(View.INVISIBLE);
                        cashout_slideshow_layout.setVisibility(View.VISIBLE);
                        mViewPager.setAdapter(new CashoutImageSlideAdapter(mContext));
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                    }
                });

            }

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void showtouchId() {

            Logger("-----showtouchId------");

            //saveUserPreference1(userName,passWord,saveRealCredential,autoLogin,inShop);
            //saveHasCredentialPreference1(true);
            //callFingerPrint();

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void callAndroidsaveInshopAccountTemporary(String upgrade_password_status, String inShop) {

            if (upgrade_password_status.equalsIgnoreCase("1")) {
                prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
                String doAutoLoginValue = prefs.getString(AppConfig.AUTO_LOGIN, "");

                String userName2 = prefs.getString(AppConfig.USER_NAME, "");

                String password2 = prefs.getString(AppConfig.ONLINE_USER_PASSWORD, "");

                if (tmpPassword_to_Upgrade_inshop_Customer2 == null || tmpPassword_to_Upgrade_inshop_Customer2 == "" || tmpPassword_to_Upgrade_inshop_Customer2.equalsIgnoreCase("") || tmpPassword_to_Upgrade_inshop_Customer2.equalsIgnoreCase(null))
                    tmpPassword_to_Upgrade_inshop_Customer2 = password2;

                String saveRealCredential = "0";

                //Logger("22 callAndroid_save_InshopAccountTemporary  status-- "+upgrade_password_status+"   tmpPassword_to_Upgrade_inshop_Customer2--  "+tmpPassword_to_Upgrade_inshop_Customer2+"  inShop---   "+inShop);

                saveUserPreference2(userName2, tmpPassword_to_Upgrade_inshop_Customer2, saveRealCredential, doAutoLoginValue, inShop);  //mitesh

            }

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void callAndroidchangeInshopAccountTemporary(String tmpPassword_to_Upgrade_inshop_Customer1) {
            tmpPassword_to_Upgrade_inshop_Customer2 = tmpPassword_to_Upgrade_inshop_Customer1;
            Logger("callAndroid_change_InshopAccountTemporary Password --   " + tmpPassword_to_Upgrade_inshop_Customer2);

        }


        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void changePasswordTemporary(String a, String b) {

            tmp_userName = a;
            tmp_passWord = b;

            Log.e(JAVASCRIPT_TAG, " changePasswordTemporary  tmp_userName-- " + tmp_userName + "  tmp_passWord--" + tmp_passWord);

        }

        @JavascriptInterface
        @SuppressLint("AddJavascriptInterface")
        public void changePasswordSetIt(String a) {
            Log.e(JAVASCRIPT_TAG, " changePasswordSetIt  a-- " + a);


            if (a.equalsIgnoreCase("Success")) {
                prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
                String doAutoLoginValue = prefs.getString(AppConfig.AUTO_LOGIN, "");

                String inShop = prefs.getString(AppConfig.IN_SHOP, "");

                String userName2 = prefs.getString(AppConfig.USER_NAME, "");

                String password2 = prefs.getString(AppConfig.ONLINE_USER_PASSWORD, "");

                // Logger("11 changePasswordSetIt  tmp_passWord-- "+tmp_passWord+"  userName2--"+userName2+"   tmp_passWord--  "+tmp_passWord);

                if (tmp_passWord == null || tmp_passWord == "" || tmp_passWord.equalsIgnoreCase("") || tmp_passWord.equalsIgnoreCase(null))
                    tmp_passWord = password2;

                String saveRealCredential = "0";

                // Logger("22 changePasswordSetIt  AA-- "+a+"  userName2--"+userName2+"   tmp_passWord--  "+tmp_passWord);

                saveUserPreference2(userName2, tmp_passWord, saveRealCredential, doAutoLoginValue, inShop);  //mitesh

            }

        }
    }


//***************************************WebAppInterface END ************************************************//


    public void appTouchIdCancel(boolean multipleError) {
        //Log.e("", "Call  appTouchIdCancel");
        if (!multipleError) {
            String call_app_touch_id_cancel = "Cancel";
            mWebView.loadUrl("javascript:appTouchIdCancel(\"" + call_app_touch_id_cancel + "\")");
        }
        if (progressDialog2 != null && progressDialog2.isShowing()) {
            isAutoLogin = false;
            loginLoading = false;
            progressDialog2.dismiss();
        }

        // need to close the dialog.
        //mWebView.loadUrl("javascript:appTouchIdCancel()");

    }

    public void fingerPrintAuthentationSuccess(boolean withFingerprint) {
        if (withFingerprint) {
            // If the user has authenticated with fingerprint, verify that using cryptography and
            // then show the confirmation message.
            //appLoginSubmit("FINGER_PRINT");
            doAutoLoginWithCCB();
            tryEncrypt();
        } else {
            // Authentication happened with backup password. Just show the confirmation message.
            showConfirmation(null);
            if (progressDialog2 != null && progressDialog2.isShowing()) {
                loginLoading = false;
                //loginCalled = false;
                progressDialog2.cancel();
            }
        }
    }

    private class JavascriptNativeBridge {
        private Handler handler;

        /**
         * Instantiate the interface and set the context
         */
        JavascriptNativeBridge(Context c) {
            handler = new Handler(c.getMainLooper());
        }

        @JavascriptInterface
        public void sendMessageToNative(final String json) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    System.out.println("This is for gvc testing " + json);
                    saveCCBEvents(json);
                }
            });
        }

        @JavascriptInterface
        public void postMessage(final String json) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    System.out.println("This is for gvc testing " + json);
                    saveCCBEvents(json);
                }
            });
        }
    }

    public void messageToWeb(final String json) {
        mWebView.loadUrl("javascript:vanillaApp.native.messageToWeb(" + json + ")");
    }

    public void saveCCBEvents(String message) {
        try {
            JSONObject job = new JSONObject(message);
            String eventName = job.isNull("eventName") ? "" : job.getString("eventName");
            switch (eventName) {
                case AppConfig.CCB_INITIALIZED_EVENT:

                    break;
                case AppConfig.GET_APPLICATION_SETTINGS_EVENT:

                    break;
                case AppConfig.APP_INITIALIZED_EVENT:
                    if (loginDataReceived) {
                        if (progressDialog2 != null && progressDialog2.isShowing()) {
                            loginLoading = false;
                            progressDialog2.dismiss();
                        }
                        isAutoLogin = false;
                        loginDataReceived = false;
                        /*if (differentUser) {
                            messageDialog();
                        }*/
                    }
                    break;
                case AppConfig.LOGIN_SCREEN_ACTIVE_EVENT:
                    String url = mWebView.getOriginalUrl();
                    String url2 = mWebView.getUrl();
                    SharedPreferences pref = getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE);
                    String userName2 = pref.getString("Logged_userName", "");
                    String password2 = pref.getString("Logged_password", "");
                    String fingerPrintEnabled = pref.getString("Logged_touchIdEnabled", "1");
                    if (doLogin && userName2.trim().length() > 0 && password2.trim().length() > 0
                            && fingerPrintEnabled.equalsIgnoreCase("0")) {
                        isAutoLogin = true;
                        doLogin = false;
                        if (fingerprintManager.hasEnrolledFingerprints()) {
                            callFingerPrint();
                        }
                    }
                    doLogin = false;
                    if (!isAutoLogin && progressDialog2 != null && progressDialog2.isShowing()) {
                        loginLoading = false;
                        progressDialog2.dismiss();
                    }
                    break;
                case AppConfig.PRE_LOGIN_EVENT:
                    if (!job.isNull("parameters")) {
                        JSONObject preLoginObj = job.getJSONObject("parameters");
                        String userName, password, autoLogin = "0";
                        SharedPreferences pref2 = getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE);
                        String userNameExist = pref2.getString("Logged_userName", "");
                        boolean rememberMe;
                        userName = preLoginObj.isNull("userName") ? "" : preLoginObj.getString("userName");
                        if (userNameExist.length() > 0 && userNameExist.equals(userName)) {
                            showLinkTouchID = false;
                        } else {
                            showLinkTouchID = true;
                        }
                        password = preLoginObj.isNull("password") ? "" : preLoginObj.getString("password");
                        rememberMe = preLoginObj.isNull("rememberMe") ? false : preLoginObj.getBoolean("rememberMe");
                        /*String existingUser = getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                                .getString("userName", "");*/
                        /*if(!existingUser.equalsIgnoreCase(userName)){
                            Log.i("Second user touch id link", userName + "----------" + existingUser);
                            mWebView.loadUrl("javascript:secondTouchIdPopup()");
                        }*/

                        getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                                .edit()
                                .putString("Logged_userName", userName)
                                .putString("Logged_password", password)
                                .commit();
                    }
                    loginDataReceived = true;
                    //if (!isAutoLogin && !registration) {
                    //loginLoading = true;
                    //progressDialog2.show();
                    //}
                    registration = false;
                    break;
                case AppConfig.PasswordUpdate_EVENT:
                    if (!job.isNull("parameters")) {
                        JSONObject preLoginObj = job.getJSONObject("parameters");
                        SharedPreferences pref2 = getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE);
                        String userNameExist = pref2.getString("Logged_userName", "");
                        boolean rememberMe;
                        String userName = preLoginObj.isNull("username") ? "" : preLoginObj.getString("username");
                        if (userNameExist.length() > 0 && userNameExist.equals(userName)) {
                            String password = preLoginObj.isNull("newPassword") ? "" : preLoginObj.getString("newPassword");
                            getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                                    .edit()
                                    .putString("Logged_password", password)
                                    .commit();
                        }
                    }
                    loginDataReceived = true;
                    //if (!isAutoLogin && !registration) {
                    //loginLoading = true;
                    //progressDialog2.show();
                    //}
                    registration = false;
                    break;
                case AppConfig.POST_LOGIN_EVENT:
                    if (!job.isNull("parameters")) {
                        JSONObject postLoginObj = job.getJSONObject("parameters");
                        PostLoginCCbEventData postLoginCCbEventData = new PostLoginCCbEventData(postLoginObj);
                        Gson gson = new Gson();
                        getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                                .edit()
                                .putString("Logged_userDetails", gson.toJson(postLoginCCbEventData))
                                .commit();
                    }
                    break;
                case AppConfig.LOGOUT:

                    break;
                case AppConfig.REGISTRATION_EVENT:
                    registration = true;
                    if (progressDialog2 != null && progressDialog2.isShowing()) {
                        loginLoading = false;
                        progressDialog2.dismiss();
                    }
                    break;
                case AppConfig.LOGIN_FAIL_EVENT:
                    if (progressDialog2 != null && progressDialog2.isShowing()) {
                        loginLoading = false;
                        progressDialog2.dismiss();
                    }
                    isAutoLogin = false;
                    loginDataReceived = false;
                    if (!job.isNull("parameters")) {
                        JSONObject preLoginObj = job.getJSONObject("parameters");
                        String type;
                        type = preLoginObj.isNull("type") ? "" : preLoginObj.getString("type");
                        Toast.makeText(BrowserActivity.this, type + " Failed", Toast.LENGTH_LONG);
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void doAutoLoginWithCCB() {
        SharedPreferences pref = getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE);
        String userName2 = pref.getString("Logged_userName", "");
        String password2 = pref.getString("Logged_password", "");
        //Log.i("CCB_Mobile_ Success case ", "Fingerprint");
        Map<String, Object> params = new HashMap<>();
        params.put(AppConfig.USERNAME, userName2);
        params.put(AppConfig.PASSWORD, password2);
        //params.put(AppConfig.IS_TOUCH_ID_ENABLED, true);
        messageToWeb(getFormattedCCBJson(AppConfig.LOGIN_EVENT, params));
    }

    public static String getFormattedCCBJson(String eventName, Map<String, Object> parameters) {
        try {
            //Log.i("CCB_Mobile_ ", "params");
            Gson gsonObj = new Gson();
            Map<String, Object> inputMap = new HashMap<>();
            inputMap.put(AppConfig.CCB_EVENT_NAME, eventName);
            if (parameters != null && !parameters.isEmpty()) {
                inputMap.put(AppConfig.CCB_PARAMETERS, parameters);
            }
            // convert map to JSON String
            return gsonObj.toJson(inputMap);
        } catch (Exception e) {
            return "";
        }
    }

    public boolean pickedTouchIDOption = true;
    AlertDialog addJobDialog, setUpDialog;

    public void linkTouchIDDialog() {
        pickedTouchIDOption = false;
        AlertDialog.Builder builder = new AlertDialog.Builder(BrowserActivity.this);
        //builder.setCancelable(false);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(BrowserActivity.this).inflate(R.layout.link_touch_id_dialog, viewGroup, false);
        builder.setView(dialogView);
        final TextView linkButton = (TextView) dialogView.findViewById(R.id.linkButton);
        final TextView setupLaterTextView = (TextView) dialogView.findViewById(R.id.setupLaterTextView);

        addJobDialog = builder.create();
        addJobDialog.setCanceledOnTouchOutside(false);
        //addJobDialog.setCancelable(false);
        addJobDialog.setContentView(R.layout.link_touch_id_dialog);

        setupLaterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickedTouchIDOption = true;
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                                .edit()
                                .putString("Logged_touchIdEnabled", "1")
                                .commit();
                        mWebView.loadUrl("javascript:setupLaterTouch()");
                        if (settingsCalled) {
                            settingsCalled = false;
                            String url2 = mWebView.getOriginalUrl();
                            //mWebView.loadUrl(url2);
                        }
                    }
                });
                addJobDialog.dismiss();
            }
        });
        linkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickedTouchIDOption = true;
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebView.loadUrl("javascript:linkTouchId()");
                        if (settingsCalled) {
                            settingsCalled = false;
                            String url2 = mWebView.getOriginalUrl();
                        }
                    }
                });
                addJobDialog.dismiss();
            }
        });
        addJobDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                // do something
                if (!pickedTouchIDOption) {
                    getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                            .edit()
                            .putString("Logged_userName", "")
                            .putString("Logged_password", "")
                            .putString("Logged_touchIdEnabled", "1")
                            .commit();
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:setupLaterTouch()");
                            if (settingsCalled) {
                                settingsCalled = false;
                            }
                        }
                    });
                    pickedTouchIDOption = true;
                }
            }
        });
        addJobDialog.show();
    }

    public void setUpTouchIDDialog() {
        pickedTouchIDOption = false;
        AlertDialog.Builder builder = new AlertDialog.Builder(BrowserActivity.this);
        //builder.setCancelable(false);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(BrowserActivity.this).inflate(R.layout.link_touch_id_dialog, viewGroup, false);
        builder.setView(dialogView);
        final TextView linkButton = (TextView) dialogView.findViewById(R.id.linkButton);
        linkButton.setText("Setup Touch ID");
        final TextView setupLaterTextView = (TextView) dialogView.findViewById(R.id.setupLaterTextView);

        setUpDialog = builder.create();
        setUpDialog.setCanceledOnTouchOutside(false);
        //setUpDialog.setCancelable(false);
        setUpDialog.setContentView(R.layout.link_touch_id_dialog);
        linkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSecuritySettings();
                setUpDialog.dismiss();
                pickedTouchIDOption = true;
            }
        });
        setupLaterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickedTouchIDOption = true;
                getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                        .edit()
                        .putString("Logged_touchIdEnabled", "1")
                        .commit();
                mWebView.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebView.loadUrl("javascript:setupLaterTouch()");
                        if (settingsCalled) {
                            settingsCalled = false;
                        }
                    }
                });
                setUpDialog.dismiss();
            }
        });
        setUpDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                // do something
                if (!pickedTouchIDOption) {
                    getSharedPreferences("Logged_MyLoginPreference", MODE_PRIVATE)
                            .edit()
                            .putString("Logged_userName", "")
                            .putString("Logged_password", "")
                            .putString("Logged_touchIdEnabled", "1")
                            .commit();
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:setupLaterTouch()");
                            if (settingsCalled) {
                                settingsCalled = false;
                            }
                        }
                    });
                    pickedTouchIDOption = true;
                }
            }
        });
        setUpDialog.show();
    }

    private void gotoSecuritySettings() {
        settingsCalled = true;
        startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
    }
}