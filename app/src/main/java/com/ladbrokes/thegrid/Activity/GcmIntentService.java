package com.ladbrokes.thegrid.Activity;
import java.util.Date;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService 
{
    private static final Integer pHashLength = 5;
    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "GCM Demo";
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) 
        {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                displayNotification(intent);
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        //GcmBroadcastReceiver.completeWakefulIntent(intent);  //mitesh
    }
    // This is just one simple example of what you might choose to do with a message
    private void sendNotification(String msg) {
        Log.i(TAG, msg);
    }
    // Display the remote notification
    private void displayNotification(Intent intent) 
    {
        Context mContext = getApplicationContext();
        CharSequence tickerText = "null";
        Bundle extras = intent.getExtras();
        final PackageManager pm = mContext.getPackageManager();
        ApplicationInfo ai;
        try 
        {
            ai = pm.getApplicationInfo( mContext.getPackageName(), 0);
        }
        catch (final NameNotFoundException e) 
        {
            ai = null;
        }
        final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
        @SuppressWarnings("unused")
        String pHash = "";
        try {
            for (String key : extras.keySet()) {
                if ( key.compareToIgnoreCase("p") == 0 && extras.getString(key).length() > pHashLength ) 
                {
                    pHash = extras.getString(key);
                }
                else if ( key.compareToIgnoreCase("payload") == 0 && extras.getString(key).length() > 0 )
                {
                    tickerText = extras.getString(key);
                }
                Log.e(TAG, String.format("onMessage: %s=%s", key, extras.getString(key)));
            }
        } catch (NullPointerException npe) {
            tickerText = "no key=msg has been provided.";
        }
    }
}