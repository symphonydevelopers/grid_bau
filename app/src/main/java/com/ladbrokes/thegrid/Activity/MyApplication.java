/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.ladbrokes.thegrid.Activity;


import com.otherlevels.android.sdk.OlAndroidLibrary;
import com.otherlevels.android.sdk.Options;
import com.salesforce.marketingcloud.InitializationStatus;
import com.salesforce.marketingcloud.MarketingCloudConfig;
import com.salesforce.marketingcloud.MarketingCloudSdk;

import android.app.Application;
import android.util.Log;

/*import dagger.ObjectGraph;*/

/**
 * The Application class of the sample which holds the ObjectGraph in Dagger and enables
 * dependency injection.
 */
public class MyApplication extends Application {

    private static final String TAG = MyApplication.class.getSimpleName();

    /*private ObjectGraph mObjectGraph;*/

    //private static GoogleAnalytics sAnalytics;
    // private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        /*
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) 
        {
            // only for gingerbread and newer versions
            initObjectGraph(new FingerprintModule(this));
        }
        */

        Log.e("", " MyApplication  11111");
        try {
            MarketingCloudSdk.init(this, MarketingCloudConfig.builder()

                    .setApplicationLabel("389cd738-b317-430b-96a2-d8dfaff9e85d")
                    .setAccessToken("5bagnckye5ma59gxyr8acp7w")
                    .setGcmSenderId("971042794089")
                    .setAnalyticsEnabled(true)
                    .setPiAnalyticsEnabled(true)
                    .setNotificationRecipient(MainActivity.class) //Landing activity for push notifications
                    .build(), new MarketingCloudSdk.InitializationListener() {
                @Override
                public void complete(InitializationStatus status) {
                }
            });
        } catch (Exception e) {
        }
        Log.e("", " MyApplication  2222");


        // sAnalytics = GoogleAnalytics.getInstance(this);

        Options options = new Options();
        options.appKey = "66a2c903010cea2bd1144bdd4ce73ef5"; // mandatory

        //f4a3b7714560040294b015f7a2427031  //gurmeet gmail enterprise

        //66a2c903010cea2bd1144bdd4ce73ef5 //gurmeet ladbrokes production

        // options.appOpenInterstitialIntent = new Intent(this, BrowserActivity.class); //optional
        options.gcmSenderId = "108189564100"; //optional
        // options.registrationTags ; //optional
        // Initialise the libraryc
        OlAndroidLibrary.init(this, options);

    }
    /*
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
          sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
      }
*/
    /**
     * Initialize the Dagger module. Passing null or mock modules can be used for testing.
     *
     */
    /*public void initObjectGraph(Object module) {
        mObjectGraph = module != null ? ObjectGraph.create(module) : null;
    }

    public void inject(Object object) {
        if (mObjectGraph == null) {
            // This usually happens during tests.
            Log.i(TAG, "Object graph is not initialized.");
            return;
        }
        mObjectGraph.inject(object);
    }*/

}