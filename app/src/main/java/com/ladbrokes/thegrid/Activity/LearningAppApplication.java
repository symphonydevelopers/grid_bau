/*
 * Copyright (c) 2016, salesforce.com, inc.
 * All rights reserved.
 * Licensed under the BSD 3-Clause license.
 * For full license text, see LICENSE.txt file in the repo root  or https://opensource.org/licenses/BSD-3-Clause
 */
package com.ladbrokes.thegrid.Activity;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.BuildConfig;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.GoogleApiAvailability;

//import com.ladbrokes.thegrid.fingerprintdialog.FingerprintModule;
//import com.ladbrokes.android.thegrid.store.BuildConfig;
import com.ladbrokes.android.thegrid.store.R;
//import com.newrelic.agent.android.NewRelic;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.ladbrokes.thegrid.fingerprintdialog.FingerprintModule;
import com.newrelic.agent.android.NewRelic;
import com.otherlevels.android.sdk.OlAndroidLibrary;
import com.otherlevels.android.sdk.Options;
import com.salesforce.marketingcloud.InitializationStatus;
import com.salesforce.marketingcloud.MCLogListener;
import com.salesforce.marketingcloud.MarketingCloudConfig;
import com.salesforce.marketingcloud.MarketingCloudSdk;


import com.salesforce.marketingcloud.notifications.NotificationManager;
import com.salesforce.marketingcloud.notifications.NotificationMessage;
import com.salesforce.marketingcloud.registration.Attribute;
import com.salesforce.marketingcloud.registration.Registration;
import com.salesforce.marketingcloud.registration.RegistrationManager;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/*import dagger.ObjectGraph;*/


/**
 * LearningAppApplication is the primary application class.
 * This class extends Application to provide global activities.
 * <p/>
 *
 * @author Salesforce &reg; 2017.
 */

public class LearningAppApplication extends Application implements MarketingCloudSdk.InitializationListener,
        RegistrationManager.RegistrationEventListener, NotificationManager.NotificationBuilder {


    /**
     * Set to true to show how Salesforce analytics will save statistics for
     * how your customers use the app.
     */
    public static final boolean ANALYTICS_ENABLED = true;
    /**
     * Set to true to test how notifications can send your app customers to
     * different web pages.
     */
    public static final boolean CLOUD_PAGES_ENABLED = true;
    /**
     * Set to true to show how Predictive Intelligence analytics (PIAnalytics) will
     * save statistics for how your customers use the app (by invitation at this point).
     */
    public static final boolean PI_ENABLED = true;
    /**
     * Set to true to show how beacons messages works within the SDK.
     */
    public static final boolean PROXIMITY_ENABLED = false;
    /**
     * Set to true to show how geo fencing works within the SDK.
     */
    public static final boolean LOCATION_ENABLED = true;
    public static final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    private static final String TAG = "~#LearningApp";
    //private SharedPreferences sharedPreferences;
    private String lastBeaconReceivedEventDatetime = "";
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

     /*
    public String getLastBeaconReceivedEventDatetime() {
        if (TextUtils.isEmpty(lastBeaconReceivedEventDatetime)) {
            lastBeaconReceivedEventDatetime = sharedPreferences.getString("lastBeaconReceivedEventDatetime", "Last Event Datetime Not Available");
        }
        return lastBeaconReceivedEventDatetime;
    }
    */

    /**
     * The onCreate() method initialize your app.
     * <p/>
     * In {@link MarketingCloudConfig.Builder} you must set several parameters:
     * <ul>
     * <li>
     * AppId and AccessToken: these values are taken from the Marketing Cloud definition for your app.
     * </li>
     * <li>
     * GcmSenderId for the push notifications: this value is taken from the Google API console.
     * </li>
     * <li>
     * You also set whether you enable LocationManager, CloudPages, and Analytics.
     * </li>
     * </ul>
     * <p/>
     * <p/>
     * The application keys are stored in a separate file (secrets.xml) in order to provide
     * centralized access to these keys and to ensure you use the appropriate keys when
     * compiling the test and production versions.
     **/
    @Override
    public void onCreate() {
        super.onCreate();
        //if(!AppConfig.FINAL_URL.contains("stg")){
            NewRelic.withApplicationToken("AA0d6c4c625dfac67ac2e13f74ed3eb50a61b62fed-NRMA").start(this);
       // }
        // sharedPreferences = getSharedPreferences("AndroidLearningApp", Context.MODE_PRIVATE);
        // startService(new Intent(this, MyService.class));
        /** Register to receive push notifications. */
        MarketingCloudSdk.setLogLevel(BuildConfig.DEBUG ? MCLogListener.VERBOSE : MCLogListener.ERROR);
        MarketingCloudSdk.setLogListener(new MCLogListener.AndroidLogListener());
        MarketingCloudSdk.init(this, MarketingCloudConfig.builder().setApplicationId(getString(R.string.app_id))
                .setAccessToken(getString(R.string.access_token))
                .setGcmSenderId(getString(R.string.gcm_sender_id))
                .setAnalyticsEnabled(ANALYTICS_ENABLED)
                .setGeofencingEnabled(LOCATION_ENABLED)
                .setPiAnalyticsEnabled(PI_ENABLED)
                .setCloudPagesEnabled(CLOUD_PAGES_ENABLED)
                .setProximityEnabled(PROXIMITY_ENABLED)
                .setNotificationSmallIconResId(R.drawable.icon)
                .setNotificationBuilder(this)
                .setNotificationChannelName("Marketing Notifications")
                .setOpenDirectRecipient(ShowDeepLinkWebView.class).build(), this);

        MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
            @Override
            public void ready(MarketingCloudSdk sdk) {
                sdk.getRegistrationManager().registerForRegistrationEvents(LearningAppApplication.this);
                // sdk.getRegionMessageManager().registerGeofenceMessageResponseListener(LearningAppApplication.this);
                //sdk.getRegionMessageManager().registerProximityMessageResponseListener(LearningAppApplication.this);
            }
        });





        Options options = new Options();
        // options.appKey = "66a2c903010cea2bd1144bdd4ce73ef5"; // mandatory

        options.appKey = "66a2c903010cea2bd1144bdd4ce73ef5"; // mandatory
// options.appOpenInterstitialIntent = new Intent(this, BrowserActivity.class); //optional
        // options.gcmSenderId = "108189564100"; //optional
        // options.registrationTags ; //optional
        // Initialise the libraryc
        OlAndroidLibrary.init(this, options);

        sAnalytics = GoogleAnalytics.getInstance(this);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // only for gingerbread and newer versions
            //initObjectGraph(new FingerprintModule(this));
        }
    }
    /**
     * Initialize the Dagger module. Passing null or mock modules can be used for testing.
     *
     */
    /*private ObjectGraph mObjectGraph;

    public void initObjectGraph(Object module) {
        mObjectGraph = module != null ? ObjectGraph.create(module) : null;
    }

    public void inject(Object object) {
        if (mObjectGraph == null) {
            // This usually happens during tests.
            Log.i(TAG, "Object graph is not initialized.");
            return;
        }
        mObjectGraph.inject(object);
    }*/

    /**
     * Called when sdk initialization has completed.
     * <p/>
     * When the SDK initialization is completed.
     */
    @Override
    public void complete(InitializationStatus status) {
        if (!status.isUsable()) {
            Log.e(TAG, "Marketing Cloud Sdk init failed.", status.unrecoverableException());
        } else {
            MarketingCloudSdk cloudSdk = MarketingCloudSdk.getInstance();
            cloudSdk.getAnalyticsManager().trackPageView("data://ReadyAimFireCompleted", "Marketing Cloud SDK Initialization Complete");

            if (status.locationsError()) {
                final GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
                Log.i(TAG, String.format(Locale.ENGLISH, "Google Play Services Availability: %s", googleApiAvailability.getErrorString(status.locationPlayServicesStatus())));
                if (googleApiAvailability.isUserResolvableError(status.locationPlayServicesStatus())) {
                    googleApiAvailability.showErrorNotification(LearningAppApplication.this, status.locationPlayServicesStatus());
                }
            }
        }
    }


    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }
        return sTracker;
    }


    @Override
    public NotificationCompat.Builder setupNotificationBuilder(@NonNull Context context, @NonNull NotificationMessage notificationMessage) {
        NotificationCompat.Builder builder = NotificationManager.setupNotificationBuilder(context, notificationMessage);

        Map<String, String> customKeys = notificationMessage.customKeys();
        if (!customKeys.containsKey("category") || !customKeys.containsKey("sale_date")) {
            return builder;
        }

        if ("sale".equalsIgnoreCase(customKeys.get("category"))) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            try {
                Date saleDate = simpleDateFormat.parse(customKeys.get("sale_date"));
                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, saleDate.getTime())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, saleDate.getTime())
                        .putExtra(CalendarContract.Events.TITLE, customKeys.get("event_title"))
                        .putExtra(CalendarContract.Events.DESCRIPTION, customKeys.get("alert"))
                        .putExtra(CalendarContract.Events.HAS_ALARM, 1)
                        .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, R.id.interactive_notification_reminder, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                builder.addAction(android.R.drawable.ic_menu_my_calendar, getString(R.string.in_btn_add_reminder), pendingIntent);
            } catch (ParseException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
        return builder;
    }

    /**
     * Listens for Registrations
     * <p/>
     * This method is one of several methods to log notifications when an event occurs in the SDK.
     * Different attributes indicate which event has occurred.
     * <p/>
     * RegistrationEvent will be triggered when the SDK receives the response from the
     * registration as triggered by the com.google.android.c2dm.intent.REGISTRATION intent.
     */
    @Override
    public void onRegistrationReceived(@NonNull Registration registration) {
        MarketingCloudSdk.getInstance().getAnalyticsManager().trackPageView("data://RegistrationEvent", "Registration Event Completed");
        if (MarketingCloudSdk.getLogLevel() <= Log.DEBUG) {
            Log.d(TAG, "Marketing Cloud update occurred.");
            Log.d(TAG, "Device ID:" + registration.deviceId());
            Log.d(TAG, "Device Token:" + registration.systemToken());
            Log.d(TAG, "Subscriber key:" + registration.contactKey());
            for (Attribute attribute : registration.attributes()) {
                Log.d(TAG, "Attribute " + (attribute).key() + ": [" + (attribute).value() + "]");
            }
            Log.d(TAG, "Tags: " + registration.tags());
            Log.d(TAG, "Language: " + registration.locale());
            Log.d(TAG, String.format("Last sent: %1$d", System.currentTimeMillis()));
        }
    }




    public void appsFlyerInitialize() {

        /**
         #AppsFlyer: Optional methods- Use according to your specific needs. Please refer to the SDK integration documentation for more information

         #AppsFlyer: Please pay special attention to the collection of device IDs

         AppsFlyerLib.getInstance().setImeiData("GET_DEVICE_IMEI");
         AppsFlyerLib.getInstance().setAndroidIdData("GET_DEVICE_ANDROID_ID");
         AppsFlyerLib.getInstance().setCustomerUserId("myId");
         AppsFlyerLib.getInstance().setCurrencyCode("GBP");
         */
//
        TelephonyManager tm = (TelephonyManager) getSystemService(this.TELEPHONY_SERVICE);
        // get IMEI
        //String imei = tm.getDeviceId();

        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        //Log.e("","imei--   "+imei+"  android_id--  "+android_id);

        //AppsFlyerLib.getInstance().setImeiData(imei);
        AppsFlyerLib.getInstance().setAndroidIdData(android_id);


        /**
         #AppsFlyer: collecting your GCM project ID by setGCMProjectID allows you to track uninstall data in your dashboard

         AppsFlyerLib.getInstance().setGCMProjectNumber("SENDER_ID");

         Please refer to this documentation for more information:
         https://support.appsflyer.com/hc/en-us/articles/208004986
         */

        /**
         #AppsFlyer: the startTracking method must be called after any optional 'Set' methods
         You can get your AppsFlyer Dev Key from the "SDK Integration" section in your dashboard
         */

        AppsFlyerLib.getInstance().startTracking(this, "f35LEGdctLafhEsRssSNan");


        AppsFlyerLib.getInstance().setGCMProjectNumber("971042794089");


        AppsFlyerLib.getInstance().enableUninstallTracking("971042794089"); // ADD THIS LINE HERE

        /**
         #AppsFlyer: registerConversionListener implements the collection of attribution (conversion) data
         Please refer to this documentation to view all the available attribution parameters:
         https://support.appsflyer.com/hc/en-us/articles/207032096-Accessing-AppsFlyer-Attribution-Conversion-Data-from-the-SDK-Deferred-Deeplinking
         */

        AppsFlyerLib.getInstance().registerConversionListener(this, new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
                //Log.e("AppsFlyer"," AppsFlyer   111   "+conversionData.keySet());

                for (String attrName : conversionData.keySet()) {
                    try {
                        Log.e("AppsFlyer", "attribute: " + attrName + " = " + conversionData.get(attrName));
                    } catch (Exception e) {

                    }
                }


                //SCREEN VALUES//
                final String install_type = "Install Type: " + conversionData.get("af_status");


                final String media_source = "Media Source: " + conversionData.get("media_source");


                final String install_time = "Install Time(GMT): " + conversionData.get("install_time");


                final String click_time = "Click Time(GMT): " + conversionData.get("click_time");

                Log.e("Install", "install_type---   " + install_type + "  media_source--  " + media_source + "  install_time--  " + install_time + "  click_time--  " + click_time);

                /*
                runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        // ((TextView) findViewById(R.id.logView)).setText(install_type + "\n" + media_source + "\n" + click_time + "\n" + install_time);
                    }
                });
                */
            }

            @Override
            public void onInstallConversionFailure(String errorMessage) {
                Log.e(AppsFlyerLib.LOG_TAG, "error getting conversion data: " + errorMessage);
                // ((TextView) findViewById(R.id.logView)).setText(errorMessage);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {
            }

            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.e(AppsFlyerLib.LOG_TAG, "error onAttributionFailure : " + errorMessage);
            }
        });

    }


}