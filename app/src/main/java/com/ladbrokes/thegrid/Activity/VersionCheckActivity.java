package com.ladbrokes.thegrid.Activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Activity.BrowserActivity.DownloadVersionCheck;
import com.ladbrokes.thegrid.Config.AppConfig;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.MutableContextWrapper;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.widget.ImageView;
import android.widget.TextView;


public class VersionCheckActivity extends FragmentActivity {
    private View mContent;
    private ImageView image_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setBackgroundDrawable(null);

        // Retains part of the View hierarchy without leaking the previous Context
        mContent = (View) getLastCustomNonConfigurationInstance();
        if (mContent == null) {
            LayoutInflater inflater = getLayoutInflater()
                    .cloneInContext(new MutableContextWrapper(this));
            ViewGroup parent = (ViewGroup) getWindow().getDecorView()
                    .findViewById(Window.ID_ANDROID_CONTENT);
            mContent = inflater.inflate(R.layout.version_check_view, parent, false);
            setContentView(mContent);
        } else {
            MutableContextWrapper context = (MutableContextWrapper) mContent.getContext();
            context.setBaseContext(this);
            setContentView(mContent);
        }
        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);

        image_view = (ImageView) findViewById(R.id.image_view);
        checkVersion();


    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;

    }


    protected void checkVersion() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        Current_VersionNumber = pInfo.versionName;
        int version = pInfo.versionCode;

        try {
            String url = (AppConfig.VERSION_CHECK_URL);
            new DownloadVersionCheck().execute(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String Update_Action = "";
    String Force_Action = "";
    String Latest_VersionNumber = "";
    String DownloadUrl = "";
    String showFooter3 = "0";


    String Current_VersionNumber = "1.0.0";
    boolean newVersionAvailable = false;

    String currentSeperatedValue[];
    String latestSeperatedValue[];
    int currentCount = 0;
    int latestCount = 0;

    public boolean checkNewVersionAvailable(String currentValue, String latestValue) {
        boolean newVersion = false;
        try {
            if (currentValue.equalsIgnoreCase(latestValue))
                newVersion = false;
            else {

                currentValue = currentValue.replace('.', ':');
                latestValue = latestValue.replace('.', ':');
                currentSeperatedValue = new String[10];
                latestSeperatedValue = new String[10];

                for (String s : latestValue.split(":")) {
                    latestSeperatedValue[latestCount] = s;
                    latestCount++;
                }

                for (String s : currentValue.split(":")) {
                    currentSeperatedValue[currentCount] = s;
                    currentCount++;
                }
                int tmpCount = 0;
                if (currentCount < latestCount)
                    tmpCount = latestCount;
                else
                    tmpCount = currentCount;

                for (int i = 0; i < tmpCount; i++) {

                    if (currentSeperatedValue[i] == null)
                        currentSeperatedValue[i] = "0";

                    if (latestSeperatedValue[i] == null)
                        latestSeperatedValue[i] = "0";


                    if (Integer.parseInt(latestSeperatedValue[i]) > Integer.parseInt(currentSeperatedValue[i])) {
                        newVersion = true;
                        break;
                    } else if (Integer.parseInt(latestSeperatedValue[i]) < Integer.parseInt(currentSeperatedValue[i])) {
                        newVersion = false;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            newVersion = false;
        }
        return newVersion;
    }

    class DownloadVersionCheck extends AsyncTask<String, Void, Boolean> {

        ProgressDialog pDialog;


        protected void onPreExecute() {
		/*
		if(pDialog==null)
		pDialog = new ProgressDialog(BrowserActivity.this);
		pDialog.setMessage("Checking Version. Please wait. ");
		pDialog.setCancelable(false);
		pDialog.show();
		*/

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        protected Boolean doInBackground(String... params) {
            InputStream inputStream = null;

            boolean flag = false;

            String result = "";
            while (!flag) {
                try {

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(params[0]);

                    httpGet.setHeader("Accept", "application/json");
                    httpGet.setHeader("Content-type", "application/json; charset=utf-8");

                    HttpResponse httpResponse = httpclient.execute(httpGet);

                    inputStream = httpResponse.getEntity().getContent();
                    if (inputStream != null)
                        result = convertInputStreamToString(inputStream);
                    else
                        result = "Did not work!";

                    // result= textReader();
                    //   Log.e("", "result--   "+result);


                    try {

                        JSONObject jsonresponse = new JSONObject(result);
                        JSONObject android = jsonresponse.getJSONObject("android");
                        JSONArray jsonContents = (JSONArray) android.get("versions");

                        for (int i = 0; i < jsonContents.length(); i++) {
                            JSONObject jsonObj = jsonContents.getJSONObject(i);
                            Latest_VersionNumber = jsonObj.getString("v");
                            Update_Action = jsonObj.getString("update");
                            Force_Action = jsonObj.getString("force");
                            DownloadUrl = jsonObj.getString("location");
                            try {

                                //Log.e("","11  Wrapper doInBackground  showFooter3   --  "+showFooter3);

                                showFooter3 = jsonObj.getString("showFooter");
                                //showFooter3="1";

                                SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                                editor.putString(AppConfig.SHOW_FOOTER, showFooter3);
                                editor.commit();


                            } catch (Exception e) {
                            }

                            //showFooter="0";
                            //Log.e("","33  Wrapper doInBackground  showFooter   --  "+showFooter3);

                        }
                    } catch (Exception npe) {
                        Latest_VersionNumber = "";
                        if (pDialog != null && pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                        npe.printStackTrace();
                    }
                    flag = true;
                    inputStream.close();

                } catch (Exception e) {

                    Latest_VersionNumber = "";
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                }
            }
            return flag;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            try {
                if (pDialog != null & pDialog.isShowing())
                    pDialog.dismiss();
            } catch (Exception e) {
            }

            if (result) {

                newVersionAvailable = checkNewVersionAvailable(Current_VersionNumber, Latest_VersionNumber);
                if (newVersionAvailable == true) {
                    boolean ForceCancel = prefs.getBoolean("ForceCancel", false);

                    Log.e("", "  ForceCancel--  " + ForceCancel);
                    if (Update_Action.equalsIgnoreCase("1") && Force_Action.equalsIgnoreCase("1")) {

                        displayDialog();
                    } else if (Update_Action.equalsIgnoreCase("1") && Force_Action.equalsIgnoreCase("0") && ForceCancel == false) {

                        displayDialog();
                    } else {
                        Intent intent = new Intent(VersionCheckActivity.this, BrowserActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                    editor.putBoolean("ForceCancel", false);
                    editor.commit();
                    //onNewIntent(getIntent());

                    Intent intent = new Intent(VersionCheckActivity.this, BrowserActivity.class);
                    startActivity(intent);
                    finish();

                }
            } else {
                Intent intent = new Intent(VersionCheckActivity.this, BrowserActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    SharedPreferences prefs;
    int version_msg = 0;

    public void displayDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (Force_Action.equals("0"))
            //  version_msg="A new version of The Grid app is available. Please click OK to upgrade or Cancel to stay in the same version.";
            version_msg = R.string.version_change_optional_message;


        else
            //version_msg="A new version of The Grid app is available. Please click OK to upgrade or Cancel to close the app.";
            version_msg = R.string.version_change_mandatory_message;

        builder.setTitle("Alert").setMessage(version_msg)
                .setCancelable(false)
                .setIcon(R.drawable.alert)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String url = DownloadUrl;
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                startActivity(browserIntent);
                                finish();

                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Force_Action.equals("0")) {
                            SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
                            editor.putBoolean("ForceCancel", true);
                            editor.commit();
                            dialog.cancel();

                            Intent intent = new Intent(VersionCheckActivity.this, BrowserActivity.class);
                            startActivity(intent);
                            finish();


                        } else if (Force_Action.equals("1"))
                            finish();
                        else {
                            dialog.cancel();
                            Intent intent = new Intent(VersionCheckActivity.this, BrowserActivity.class);
                            startActivity(intent);
                            finish();

                        }

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}
