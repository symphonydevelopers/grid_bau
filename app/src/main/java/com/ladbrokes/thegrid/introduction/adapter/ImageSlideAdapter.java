package com.ladbrokes.thegrid.introduction.adapter;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Activity.BrowserActivity;
import com.ladbrokes.thegrid.Activity.MainActivity;
import com.ladbrokes.thegrid.Activity.VersionCheckActivity;
import com.ladbrokes.thegrid.Config.AppConfig;
import com.ladbrokes.thegrid.introduction.fragment.HomeActivity;
import com.ladbrokes.thegrid.utils.CheckNetworkConnection;

public class ImageSlideAdapter extends PagerAdapter 
{
	FragmentActivity activity;
	boolean isTablet=false;
	public ImageSlideAdapter(FragmentActivity activity) 
	{
		this.activity = activity;
		
		isTablet=isTablet(activity);
		//isTablet=false;
     	if(isTablet==true)
     		image_count=3;
     	else
     		image_count=3;
     		
		
   inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

	}
	int image_count=3;
	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	
	
	
	  LayoutInflater inflater=null;
	@Override
	public int getCount() {
		return image_count;
	}
	Button btn;
    //String firstTimeLaunchPref="firstTimeLaunchPref";
    ImageView mImageView =null;
    
  	
	//private AnimatedGifImageView animatedGifImageView;
    
   // View view =null;
	@Override
	public View instantiateItem(ViewGroup container, final int position) 
	{
		  if(inflater==null)
		inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		View view = inflater.inflate(R.layout.activity_gif_main, container, false);
		
		
		btn=((Button) view.findViewById(R.id.button1));
		
		btn.setOnClickListener(new OnClickListener() 
		{

			@Override
			public void onClick(View v) 
			
			{
				
				 
				
				
				
				
				//Log.e("", "Cross  Button Click ");
				
				SharedPreferences.Editor editor = activity.getSharedPreferences(AppConfig.DB_NAME_ONBOARD_TUTORIAL, 0).edit();
				editor.putBoolean(AppConfig.FIRST_TIME_APP_LAUNCH, false);
				    editor.commit();
				    
				    
				    if (!CheckNetworkConnection.isConnectionAvailable(activity)) 
		 			{
		 				String message = activity.getResources().getString(R.string.no_internet_connection);
		 				showAlertDialog(message, true);
		 					return;
		 			}    
				    
				
				Intent intent = new Intent(activity,BrowserActivity.class);
				activity.startActivity(intent);
				activity.finish();
			}
		});
		
		
		
		//animatedGifImageView = ((AnimatedGifImageView)view.findViewById(R.id.animatedGifImageView));
		mImageView=null;
		mImageView = (ImageView) view.findViewById(R.id.image_display1);
		if(isTablet==false)
		{
		
				if(position==0)
				{
					
					mImageView.setBackgroundResource(R.drawable.onboarding_mobile1);
					btn.setVisibility(View.INVISIBLE);
					
				}	
				else if(position==1)
				{
					mImageView.setBackgroundResource(R.drawable.onboarding_mobile2);
					btn.setVisibility(View.INVISIBLE);
				}
				else if(position==2)
				{
					mImageView.setBackgroundResource(R.drawable.onboarding_mobile3);
					btn.setVisibility(View.VISIBLE);
				}
			
		}
		else
		{
			
			if(position==0)
			{
				
				mImageView.setBackgroundResource(R.drawable.onboarding_tablet1);
				btn.setVisibility(View.INVISIBLE);
				
			}	
			else if(position==1)
			{
				mImageView.setBackgroundResource(R.drawable.onboarding_tablet2);
				btn.setVisibility(View.INVISIBLE);
			}
			else if(position==2)
			{
				mImageView.setBackgroundResource(R.drawable.onboarding_tablet3);
				btn.setVisibility(View.VISIBLE);
			}
			
			
		}
		//mImageView.setBackgroundResource(R.drawable.tablet_intro);	
		
		container.addView(view);
		return view;
	}
	
	
	public void showAlertDialog(String message, final boolean finish) 
	{
		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
		alertDialog.setMessage(message);
		alertDialog.setCancelable(false);

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) 
					{
						
						 activity.runOnUiThread(new Runnable()
						 {
						     @Override
						     public void run() 
						     {
						    		if (!CheckNetworkConnection.isConnectionAvailable(activity)) 
						 			{
						 				String message = activity.getResources().getString(R.string.no_internet_connection);
						 				showAlertDialog(message, true);
					 					return;
						 			}
						    		
						    		else
						    		{
						    			Intent intent = new Intent(activity,BrowserActivity.class);
										activity.startActivity(intent);
										activity.finish();
						    		}	
						    			//Log.e("", "22  firstTimeCashoutLaunchValue-- 22 ");
						      }
						});
						
						
						dialog.dismiss();
						//if (finish)
							//activity.finish();
					}
				});
		alertDialog.show();
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
}