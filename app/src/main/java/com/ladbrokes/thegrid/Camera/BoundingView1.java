package com.ladbrokes.thegrid.Camera;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;



/**
 * View for displaying bounds for active camera region
 */
public class BoundingView1 extends View {
    /**
     * Camera manager
     */
   // private CameraManager cameraManager;

    public BoundingView1(Context context) {
        super(context);
      //  scannerAlpha=0;
        invalidate();
    }

    /**
     * Sets camera manger
     * @param cameraManager
     */
    
        
      
	public Paint circlePaint,dotPaint;
	Handler handler ;
	 Runnable runnable;
    @Override
    
    
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //if (cameraManager != null) 
        {
        	  // Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        	   circlePaint = new Paint();
               // paint.setARGB(110, 75, 75, 50);
            
        	   circlePaint.setStyle(Paint.Style.STROKE); 
        	   circlePaint.setStrokeWidth(3);
                circlePaint.setColor(Color.RED);
		        circlePaint.setAntiAlias(true);    
		        
		        
		        
		    	dotPaint = new Paint();
		        
		       // dotPaint.setStyle(Paint.Style.STROKE); 
		        //dotPaint.setStrokeWidth(4);
		   
		       // dotPaint.setAntiAlias(true);     
                
		       // canvas.drawRect(0,canvas.getHeight()/2,canvas.getWidth(),300, dotPaint);
		       // dotPaint.setAlpha(SCANNER_ALPHA[scannerAlpha]);
		       // Log.e("", "Canvas Height--  "+canvas.getHeight());
		    	
		    	
		        Rect rt = new Rect(20, canvas.getHeight()/2-230, getWidth()-20, canvas.getHeight()/2+230);
		        dotPaint.setColor(Color.LTGRAY);
		        dotPaint.setStyle(Style.FILL_AND_STROKE);
		        dotPaint.setAlpha(80);
		        
		       // canvas.drawRect(rt, dotPaint);    //mitesh

            
            Rect boundingRect = getBoundingRectUi(canvas.getWidth(), canvas.getHeight());
          
   
            	/*
            if(handler == null) 
	               handler = new Handler();
	    	   runnable = new Runnable()
	    	   {
	    		   
	                 public void run() 
	                 {
			                    try 
			                    {
			                        Log.e("","Boundary View  --  "+scannerAlpha);
			                            handler.postDelayed(this, 5000); 
			                        } catch (Exception e) { 
			                        e.printStackTrace();
			                    } 
	                	 
	               }
	           };
	           handler.postDelayed(runnable, 10000); 
	           */
	           
	           /*
            try{
            	Thread.sleep(1000);
                scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;
			       
            	
            }catch(Exception e){}
            */
            
         //  Log.e("","boundingRect.height()--   "+boundingRect.height());
                  /*   
	           if(scannerAlpha==0)
	               drawLine(canvas,circlePaint,boundingRect.left, 40, boundingRect.width(), 40);
	           else if(scannerAlpha==1)
	              drawLine(canvas,circlePaint,boundingRect.left, boundingRect.height()/2-(boundingRect.height()/4), boundingRect.width(), boundingRect.height()/2-(boundingRect.height()/4));
	           else if(scannerAlpha==2)
	           */
	               drawLine(canvas,circlePaint,boundingRect.left, boundingRect.height()/2, boundingRect.width(), boundingRect.height()/2);
	         /*
	           else if(scannerAlpha==3)
	               drawLine(canvas,circlePaint,boundingRect.left, boundingRect.height()/2+(boundingRect.height()/4), boundingRect.width(), boundingRect.height()/2+(boundingRect.height()/4));
	           else if(scannerAlpha==4)
	               drawLine(canvas,circlePaint,boundingRect.left, boundingRect.height()/2+(boundingRect.height()/2), boundingRect.width(), boundingRect.height()/2+(boundingRect.height()/2));
	           else
	           drawLine(canvas,circlePaint,boundingRect.left, 40, boundingRect.width(),40);
				*/
            
            
        	//buildPoints();
        	
        	// canvas.drawPoint(boundingRect.left+dot_x[scannerAlpha],boundingRect.height()/2+dot_y[scannerAlpha], dotPaint);
        	
		      invalidate();
            
        }
    }
    
    
    
    
    
   // local-mobile.ladbrokes.com
    public final synchronized Rect getBoundingRectUi(int uiWidth, int uiHeight) 
    {
    	/*
        double heightFraction = BOUNDS_FRACTION;
        double widthFraction = BOUNDS_FRACTION;
        if (orientation == 90 || orientation == 270) {
            heightFraction = VERTICAL_HEIGHT_FRACTION;
        }

        int height = (int) (uiHeight * heightFraction);
        int width = (int) (uiWidth * widthFraction);
        int left = (int) (uiWidth * ((1 - widthFraction) / 2));
        int top = (int) (uiHeight * ((1 - heightFraction) / 2));
        int right = left + width;
        int bottom = top + height;
*/
       // return new Rect(left+100, top-50, right+100, bottom+50);
        
        return new Rect(tmp, tmp, uiWidth-tmp, uiHeight-tmp);
    }
    int tmp=20;

    
   
    
  
    
    
    boolean draw_line=true;
    private  void drawLine(Canvas canvas, Paint circlePaint, int x, int y,int width,int height)
    {
    	//draw_line=true;
    	if(draw_line==true)
    	{
    		canvas.drawLine(x, y, width, height, circlePaint);
    	
    		draw_line=false;
    	}
    	else
    	{
    		draw_line=true;
    	}
        // invalidate();
    }

    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
