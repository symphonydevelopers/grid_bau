package com.ladbrokes.thegrid.Camera;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import com.ladbrokes.android.thegrid.store.R;
import com.ladbrokes.thegrid.Activity.BrowserActivity;
import com.ladbrokes.thegrid.Activity.MainActivity;
import com.ladbrokes.thegrid.Config.AppConfig;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class ZBarScannerActivity extends Activity implements Camera.PreviewCallback, ZBarConstants {

    private static final String TAG = "ZBarScannerActivity";
    private static final String DisplayUtils = null;
    private CameraPreview mPreview;
    private Camera mCamera;
    private ImageScanner mScanner;
    private Handler mAutoFocusHandler;
    private boolean mPreviewing = true;

    Button btnSwitch, btnCancel;
    TextView barCodeValue, headerText;

    private boolean isFlashOn;
    private boolean hasFlash;
    Parameters params;
    MediaPlayer mp;

    SharedPreferences prefs;
    //  String firstTimeLaunchPref="firstTimeLaunchPref";

    static {
        try {
            System.loadLibrary("iconv");
        } catch (Exception e) {
        }
    }

    String dataToSend = "";
    int rotation = 1;

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean isTablet = isTablet(this);

        isTablet = false;
        if (isTablet == true)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Log.e("", "ZBarScannerActivity   onCreate   ");

        if (!isCameraAvailable()) {
            // Cancel request if there is no rear-facing camera.
            cancelRequest();
            return;
        }

        // Hide the window title.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.zbar_scanner_portrait);


        btnSwitch = (Button) findViewById(R.id.light);
        btnCancel = (Button) findViewById(R.id.cancel);
        // btnTap = (Button) findViewById(R.id.tapButton);


        mAutoFocusHandler = new Handler();

        // Create and configure the ImageScanner;
        setupScanner();

        // Create a RelativeLayout container that will hold a SurfaceView,
        // and set it as the content of our activity.


        rotation = getScreenOrientation();

        mPreview = new CameraPreview(this, this, autoFocusCB, rotation);

        BoundingView1 mPreview1 = new BoundingView1(this);


        hasFlash = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        btnSwitch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                if (isFlashOn) {

                    turnOffFlash();

                    //changeCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
                    //isFlashOn=false;

                } else {
                    // turn on flash
                    turnOnFlash();

                    //changeCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    //isFlashOn=true;


                }


            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();

            }
        });


        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_layout);
        // FrameLayout preview1 = (FrameLayout) findViewById(R.id.camera_layout);

        // ((BoundingView) findViewById(R.id.bounding_view)).setCameraManager();


        preview.addView(mPreview);
        preview.addView(mPreview1);


        // setContentView(mPreview);
    }

    public void setupScanner() {
        mScanner = new ImageScanner();
        mScanner.setConfig(0, Config.X_DENSITY, 3);
        mScanner.setConfig(0, Config.Y_DENSITY, 3);

        int[] symbols = getIntent().getIntArrayExtra(SCAN_MODES);

        // Log.e("", "11   setupScanner  symbols--   "+symbols);

        if (symbols != null) {
            mScanner.setConfig(Symbol.NONE, Config.ENABLE, 0);
            for (int symbol : symbols) {
                // Log.e("", "22  setupScanner  symbol--   "+symbols);
                mScanner.setConfig(symbol, Config.ENABLE, 1);
            }
        }


    }


    /*public void changeCamera(int cameraId) {
        try {
            if (mCamera != null) {
                mPreview.setCamera(null, rotation);
                mCamera.cancelAutoFocus();
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();
                mCamera.release();
                mPreview.hideSurfaceView();
                mPreviewing = false;
                mCamera = null;
            }
            if (mCamera == null)
                mCamera = open(cameraId);
            if (mCamera == null) {
                cancelRequest();
                return;
            }

            mPreview.setCamera(mCamera, rotation);
            mPreview.showSurfaceView();
            mPreviewing = true;
        } catch (Exception e) {

        }

        SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE).edit();
        editor.putInt("cameraId", cameraId);
        editor.commit();

    }*/


    @SuppressWarnings("deprecation")
    public Camera open(int cameraId) {

        int numCameras = Camera.getNumberOfCameras();
        if (numCameras == 0) {
            Log.e(TAG, "No cameras!");
            return null;
        }

        // Select a camera if no explicit camera requested
        int index = 0;
        while (index < numCameras) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            try {
                Camera.getCameraInfo(index, cameraInfo);
            } catch (Exception e) {
                Log.e("camera issue", e.getMessage());
            }


            // cameraId=Camera.CameraInfo.CAMERA_FACING_BACK;
            if (cameraInfo.facing == cameraId) {
                break;
            }
            index++;
        }

        cameraId = index;

        Camera camera;
        try {
            if (cameraId < numCameras) {
                // Log.e(TAG, "Opening camera #" + cameraId);
                camera = Camera.open(cameraId);
            } else {
                // Log.e(TAG, "No camera facing back; returning camera #0");
                camera = Camera.open(0);
            }
        } catch (Exception e) {
            camera = Camera.open(0);
        }
        /*if (cameraId < numCameras) {
            // Log.e(TAG, "Opening camera #" + cameraId);
            camera = Camera.open(cameraId);
        } else {
            // Log.e(TAG, "No camera facing back; returning camera #0");
            camera = Camera.open(0);
        }*/

        return camera;
    }

    int cameraId = 0;

    @Override
    protected void onResume() {
        super.onResume();

        prefs = getSharedPreferences(AppConfig.DB_NAME, MODE_PRIVATE);
        cameraId = prefs.getInt("cameraId", 0);
        // Log.e("","onResume  cameraId--    "+cameraId);
        // Open the default i.e. the first rear facing camera.
        mCamera = open(cameraId);
        if (mCamera == null) {
            // Cancel request if mCamera is null.
            cancelRequest();
            return;
        }

        mPreview.setCamera(mCamera, rotation);
        mPreview.showSurfaceView();

        mPreviewing = true;
    }


    public int getScreenOrientation() {
        Display getOrient = getWindowManager().getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_UNDEFINED;
        if (getOrient.getWidth() == getOrient.getHeight()) {
            orientation = Configuration.ORIENTATION_SQUARE;
        } else {
            if (getOrient.getWidth() < getOrient.getHeight()) {
                orientation = Configuration.ORIENTATION_PORTRAIT;
            } else {
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }


    @Override
    protected void onPause() {
        super.onPause();

        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (mCamera != null) {
            mPreview.setCamera(null, rotation);
            try {
                mCamera.cancelAutoFocus();
            } catch (Exception e) {
                // auto focus cancellation is failing due to device dependence or hard ware issue
                Log.e("camera error", e.getMessage());
            }
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();

            // According to Jason Kuang on http://stackoverflow.com/questions/6519120/how-to-recover-camera-preview-from-sleep,
            // there might be surface recreation problems when the device goes to sleep. So lets just hide it and
            // recreate on resume
            mPreview.hideSurfaceView();

            mPreviewing = false;
            mCamera = null;
        }
    }

    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public void cancelRequest() {
        Intent dataIntent = new Intent();
        dataIntent.putExtra(ERROR_INFO, "Camera unavailable");
        setResult(Activity.RESULT_CANCELED, dataIntent);
        finish();
    }


    public void onPreviewFrame(byte[] data, Camera camera) {
        Camera.Parameters parameters = null;
        int cameraSizePreviewWidth, cameraSizePreviewHeight;
        try {
            parameters = camera.getParameters();
        } catch (Exception e) {
            parameters = null;
        }

        if (parameters == null) {
            cameraSizePreviewWidth = 720;
            cameraSizePreviewHeight = 1280;
        } else {
            Camera.Size size = parameters.getPreviewSize();
            cameraSizePreviewWidth = size.width;
            cameraSizePreviewHeight = size.height;
        }


        String str1 = new String(data);
        // int width = size.width;
        // int height = size.height;
        // Log.e("","onPreviewFrame--   size.width--  "+size.width+"   size.height--  "+size.height+"   data--    "+data.toString()+"   str1--   "+str1);
        //Image barcode = new Image(size.width, size.height, "Y800");
        Image barcode = new Image(cameraSizePreviewWidth, cameraSizePreviewHeight, "Y800");

        int rotation1 = getScreenOrientation();

        barcode.setData(data);

        // Log.e("", "cameraId--   "+cameraId+"   rotation1--  "+rotation1);
        //setCrop(left,top, width,height);
        //In Image  width,left,top,height

        //size.width=1920;  s6   1920/2=960
        //size.height=1080;  s6   1080/2=540

        //LEFT = WIDTH;
        //TOP= LEFT
        //WIDTH=TOP
        //HEIGHT=WIDTH


        if (cameraId == 0) {

            //setCrop(TOP,LEFT,HEIGHT,WIDTH)  //portrait
            //setCrop(LEFT,TOP,WIDTH,HEIGHT,)  //landscape

            int portrait_left = 0;
            //int portrait_width = size.width;
            int portrait_width = cameraSizePreviewWidth;
            int landscape_left = 0;
            //int landscape_width = size.width;
            int landscape_width = cameraSizePreviewWidth;
        	
        	
         	/*
        	int portrait_top=(size.height/2+250);
        	int portrait_height=350;
         	int landscape_top=(size.height/2-120);
        	int landscape_height=200;
        	*/


            //int portrait_top = (size.height / 2 + 150);
            int portrait_top = (cameraSizePreviewHeight / 2 + 150);
            int portrait_height = 450;
            //int landscape_top = (size.height / 2 - 250);
            int landscape_top = (cameraSizePreviewHeight / 2 - 250);
            int landscape_height = 200;
        	
        	
        	/*
        	 if(rotation1==1)
  	 	            barcode.setCrop(portrait_top,portrait_left,portrait_height,portrait_width);
  	 	        else
  	         barcode.setCrop(landscape_left,landscape_top,landscape_width,200);
            */


            if (rotation1 == 1)
                barcode.setCrop(portrait_top, 0, portrait_height, portrait_width);
//          barcode.setCrop(portrait_top,portrait_left,portrait_height,portrait_width);

            else
                //  barcode.setCrop(landscape_left,0,landscape_width,200);
                barcode.setCrop(landscape_left, landscape_top, landscape_width, landscape_height);


        }


        //   barcode.setData(data);
        int result = mScanner.scanImage(barcode);


        if (result != 0) {

            SymbolSet syms = mScanner.getResults();

            for (Symbol sym : syms) {
                String symData = sym.getData();

                if (!TextUtils.isEmpty(symData))// && symData.length()>15)
                {
                    //Log.e("","fss--------- symData--   "+symData+"  result--  "+result+"   symData.length()-- "+symData.length());

                    mCamera.cancelAutoFocus();
                    mCamera.setPreviewCallback(null);
                    mCamera.stopPreview();
                    mPreviewing = false;


                    /*Intent dataIntent = new Intent();
                    dataIntent.putExtra(SCAN_RESULT, symData);
                    dataIntent.putExtra(SCAN_RESULT_TYPE, sym.getType());
                    setResult(Activity.RESULT_OK, dataIntent);*/
                
                    /*
                	RetailBarcode bc = null;
                	try{
        				
        				bc=new RetailBarcode(symData);
        				dataToSend=""+(bc.getShopId()+""+bc.getSlipId());
        				Log.e("Slip ID: ","datatoSend   --------" +bc.getShopId()+"-"+bc.getSlipId()+"  dataToSend--  "+dataToSend);
        		    	
        			}
        			catch(Exception e)
        			{
        				
        			}*/
                    playSound1();

                    dataToSend = symData;


                    callBarCodeSending(dataToSend);


                    //barCodeValue.setText(""+dataToSend);
                    //bottom_text.setVisibility(View.VISIBLE);

                    btnCancel.setVisibility(View.INVISIBLE);
                    btnSwitch.setVisibility(View.INVISIBLE);

                    btnSwitch.setGravity(Gravity.CENTER);

                    //  finish();
                    break;
                } else {
                    //onResume1();
                }


            }
        } else {
            //onResume1();
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (mCamera != null && mPreviewing) {
                Camera.Parameters p = mCamera.getParameters();
                List<String> focusModes = p.getSupportedFocusModes();

                if (focusModes != null && focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                    mCamera.autoFocus(autoFocusCB);
                }
            }
        }
    };

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            mAutoFocusHandler.postDelayed(doAutoFocus, 2000);
        }
    };


    private void turnOnFlash() {

        //Log.e("","turnOnFlash  isFlashOn--    "+isFlashOn+"      params--  "+params+"    mCamera--  "+mCamera);
        try {
            if (!isFlashOn) {
                if (mCamera == null) {
                    return;
                }
                // play sound
                playSound();

                params = mCamera.getParameters();
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

                params.setFlashMode(Parameters.FLASH_MODE_TORCH);
                mCamera.setParameters(params);
                mCamera.startPreview();
                isFlashOn = true;

                // changing button/switch image
                //toggleButtonImage();
            }
        } catch (Exception e) {
            //	e.printStackTrace();

            if (hasFlash == false)
                Toast.makeText(this, "Sorry, your device doesn't support flash light!", Toast.LENGTH_SHORT).show();


        }

    }

    /*
     *
     * Turning Off flash
     */
    @SuppressLint("NewApi")
    private void turnOffFlash() {

        if (isFlashOn) {
            if (mCamera == null || params == null) {
                return;
            }
            // play sound
            playSound();
            try {
                params = mCamera.getParameters();
                params.setFlashMode(Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(params);
                //cameraManager.getCamera().stopPreview();
                isFlashOn = false;
            } catch (Exception e) {
                //turning off flash fails.
            }
            /*params = mCamera.getParameters();
            params.setFlashMode(Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(params);
            //cameraManager.getCamera().stopPreview();
            isFlashOn = false;

            // changing button/switch image
            //toggleButtonImage();*/
        }

    }


    @Override
    public void onBackPressed() {
        String betslip_url = AppConfig.HOME_URL;
        boolean qr_code_correct = true;
        String barcode_value = "";
        callBackScreen(betslip_url, qr_code_correct, barcode_value);


    }


    //String openApp="";
    public void callBackScreenNotUsed(String betslip_url, boolean qr_code_correct) {
        Intent intent = new Intent(ZBarScannerActivity.this, BrowserActivity.class);
        ;
        intent.putExtra("betslip_url", betslip_url);
        intent.putExtra("SelectIcon", "TRUE");
        intent.putExtra("qr_code_correct", qr_code_correct);
        startActivity(intent);
        finish();
    }


    public void callBackScreen(String betslip_url, boolean qr_code_scanner, String barcode_value) {
        // Log.e("", "callBackScreen  qr_code_scanner--  "+qr_code_scanner+"  barcode_value--  "+barcode_value+"    betslip_url--   "+betslip_url);

        Intent intent = new Intent(ZBarScannerActivity.this, BrowserActivity.class);
        ;
        if (AppConfig.SCANNER_THROUGH_JAVASCRIPT == false) {
            intent = new Intent(ZBarScannerActivity.this, BrowserActivity.class);
            intent.putExtra("betslip_url", betslip_url);
            intent.putExtra("SelectIcon", "TRUE");
            intent.putExtra("qr_code_correct", true);
            startActivity(intent);
        } else {
            SharedPreferences.Editor editor = getSharedPreferences(AppConfig.DB_NAME, 0).edit();
            editor.putString(AppConfig.BARCODE_NUMBER_VALUE, barcode_value);
            editor.putString(AppConfig.BET_SLIP_URL, betslip_url);
            editor.putBoolean(AppConfig.CAME_FROM_SCANNER, true);
            editor.putBoolean(AppConfig.QR_CODE_SCANNER, false);
            editor.commit();
            finish();
        }

    }

    boolean qr_code_scanner = false;

    protected void callBarCodeSending(String value) {
        try {
            //Intent intent = new Intent(ZBarScannerActivity.this,BrowserActivity.class);
            String betslip_url1 = "";
            String v1 = "http";
            String v2 = "https";
            boolean qr_code_correct = false;
            if (value.startsWith(v1) || value.startsWith(v2)) {
                qr_code_scanner = true;
                boolean domainAvailable1 = isWordAvailable(value, "mobile2");
                boolean domainAvailable2 = isWordAvailable(value, "thegrid");
                //Log.e("", "domainAvailable1--  "+domainAvailable1+"    domainAvailable2--  "+domainAvailable2);
                if (domainAvailable1 == true || domainAvailable2 == true) {
                    betslip_url1 = value;
                    qr_code_correct = true;
                } else {
                    betslip_url1 = AppConfig.HOME_URL;
                    qr_code_correct = false;
                }

                Intent intent = new Intent(ZBarScannerActivity.this, BrowserActivity.class);
                intent.putExtra("betslip_url", betslip_url1);
                intent.putExtra("SelectIcon", "TRUE");
                intent.putExtra("qr_code_correct", qr_code_correct);
                startActivity(intent);
            } else {
                betslip_url1 = AppConfig.BARCODE_SUBMIT_URL + value;
                qr_code_correct = true;
                qr_code_scanner = false;
                callBackScreen(betslip_url1, qr_code_scanner, value);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void callBarCodeSendingNotUsed(String value) {
        try {
            //Intent intent = new Intent(ZBarScannerActivity.this,BrowserActivity.class);
            String betslip_url1 = "";
            //Log.e("", "callBarCodeSendingvalue--   "+value);


            String v1 = "http";
            String v2 = "https";
            boolean qr_code_correct = false;


            if (value.startsWith(v1) || value.startsWith(v2)) {

                boolean domainAvailable1 = isWordAvailable(value, "mobile2");
                boolean domainAvailable2 = isWordAvailable(value, "thegrid");
                //Log.e("", "domainAvailable1--  "+domainAvailable1+"    domainAvailable2--  "+domainAvailable2);
                if (domainAvailable1 == true || domainAvailable2 == true) {
                    betslip_url1 = value;
                    qr_code_correct = true;
                } else {
                    betslip_url1 = AppConfig.HOME_URL;
                    qr_code_correct = false;
                }


            } else {
                betslip_url1 = AppConfig.BARCODE_SUBMIT_URL + value;
                qr_code_correct = true;
            }

            callBackScreenNotUsed(betslip_url1, qr_code_correct);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isWordAvailable(String s, String word) {
        boolean d = false;
        try {
            d = s.contains(word);
        } catch (Exception e) {
            d = false;
        }
        return d;
    }


    private void playSound() {
        if (isFlashOn) {
            mp = MediaPlayer.create(ZBarScannerActivity.this, R.raw.light_switch_off);
        } else {
            mp = MediaPlayer.create(ZBarScannerActivity.this, R.raw.light_switch_on);
        }
        if(mp != null){
            mp.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    mp.release();
                }
            });
            mp.start();
        }
    }


    private void playSound1() {

        try {
            mp = MediaPlayer.create(ZBarScannerActivity.this, R.raw.light_switch_on);

            mp.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub
                    mp.release();
                }
            });
            mp.start();
        } catch (Exception e) {
        }
    }


}