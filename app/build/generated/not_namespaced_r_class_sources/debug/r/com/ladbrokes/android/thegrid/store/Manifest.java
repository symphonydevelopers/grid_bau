/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.ladbrokes.android.thegrid.store;

public final class Manifest {
  public static final class permission {
    /**
     * Near field communications permissions
     * Change Package Name Start
     */
    public static final String RECEIVE_ADM_MESSAGE="com.ladbrokes.android.thegrid.store.permission.RECEIVE_ADM_MESSAGE";
    public static final String UA_DATA="com.ladbrokes.android.thegrid.store.permission.UA_DATA";
    public static final String C2D_MESSAGE="com.ladbrokes.android.thegrid.store.permission.C2D_MESSAGE";
  }

}