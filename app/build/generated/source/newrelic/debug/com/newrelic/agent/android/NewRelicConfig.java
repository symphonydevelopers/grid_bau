package com.newrelic.agent.android;
final class NewRelicConfig {
	static final String VERSION = "5.25.0";
	static final String BUILD_ID = "8a1a6736-b314-457d-b483-e7565c8074ff";
	static final Boolean OBFUSCATED = false;
	public static String getBuildId() {
		return BUILD_ID;
	}
}
